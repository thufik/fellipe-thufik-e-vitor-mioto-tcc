#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define M 9000
#define N 9000


typedef struct pixel
{
	int red;
	int green;
	int blue;
	int alpha;

}pix;


pix **alocarMatriz(); //aloca uma matriz que contem em cada posiç uma struct(Trocar para conter ponteiros para structs)
void MontarMatriz(pix **m); //poe em cada posiç da matriz uma struct,gerando numeros aleatorios de 0 a 255 para o RGBA
void imprimirMatriz(pix **m);


void transformarMatriz(pix **m,pix **mTrans);
void funcAux(pix **m,pix **mTrans,int linha,int coluna);


int main()
{
	time_t start,stop;
    	double avg_time = 0;
    	double cur_time;
	pix **p = alocarMatriz(); //matriz de structs
	pix **vTrans = alocarMatriz();
	MontarMatriz(p);
	//imprimirMatriz(p);
	start = clock();
	transformarMatriz(p,vTrans);
	stop = clock();
	//imprimirMatriz(vTrans);
	cur_time = ((double) stop-start) / CLOCKS_PER_SEC;
	printf("Averagely used %f seconds.\n", cur_time);
	return 0;
	
}

void funcAux(pix **m,pix **mTrans,int linha,int coluna)
{
	int i;
	int j;
	int contadorR = 0;
	int contadorG = 0; 
	int contadorB = 0;
	int contadorA = 0;
	pix p;
	
	for(i = linha -2;i <= linha + 2;i++)
	{
		for(j = coluna - 2;j <= coluna + 2;j++)
		{
			if(i>=0 && i<M && j>=0 && j<N)
			{
				contadorR+= m[i][j].red;
				contadorG+= m[i][j].green;
				contadorB+= m[i][j].blue;
				contadorA+= m[i][j].alpha;
			}
		}
	}
	p.red = contadorR / 25;
	p.green = contadorG / 25;
	p.blue = contadorB / 25;
	p.alpha = contadorA / 25;
	
	mTrans[linha][coluna] = p; 

}


void transformarMatriz(pix **m,pix **mTrans)
{
	int i;
	int j;
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			funcAux(m,mTrans,i,j);
		}
	}
}

pix **alocarMatriz()
{
	int i;
	pix **p;
	p = (pix **) malloc(M * sizeof(pix *)); 
	
	for(i=0;i<M;i++)
	{
		p[i] = (pix *) malloc(N * sizeof(pix));
	}
	
	return p;
}


void MontarMatriz(pix **m)
{
	int i;
	int j;
	pix p;
	
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			p.red = rand() % 256;
			p.green = rand() % 256;
			p.blue = rand() % 256;
			p.alpha = rand() % 256;
			m[i][j] = p;
		}
	}
	
}

void imprimirMatriz(pix **m)
{
	int i;
	int j;
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			printf("Matriz RED[%d][%d] = %d\n",i,j,m[i][j].red);
	//		printf("Matriz GREEN[%d][%d] = %d\n",i,j,m[i][j].green);
	//		printf("Matriz BLUE[%d][%d] = %d\n",i,j,m[i][j].blue);
	//		printf("Matriz ALPHA[%d][%d] = %d\n",i,j,m[i][j].alpha);
		}			
	}	
}
