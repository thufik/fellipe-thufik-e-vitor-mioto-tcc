#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "xmmintrin.h"
#include "omp.h"
#define M 9000
#define N 9000


typedef struct pixel
{
	float red;
	float green;
	float blue;
	float alpha;

}pix; 


pix **alocarMatriz(); //aloca uma matriz que contem em cada posiç uma structt
void MontarMatriz(pix **m); //poe em cada posiç da matriz uma struct,gerando numeros aleatorios de 0 a 255 para o RGBA
void imprimirMatriz(pix **m);

void transformarMatriz(pix **m,pix **mTrans);
void funcAux(pix **m,pix **mTrans,int linha,int coluna);
float funcaoRed(pix **m,int linha,int coluna);
float funcaoGreen(pix **m,int linha,int coluna);
float funcaoBlue(pix **m,int linha,int coluna);
float funcaoAlpha(pix **m,int linha,int coluna);


int main()
{
    
    time_t start,stop;
    double avg_time = 0;
    double cur_time;
	pix **p = alocarMatriz(); //matriz de structs
	pix **vTrans = alocarMatriz();
	MontarMatriz(p);
	//imprimirMatriz(p);	
	start = clock();
 	transformarMatriz(p,vTrans); //passar como endereco
	stop = clock();
	cur_time = ((double) stop-start) / CLOCKS_PER_SEC;
	printf("Averagely used %f seconds.\n", cur_time);
	//imprimirMatriz(vTrans);
	return 0;
	
}

void funcAux(pix **m,pix **mTrans,int linha,int coluna)
{
    pix p;
    float total = 0;
    float total1 = 0;
    float total2 = 0;
    float total3 = 0;

/*   
	 #pragma omp parallel sections
   	 {
   	     #pragma omp section
   	     {  
	        // printf("Eu sou a thread %d\n",omp_get_thread_num());	
		 total = funcaoRed(m,linha,coluna);
             }	
	     #pragma omp section
	     {
		 // printf("Eu sou a thread %d\n",omp_get_thread_num());
       		   total1 = funcaoGreen(m,linha,coluna);
   	     } 
             #pragma omp section
             {
                 // printf("Eu sou a thread %d\n",omp_get_thread_num());
                   total2 = funcaoGreen(m,linha,coluna);
             }
             #pragma omp section
             {
                 // printf("Eu sou a thread %d\n",omp_get_thread_num());
                   total3 = funcaoGreen(m,linha,coluna);
             }

	}

*/
	//printf("Saiu\n");



        total = funcaoRed(m,linha,coluna);
        total1 = funcaoGreen(m,linha,coluna);
	total2 =  funcaoRed(m,linha,coluna);
	total3 = funcaoRed(m,linha,coluna);
//	p.red = total/9;
//        p.green = total1/9;
//	p.blue = total2/9;
//	p.alpha = total3/9;
//	mTrans[linha][coluna] = p;	
		

}



float funcaoRed(pix **m,int linha,int coluna)
{
    
    
     int i;
    __m128 x;
    __m128 y;
    __m128 z;
    __m128 parcial;
    __m128 full;
    float total = 0;

    
    if(linha == 0 || coluna == 0 || linha == (M-1) || coluna == (N-1))
    {
	       if(linha == 0)
           {
               
               if(coluna == 0)
               {
                   y = _mm_set_ps(0.0,0.0,m[linha][coluna].red,m[linha][coluna+1].red);
                   z = _mm_set_ps(0.0,0.0,m[linha+1][coluna].red,m[linha+1][coluna+1].red);

               }
               else if(coluna == N-1)
               {
                   y = _mm_set_ps(0.0,0.0,m[linha][coluna-1].red,m[linha][coluna].red);
                   z = _mm_set_ps(0.0,0.0,m[linha+1][coluna-1].red,m[linha+1][coluna].red);
               }
               else
               {
                   y = _mm_set_ps(0.0,m[linha][coluna-1].red,m[linha][coluna].red,m[linha][coluna+1].red);
                   z = _mm_set_ps(0.0,m[linha+1][coluna-1].red,m[linha+1][coluna].red,m[linha+1][coluna+1].red);
               }
               
               parcial = _mm_add_ps(y,z);

               
               for(i=0;i<3;i++)
               {
                   total+=parcial[i];
               }
               
           }
           else if(linha == M-1)
           {
               if(coluna == 0)
               {
                   y = _mm_set_ps(0.0,0.0,m[linha-1][coluna].red,m[linha-1][coluna+1].red);
                   z = _mm_set_ps(0.0,0.0,m[linha][coluna].red,m[linha][coluna+1].red);

               }
               else if(coluna == N-1)
               {
                   y = _mm_set_ps(0.0,0.0,m[linha-1][coluna-1].red,m[linha-1][coluna].red);
                   z = _mm_set_ps(0.0,0.0,m[linha][coluna-1].red,m[linha][coluna].red);

               }
               else
               {
                   y = _mm_set_ps(0.0,m[linha-1][coluna-1].red,m[linha-1][coluna].red,m[linha-1][coluna+1].red);
                   z = _mm_set_ps(0.0,m[linha][coluna-1].red,m[linha][coluna].red,m[linha][coluna+1].red);

               }
                   parcial = _mm_add_ps(y,z);

               
               for(i=0;i<3;i++)
               {
                   total+=parcial[i];
               }
               
           }
           else
           {
               if(coluna == 0)
               {
                   x = _mm_set_ps(0.0,0.0,m[linha-1][coluna].red,m[linha-1][coluna+1].red);
                   y = _mm_set_ps(0.0,0.0,m[linha][coluna].red,m[linha][coluna+1].red);
                   z = _mm_set_ps(0.0,0.0,m[linha+1][coluna].red,m[linha+1][coluna+1].red);

               }
               else
               {
                   x = _mm_set_ps(0.0,0.0,m[linha-1][coluna-1].red,m[linha-1][coluna].red);
                   y = _mm_set_ps(0.0,0.0,m[linha][coluna-1].red,m[linha][coluna].red);
                   z = _mm_set_ps(0.0,0.0,m[linha+1][coluna-1].red,m[linha+1][coluna].red);
                   
               }
                   parcial = _mm_add_ps(y,z);
                   full = _mm_add_ps(parcial,x);

               for(i=0;i<3;i++)
               {
                   total+=full[i];

               }
           }
    }
    else
    {
        
            x = _mm_set_ps(0.0,m[linha-1][coluna-1].red,m[linha-1][coluna].red,m[linha-1][coluna+1].red);
            y = _mm_set_ps(0.0,m[linha][coluna-1].red,m[linha][coluna].red,m[linha][coluna+1].red);
            z = _mm_set_ps(0.0,m[linha+1][coluna-1].red,m[linha+1][coluna].red,m[linha+1][coluna+1].red);
            parcial = _mm_add_ps(y,z);
            full = _mm_add_ps(parcial,x);

        
        for(i=0;i<3;i++)
        {
            total+=full[i];
            
        }
        
    }
    
    return total;
    
    
}


float funcaoGreen(pix **m,int linha,int coluna)
{
    
    int i;
    __m128 x;
    __m128 y;
    __m128 z;
    __m128 parcial;
    __m128 full;

    
    
    
    
    float total = 0;
    float total1 = 0;
    float total2 = 0;
    float total3 = 0;
    
    if(linha == 0 || coluna == 0 || linha == (M-1) || coluna == (N-1))
    {
	       if(linha == 0)
           {
               
               if(coluna == 0)
               {
                   y = _mm_set_ps(0.0,0.0,m[linha][coluna].red,m[linha][coluna+1].green);
                   z = _mm_set_ps(0.0,0.0,m[linha+1][coluna].red,m[linha+1][coluna+1].green);
               }
               else if(coluna == N-1)
               {
                   y = _mm_set_ps(0.0,0.0,m[linha][coluna-1].red,m[linha][coluna].green);
                   z = _mm_set_ps(0.0,0.0,m[linha+1][coluna-1].red,m[linha+1][coluna].green);

               }
               else
               {
                   y = _mm_set_ps(0.0,m[linha][coluna-1].red,m[linha][coluna].red,m[linha][coluna+1].green);
                   z = _mm_set_ps(0.0,m[linha+1][coluna-1].red,m[linha+1][coluna].red,m[linha+1][coluna+1].green);
               }
               
                   parcial = _mm_add_ps(y,z);
               
               for(i=0;i<3;i++)
               {
                   total+=parcial[i];
               }
               
           }
           else if(linha == M-1)
           {
               if(coluna == 0)
               {
                   y = _mm_set_ps(0.0,0.0,m[linha-1][coluna].red,m[linha-1][coluna+1].green);
                   z = _mm_set_ps(0.0,0.0,m[linha][coluna].red,m[linha][coluna+1].green);

               }
               else if(coluna == N-1)
               {
                   y = _mm_set_ps(0.0,0.0,m[linha-1][coluna-1].red,m[linha-1][coluna].green);
                   z = _mm_set_ps(0.0,0.0,m[linha][coluna-1].red,m[linha][coluna].green);

               }
               else
               {
                   y = _mm_set_ps(0.0,m[linha-1][coluna-1].red,m[linha-1][coluna].red,m[linha-1][coluna+1].green);
                   z = _mm_set_ps(0.0,m[linha][coluna-1].red,m[linha][coluna].red,m[linha][coluna+1].green);

               }

                   parcial = _mm_add_ps(y,z);
               
               for(i=0;i<3;i++)
               {
                   total+=parcial[i];

               }
               
           }
           else
           {
               if(coluna == 0)
               {
                   x = _mm_set_ps(0.0,0.0,m[linha-1][coluna].red,m[linha-1][coluna+1].green);
                   y = _mm_set_ps(0.0,0.0,m[linha][coluna].red,m[linha][coluna+1].green);
                   z = _mm_set_ps(0.0,0.0,m[linha+1][coluna].red,m[linha+1][coluna+1].green);

               }
               else
               {
                   x = _mm_set_ps(0.0,0.0,m[linha-1][coluna-1].red,m[linha-1][coluna].green);
                   y = _mm_set_ps(0.0,0.0,m[linha][coluna-1].red,m[linha][coluna].green);
                   z = _mm_set_ps(0.0,0.0,m[linha+1][coluna-1].red,m[linha+1][coluna].green);

                   
               }
                   parcial = _mm_add_ps(y,z);
                   full = _mm_add_ps(parcial,x);

               for(i=0;i<3;i++)
               {
                   total+=full[i];
               }
           }
    }
    else
    {
        
                x = _mm_set_ps(0.0,m[linha-1][coluna-1].red,m[linha-1][coluna].red,m[linha-1][coluna+1].green);
                y = _mm_set_ps(0.0,m[linha][coluna-1].red,m[linha][coluna].red,m[linha][coluna+1].green);
                z = _mm_set_ps(0.0,m[linha+1][coluna-1].red,m[linha+1][coluna].red,m[linha+1][coluna+1].green);
        
                parcial = _mm_add_ps(y,z);
                full = _mm_add_ps(parcial,x);

        
        for(i=0;i<3;i++)
        {
            total+=full[i];
            
        }
        
    }
    
    return total;
}




void transformarMatriz(pix **m,pix **mTrans)
{
	int i;
	int j;
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			funcAux(m,mTrans,i,j);
		}
	}
}

pix **alocarMatriz()
{
	int i;
	pix **p;
	p = (pix **) malloc(M * sizeof(pix *));
	
	for(i=0;i<M;i++)
	{
		p[i] = (pix *) malloc(N * sizeof(pix));
	}
	
	return p;
}


void MontarMatriz(pix **m)
{
	int i;
	int j;
	pix p;
	
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			p.red = rand() % 256;
			p.green = rand() % 256;
			p.blue = rand() % 256;
			p.alpha = rand() % 256;
			m[i][j] = p;
		}
	}
	
}

void imprimirMatriz(pix **m)
{
	int i;
	int j;
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			printf("Matriz RED[%d][%d] = %f\n",i,j,m[i][j].red);
			//printf("Matriz GREEN[%d][%d] = %d\n",i,j,m[i][j].green);
			//printf("Matriz BLUE[%d][%d] = %d\n",i,j,m[i][j].blue);
			//printf("Matriz ALPHA[%d][%d] = %d\n",i,j,m[i][j].alpha);
		}			
	}	
}
