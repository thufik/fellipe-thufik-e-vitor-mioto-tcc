#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "xmmintrin.h"
#include "omp.h"
#define M 9000
#define N 9000


typedef struct  pixel
{
	float red;
	float green;
	float blue;
	float alpha;

}pix; 




pix **alocarMatriz(); //aloca uma matriz que contem em cada posiç uma structt
void MontarMatriz(pix **m); //poe em cada posiç da matriz uma struct,gerando numeros aleatorios de 0 a 255 para o RGBA
void imprimirMatriz(pix **m);

void transformarMatriz(pix **m,pix **mTrans);
void funcAux(pix **m,pix **mTrans,int linha,int coluna);

int contador = 0;


int main()
{
	time_t start,stop;
    	double avg_time = 0;
    	double cur_time;
	pix **p = alocarMatriz(); //matriz de structs
	pix **vTrans = alocarMatriz();
	MontarMatriz(p);
//	imprimirMatriz(p);	
	start = clock();
 	transformarMatriz(p,vTrans);
	stop = clock();
	cur_time = ((double) stop-start) / CLOCKS_PER_SEC;
	printf("Averagely used %f seconds.\n", cur_time);
	//printf("Contador = %d\n",contador);
//	imprimirMatriz(vTrans);
	return 0;
	
}

void funcAux(pix **m,pix **mTrans,int linha,int coluna)
{
	int i;
        __m128 x; 
        __m128 y;
        __m128 z;
        __m128 x1;
        __m128 y1;
        __m128 z1;
        __m128 x2;
        __m128 y2;
        __m128 z2;
        __m128 x3;
        __m128 y3;
        __m128 z3;	  
        __m128 full;
        __m128 full1;
        __m128 full2;
        __m128 full3;
	
	float total = 0;
        float total1 = 0;
        float total2 = 0;
        float total3 = 0;
	pix p;
	
	if(linha == 0 || coluna == 0 || linha == (M-1) || coluna == (N-1)) 
	{
	
	       if(linha == 0)
	       {
		   
		     if(coluna == 0)
		     {
                  	y = _mm_set_ps(0.0,0.0,m[linha][coluna].red,m[linha][coluna+1].red);
                  	z = _mm_set_ps(0.0,0.0,m[linha+1][coluna].red,m[linha+1][coluna+1].red);
                        y1 = _mm_set_ps(0.0,0.0,m[linha][coluna].green,m[linha][coluna+1].green);
                        z1 = _mm_set_ps(0.0,0.0,m[linha+1][coluna].green,m[linha+1][coluna+1].green);
                        y2 = _mm_set_ps(0.0,0.0,m[linha][coluna].blue,m[linha][coluna+1].blue);
                        z2 = _mm_set_ps(0.0,0.0,m[linha+1][coluna].blue,m[linha+1][coluna+1].blue);
                        y3 = _mm_set_ps(0.0,0.0,m[linha][coluna].alpha,m[linha][coluna+1].alpha);
                        z3 = _mm_set_ps(0.0,0.0,m[linha+1][coluna].alpha,m[linha+1][coluna+1].alpha);
		     }
		     else if(coluna == N-1)
	 	     {
			y = _mm_set_ps(0.0,0.0,m[linha][coluna-1].red,m[linha][coluna].red);
			z = _mm_set_ps(0.0,0.0,m[linha+1][coluna-1].red,m[linha+1][coluna].red);
                        y1 = _mm_set_ps(0.0,0.0,m[linha][coluna-1].green,m[linha][coluna].green);
                        z1 = _mm_set_ps(0.0,0.0,m[linha+1][coluna-1].green,m[linha+1][coluna].green);
                        y2 = _mm_set_ps(0.0,0.0,m[linha][coluna-1].blue,m[linha][coluna].blue);
                        z2 = _mm_set_ps(0.0,0.0,m[linha+1][coluna-1].blue,m[linha+1][coluna].blue);                       
                        y3 = _mm_set_ps(0.0,0.0,m[linha][coluna-1].alpha,m[linha][coluna].alpha);
                        z3 = _mm_set_ps(0.0,0.0,m[linha+1][coluna-1].alpha,m[linha+1][coluna].alpha);
		     }
		     else
		     {	
			 y = _mm_set_ps(0.0,m[linha][coluna-1].red,m[linha][coluna].red,m[linha][coluna+1].red);
                 	 z = _mm_set_ps(0.0,m[linha+1][coluna-1].red,m[linha+1][coluna].red,m[linha+1][coluna+1].red);
                         y1 = _mm_set_ps(0.0,m[linha][coluna-1].green,m[linha][coluna].green,m[linha][coluna+1].green);
                         z1 = _mm_set_ps(0.0,m[linha+1][coluna-1].green,m[linha+1][coluna].green,m[linha+1][coluna+1].green);
                         y2 = _mm_set_ps(0.0,m[linha][coluna-1].blue,m[linha][coluna].blue,m[linha][coluna+1].blue);
                         z2 = _mm_set_ps(0.0,m[linha+1][coluna-1].blue,m[linha+1][coluna].blue,m[linha+1][coluna+1].blue);
                         y3 = _mm_set_ps(0.0,m[linha][coluna-1].alpha,m[linha][coluna].alpha,m[linha][coluna+1].alpha);
                         z3 = _mm_set_ps(0.0,m[linha+1][coluna-1].alpha,m[linha+1][coluna].alpha,m[linha+1][coluna+1].alpha);	
		     }
		 
	                 full = _mm_add_ps(y,z);
                         full1 = _mm_add_ps(y1,z1);
                         full2 = _mm_add_ps(y2,z2);
                         full3 = _mm_add_ps(y3,z3);
  	
             		 for(i=0;i<3;i++)
                	 {
                        	total+=full[i];
                		total1+=full1[i];
                                total2+=full2[i];
                                total3+=full3[i];
			 }
	
	      }
	       else if(linha == M-1)
	       {
		   if(coluna == 0)
                   {
                       y = _mm_set_ps(0.0,0.0,m[linha-1][coluna].red,m[linha-1][coluna+1].red);
                       z = _mm_set_ps(0.0,0.0,m[linha][coluna].red,m[linha][coluna+1].red);
                       y1 = _mm_set_ps(0.0,0.0,m[linha-1][coluna].green,m[linha-1][coluna+1].green);
                       z1 = _mm_set_ps(0.0,0.0,m[linha][coluna].green,m[linha][coluna+1].green);
                       y2 = _mm_set_ps(0.0,0.0,m[linha-1][coluna].blue,m[linha-1][coluna+1].blue);
                       z2 = _mm_set_ps(0.0,0.0,m[linha][coluna].blue,m[linha][coluna+1].blue);
                       y3 = _mm_set_ps(0.0,0.0,m[linha-1][coluna].alpha,m[linha-1][coluna+1].alpha);
                       z3 = _mm_set_ps(0.0,0.0,m[linha][coluna].alpha,m[linha][coluna+1].alpha);
		   }
		   else if(coluna == N-1)
                   {
                        y = _mm_set_ps(0.0,0.0,m[linha-1][coluna-1].red,m[linha-1][coluna].red);
                        z = _mm_set_ps(0.0,0.0,m[linha][coluna-1].red,m[linha][coluna].red);
                        y1 = _mm_set_ps(0.0,0.0,m[linha-1][coluna-1].green,m[linha-1][coluna].green);
                        z1 = _mm_set_ps(0.0,0.0,m[linha][coluna-1].green,m[linha][coluna].green);
                        y2 = _mm_set_ps(0.0,0.0,m[linha-1][coluna-1].blue,m[linha-1][coluna].blue);
                        z2 = _mm_set_ps(0.0,0.0,m[linha][coluna-1].blue,m[linha][coluna].blue);
                        y3 = _mm_set_ps(0.0,0.0,m[linha-1][coluna-1].alpha,m[linha-1][coluna].alpha);
                        z3 = _mm_set_ps(0.0,0.0,m[linha][coluna-1].alpha,m[linha][coluna].alpha);
                   }
                   else
                   {
                         y = _mm_set_ps(0.0,m[linha-1][coluna-1].red,m[linha-1][coluna].red,m[linha-1][coluna+1].red);
                         z = _mm_set_ps(0.0,m[linha][coluna-1].red,m[linha][coluna].red,m[linha][coluna+1].red);
                         y1 = _mm_set_ps(0.0,m[linha-1][coluna-1].green,m[linha-1][coluna].green,m[linha-1][coluna+1].green);
                         z1 = _mm_set_ps(0.0,m[linha][coluna-1].green,m[linha][coluna].green,m[linha][coluna+1].green);
                         y2 = _mm_set_ps(0.0,m[linha-1][coluna-1].blue,m[linha-1][coluna].blue,m[linha-1][coluna+1].blue);
                         z2 = _mm_set_ps(0.0,m[linha][coluna-1].blue,m[linha][coluna].blue,m[linha][coluna+1].blue);
                         y3 = _mm_set_ps(0.0,m[linha-1][coluna-1].alpha,m[linha-1][coluna].alpha,m[linha-1][coluna+1].alpha);
                         z3 = _mm_set_ps(0.0,m[linha][coluna-1].alpha,m[linha][coluna].alpha,m[linha][coluna+1].alpha);
                   }
                         full = _mm_add_ps(y,z);
                         full1 = _mm_add_ps(y1,z1);
                         full2 = _mm_add_ps(y2,z2);
                         full3 = _mm_add_ps(y3,z3);
			                       
                         for(i=0;i<3;i++)
                         {
                              total+=full[i];
                              total1+=full1[i];
                              total2+=full2[i];
                              total3+=full3[i]; 
			 }
                       
	       }
	       else
	       { 
		   if(coluna == 0)
	   	   {
			x = _mm_set_ps(0.0,0.0,m[linha-1][coluna].red,m[linha-1][coluna+1].red);
			y = _mm_set_ps(0.0,0.0,m[linha][coluna].red,m[linha][coluna+1].red);
			z = _mm_set_ps(0.0,0.0,m[linha+1][coluna].red,m[linha+1][coluna+1].red);
                        x1 = _mm_set_ps(0.0,0.0,m[linha-1][coluna].green,m[linha-1][coluna+1].green);
                        y1 = _mm_set_ps(0.0,0.0,m[linha][coluna].green,m[linha][coluna+1].green);
                        z1 = _mm_set_ps(0.0,0.0,m[linha+1][coluna].green,m[linha+1][coluna+1].green);
                        x2 = _mm_set_ps(0.0,0.0,m[linha-1][coluna].blue,m[linha-1][coluna+1].blue);
                        y2 = _mm_set_ps(0.0,0.0,m[linha][coluna].blue,m[linha][coluna+1].blue);
                        z2 = _mm_set_ps(0.0,0.0,m[linha+1][coluna].blue,m[linha+1][coluna+1].blue);
                        x3 = _mm_set_ps(0.0,0.0,m[linha-1][coluna].alpha,m[linha-1][coluna+1].alpha);
                        y3 = _mm_set_ps(0.0,0.0,m[linha][coluna].alpha,m[linha][coluna+1].alpha);
                        z3 = _mm_set_ps(0.0,0.0,m[linha+1][coluna].alpha,m[linha+1][coluna+1].alpha);
	 	   }
		   else
		   {
		        x = _mm_set_ps(0.0,0.0,m[linha-1][coluna-1].red,m[linha-1][coluna].red);
                        y = _mm_set_ps(0.0,0.0,m[linha][coluna-1].red,m[linha][coluna].red);
                        z = _mm_set_ps(0.0,0.0,m[linha+1][coluna-1].red,m[linha+1][coluna].red);
                        x1 = _mm_set_ps(0.0,0.0,m[linha-1][coluna-1].green,m[linha-1][coluna].green);
                        y1 = _mm_set_ps(0.0,0.0,m[linha][coluna-1].green,m[linha][coluna].green);
                        z1 = _mm_set_ps(0.0,0.0,m[linha+1][coluna-1].green,m[linha+1][coluna].green);
                        x2 = _mm_set_ps(0.0,0.0,m[linha-1][coluna-1].blue,m[linha-1][coluna].blue);
                        y2 = _mm_set_ps(0.0,0.0,m[linha][coluna-1].blue,m[linha][coluna].blue);
                        z2 = _mm_set_ps(0.0,0.0,m[linha+1][coluna-1].blue,m[linha+1][coluna].blue);
                        x3 = _mm_set_ps(0.0,0.0,m[linha-1][coluna-1].alpha,m[linha-1][coluna].alpha);
                        y3 = _mm_set_ps(0.0,0.0,m[linha][coluna-1].alpha,m[linha][coluna].alpha);
                        z3 = _mm_set_ps(0.0,0.0,m[linha+1][coluna-1].alpha,m[linha+1][coluna].alpha);

	           }
                       
                         full = _mm_add_ps(_mm_add_ps(y,z),x);
                         full1 = _mm_add_ps(_mm_add_ps(y1,z1),x1);
                         full2 = _mm_add_ps(_mm_add_ps(y2,z2),x2);
                         full3 = _mm_add_ps(_mm_add_ps(y3,z3),x3);
                        
			 for(i=0;i<3;i++)
                         {
                                total+=full[i];
                                total1+=full1[i];
                                total2+=full2[i];
                                total3+=full3[i];				
                         }
		
	 }	
	
	
	}
	else
	{

		x = _mm_set_ps(0.0,m[linha-1][coluna-1].red,m[linha-1][coluna].red,m[linha-1][coluna+1].red);
		y = _mm_set_ps(0.0,m[linha][coluna-1].red,m[linha][coluna].red,m[linha][coluna+1].red);
		z = _mm_set_ps(0.0,m[linha+1][coluna-1].red,m[linha+1][coluna].red,m[linha+1][coluna+1].red);
                x1 = _mm_set_ps(0.0,m[linha-1][coluna-1].green,m[linha-1][coluna].green,m[linha-1][coluna+1].green);
                y1 = _mm_set_ps(0.0,m[linha][coluna-1].green,m[linha][coluna].green,m[linha][coluna+1].green);
                z1 = _mm_set_ps(0.0,m[linha+1][coluna-1].green,m[linha+1][coluna].green,m[linha+1][coluna+1].green);
                x2 = _mm_set_ps(0.0,m[linha-1][coluna-1].blue,m[linha-1][coluna].blue,m[linha-1][coluna+1].blue);
                y2 = _mm_set_ps(0.0,m[linha][coluna-1].blue,m[linha][coluna].blue,m[linha][coluna+1].blue);
                z2 = _mm_set_ps(0.0,m[linha+1][coluna-1].blue,m[linha+1][coluna].blue,m[linha+1][coluna+1].blue);
                x3 = _mm_set_ps(0.0,m[linha-1][coluna-1].alpha,m[linha-1][coluna].alpha,m[linha-1][coluna+1].alpha);
                y3 = _mm_set_ps(0.0,m[linha][coluna-1].alpha,m[linha][coluna].alpha,m[linha][coluna+1].alpha);
                z3 = _mm_set_ps(0.0,m[linha+1][coluna-1].alpha,m[linha+1][coluna].alpha,m[linha+1][coluna+1].alpha);

	       
	        full = _mm_add_ps(_mm_add_ps(y,z),x);
                full1 = _mm_add_ps(_mm_add_ps(y1,z1),x1);
                full2 = _mm_add_ps(_mm_add_ps(y2,z2),x2);
                full3 = _mm_add_ps(_mm_add_ps(y3,z3),x3);
	
		for(i=0;i<3;i++)
		{
			total+=full[i];
                        total1+=full1[i];
                        total2+=full2[i];
                        total3+=full3[i];

		}
		
	}

	
	p.red = total/9;
	p.green = total1/9;
	p.blue = total2/9;
	p.alpha = total3/9;
	mTrans[linha][coluna] = p;		


}
        
	

void transformarMatriz(pix **m,pix **mTrans)
{
	int i;
	int j;
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			funcAux(m,mTrans,i,j);
		}
	}
}

pix **alocarMatriz()
{
	int i;
	pix **p;
	p = (pix **) malloc(M * sizeof(pix *)); 
	
	for(i=0;i<M;i++)
	{
		p[i] = (pix *) malloc(N * sizeof(pix));
	}
	
	return p;
}


void MontarMatriz(pix **m)
{
	int i;
	int j;
	pix p;
	
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			p.red = rand() % 256;
			p.green = rand() % 256;
			p.blue = rand() % 256;
			p.alpha = rand() % 256;
			m[i][j] = p;
		}
	}
	
}

void imprimirMatriz(pix **m)
{
	int i;
	int j;
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			printf("Matriz RED[%d][%d] = %f\n",i,j,m[i][j].red);
			//printf("Matriz GREEN[%d][%d] = %d\n",i,j,m[i][j].green);
			//printf("Matriz BLUE[%d][%d] = %d\n",i,j,m[i][j].blue);
			//printf("Matriz ALPHA[%d][%d] = %d\n",i,j,m[i][j].alpha);
		}			
	}	
}
