#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "xmmintrin.h"
#define M 9000
#define N 9000


typedef struct pixel
{
	float red;
	float green;
	float blue;
	float alpha;

}pix; 


pix **alocarMatriz(); //aloca uma matriz que contem em cada posiç uma structt
void MontarMatriz(pix **m); //poe em cada posiç da matriz uma struct,gerando numeros aleatorios de 0 a 255 para o RGBA
void imprimirMatriz(pix **m);


void transformarMatriz(pix **m,pix **mTrans);
void funcAux(pix **m,pix **mTrans,int linha,int coluna);


int main()
{
	time_t start,stop;
    	double avg_time = 0;
    	double cur_time;
	pix **p = alocarMatriz(); //matriz de structs
	pix **vTrans = alocarMatriz();
	MontarMatriz(p);
	//imprimirMatriz(p);	
	start = clock();
 	transformarMatriz(p,vTrans);
	stop = clock();
	cur_time = ((double) stop-start) / CLOCKS_PER_SEC;
	printf("Averagely used %f seconds.\n", cur_time);
        //imprimirMatriz(vTrans);
	return 0;
	
}

void funcAux(pix **m,pix **mTrans,int linha,int coluna)
{
	int i;
	int j; 
	
        __m128 x = _mm_setzero_ps();
        __m128 y = _mm_setzero_ps();
        __m128 z = _mm_setzero_ps();
        __m128 x1 = _mm_setzero_ps();
        __m128 y1 = _mm_setzero_ps();
        __m128 z1 = _mm_setzero_ps();
        __m128 x2 = _mm_setzero_ps();
        __m128 y2 = _mm_setzero_ps();
        __m128 z2 = _mm_setzero_ps();
        __m128 x3 = _mm_setzero_ps();
        __m128 y3 = _mm_setzero_ps();
        __m128 z3 = _mm_setzero_ps();

        __m128 parcial = _mm_setzero_ps();
        __m128 full = _mm_setzero_ps();
        __m128 parcial1 = _mm_setzero_ps();
        __m128 full1 = _mm_setzero_ps();
        __m128 parcial2 = _mm_setzero_ps();
        __m128 full2 = _mm_setzero_ps();
        __m128 parcial3 = _mm_setzero_ps();
        __m128 full3 = _mm_setzero_ps();

	float total = 0;
        float total1 = 0;
        float total2 = 0;
        float total3 = 0;

	pix p;
	
	for(i = linha -1;i<= linha +1;i++)
	{
		j = coluna -1;
		if(i>=0 && i<=N-1 && j>=0 && j!= N -2)
		{
			if(i == linha - 1)
                        {
                                x = _mm_set_ps(0.0,m[i][j+2].red,m[i][j+1].red,m[i][j].red);
 				x1 = _mm_set_ps(0.0,m[i][j+2].green,m[i][j+1].red,m[i][j].green);
 				x2 = _mm_set_ps(0.0,m[i][j+2].blue,m[i][j+1].red,m[i][j].blue);
 				x3 = _mm_set_ps(0.0,m[i][j+2].alpha,m[i][j+1].red,m[i][j].alpha);
                        }
                        else if(i == linha)
                        {
                                y = _mm_set_ps(0.0,m[i][j+2].red,m[i][j+1].red,m[i][j].red);
                                y1 = _mm_set_ps(0.0,m[i][j+2].green,m[i][j+1].red,m[i][j].green);
                                y2 = _mm_set_ps(0.0,m[i][j+2].blue,m[i][j+1].red,m[i][j].blue);
                                y3 = _mm_set_ps(0.0,m[i][j+2].alpha,m[i][j+1].red,m[i][j].alpha);
                        }
                        else if(i == linha + 1)
                        {
                                z = _mm_set_ps(0.0,m[i][j+2].red,m[i][j+1].red,m[i][j].red);
                                z1 = _mm_set_ps(0.0,m[i][j+2].green,m[i][j+1].green,m[i][j].green);
                                z2 = _mm_set_ps(0.0,m[i][j+2].blue,m[i][j+1].blue,m[i][j].blue);
                                z3 = _mm_set_ps(0.0,m[i][j+2].alpha,m[i][j+1].alpha,m[i][j].alpha);
                        }

		}
                else if(i>=0 && i<=N-1 && j<0)
                {

                        if(i == linha - 1)
                        {
                                 x = _mm_set_ps(0.0,0.0,m[i][j+2].red,m[i][j+1].red);
                                 x1 = _mm_set_ps(0.0,0.0,m[i][j+2].green,m[i][j+1].green);
                                 x2 = _mm_set_ps(0.0,0.0,m[i][j+2].blue,m[i][j+1].blue);
                                 x3 = _mm_set_ps(0.0,0.0,m[i][j+2].alpha,m[i][j+1].alpha);
                        }
                        else if(i == linha)
                        {
                                y = _mm_set_ps(0.0,0.0,m[i][j+2].red,m[i][j+1].red);
                                y1 = _mm_set_ps(0.0,0.0,m[i][j+2].green,m[i][j+1].green);
                                y2 = _mm_set_ps(0.0,0.0,m[i][j+2].blue,m[i][j+1].blue);
                                y3 = _mm_set_ps(0.0,0.0,m[i][j+2].alpha,m[i][j+1].alpha);

                        }
                        else if(i == linha + 1)
                        {
                                z = _mm_set_ps(0.0,0.0,m[i][j+2].red,m[i][j+1].red);
                                z1 = _mm_set_ps(0.0,0.0,m[i][j+2].green,m[i][j+1].green);
                                z2 = _mm_set_ps(0.0,0.0,m[i][j+2].blue,m[i][j+1].blue);
                                z3 = _mm_set_ps(0.0,0.0,m[i][j+2].alpha,m[i][j+1].alpha);

                        }
                }
                else if(i>=0 && i<=N-1 && j==N - 2)
                {
                         if(i == linha - 1)
                        {
                                 x = _mm_set_ps(0.0,0.0,m[i][j].red,m[i][j+1].red);
                                 x1 = _mm_set_ps(0.0,0.0,m[i][j].green,m[i][j+1].green);
                                 x2 = _mm_set_ps(0.0,0.0,m[i][j].blue,m[i][j+1].blue);
                                 x3 = _mm_set_ps(0.0,0.0,m[i][j].alpha,m[i][j+1].alpha);
                        }
                        else if(i == linha)
                        {
                                y = _mm_set_ps(0.0,0.0,m[i][j].red,m[i][j+1].red);
                                y1 = _mm_set_ps(0.0,0.0,m[i][j].green,m[i][j+1].green);
                                y2 = _mm_set_ps(0.0,0.0,m[i][j].blue,m[i][j+1].blue);
                                y3 = _mm_set_ps(0.0,0.0,m[i][j].alpha,m[i][j+1].alpha);


                        }
                        else if(i == linha + 1)
                        {
                                z = _mm_set_ps(0.0,0.0,m[i][j].red,m[i][j+1].red);
                                z1 = _mm_set_ps(0.0,0.0,m[i][j].green,m[i][j+1].green);
                                z2 = _mm_set_ps(0.0,0.0,m[i][j].blue,m[i][j+1].blue);
                                z3 = _mm_set_ps(0.0,0.0,m[i][j].alpha,m[i][j+1].alpha);


                        }

               }
	} 

         parcial = _mm_add_ps(y,z);
         full = _mm_add_ps(parcial,x);
         parcial1 = _mm_add_ps(y1,z1);
         full1 = _mm_add_ps(parcial1,x1);
         parcial2 = _mm_add_ps(y2,z2);
         full2 = _mm_add_ps(parcial2,x2);
         parcial3 = _mm_add_ps(y3,z3);
         full3 = _mm_add_ps(parcial3,x3);

        for(i=0;i<4;i++)
        {
              total += full[i];
              total1 += full[i];
              total2 += full[i];
              total3 += full[i];

        }

       	p.red = total/9;
	p.green = total1/9;
	p.blue = total2/9;
	p.alpha = total3/9;
	mTrans[linha][coluna] = p;
	// printf("total = %f\n",total/9);

}


void transformarMatriz(pix **m,pix **mTrans)
{
	int i;
	int j;
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			funcAux(m,mTrans,i,j);
		}
	}
}

pix **alocarMatriz()
{
	int i;
	pix **p;
	p = (pix **) malloc(M * sizeof(pix *)); 
	
	for(i=0;i<M;i++)
	{
		p[i] = (pix *) malloc(N * sizeof(pix));
	}
	
	return p;
}


void MontarMatriz(pix **m)
{
	int i;
	int j;
	pix p;
	
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			p.red = rand() % 256;
			p.green = rand() % 256;
			p.blue = rand() % 256;
			p.alpha = rand() % 256;
			m[i][j] = p;
		}
	}
	
}

void imprimirMatriz(pix **m)
{
	int i;
	int j;
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			printf("Matriz RED[%d][%d] = %f\n",i,j,m[i][j].red);
			//printf("Matriz GREEN[%d][%d] = %d\n",i,j,m[i][j].green);
			//printf("Matriz BLUE[%d][%d] = %d\n",i,j,m[i][j].blue);
			//printf("Matriz ALPHA[%d][%d] = %d\n",i,j,m[i][j].alpha);
		}			
	}	
}
