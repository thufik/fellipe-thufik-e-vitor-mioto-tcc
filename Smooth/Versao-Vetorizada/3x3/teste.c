#include <stdio.h>
#include "xmmintrin.h"

int main()
{

	__m128 x,y,z;
	int i;
	float total;
	float v1[4] = {2.0,1.0,2.0,3.0};
	float v2[4] = {2.0,1.0,2.0,3.0};
	x = _mm_loadu_ps(v1);
	y = _mm_loadu_ps(v2); 
	z = _mm_add_ps(x,y); 
	_mm_store_ss(&total,z);
/*
	for(i=0;i<4;i++)
	{
		total+=z[i];
	}
*/
	printf("total = %f",total);
	//printf("z[1] = %f",z[1]);


}
