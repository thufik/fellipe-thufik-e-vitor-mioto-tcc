#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "xmmintrin.h"
#define M 9000
#define N 9000


typedef struct  pixel 
{
	float red; 
	float green;
	float blue;
	float alpha;

}pix; 

 __m128 divisao;


pix **alocarMatriz(); //aloca uma matriz que contem em cada posiç uma structt
void MontarMatriz(pix **m); //poe em cada posiç da matriz uma struct,gerando numeros aleatorios de 0 a 255 para o RGBA
void imprimirMatriz(pix **m);

void transformarMatriz(pix **m,pix **mTrans);
void funcAux(pix **m,pix **mTrans,int linha,int coluna);

int contador = 0;


int main()
{
	time_t start,stop;
    	double avg_time = 0;
    	double cur_time;
	pix **p = alocarMatriz(); //matriz de structs
	pix **vTrans = alocarMatriz();
	MontarMatriz(p);
	//imprimirMatriz(p);	
	start = clock();
	divisao = _mm_set_ps1(9.0);
 	transformarMatriz(p,vTrans);
	stop = clock();
	cur_time = ((double) stop-start) / CLOCKS_PER_SEC;
	printf("Averagely used %f seconds.\n", cur_time);
	//printf("Contador = %d\n",contador);
	//imprimirMatriz(vTrans);
	return 0;
	
}

void funcAux(pix **m,pix **mTrans,int linha,int coluna)
{
	int i;
	pix p;


	
	if(linha == 0 || coluna == 0 || linha == (M-1) || coluna == (N-1)) 
	{
	
	       if(linha == 0)
	       {
		   
		     if(coluna == 0)
		     {
			
		        __m128 x;
        		__m128 y;
      		        __m128 z;
        		__m128 x1;
                        __m128 full;
                        __m128 full1;
                        

			x = _mm_setr_ps(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
			y = _mm_setr_ps(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
			z = _mm_setr_ps(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
			x1 = _mm_setr_ps(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
			
		        full = _mm_add_ps(x,_mm_add_ps(y,z));
                	full1 = _mm_add_ps(x1,full);
		        p.red = full1[0]/9;
	                p.green = full1[1]/9;
	                p.blue = full1[2]/9;
	                p.alpha = full1[3]/9;
	                mTrans[linha][coluna] = p;

		     }
		     else if(coluna == N-1)
	 	     {          
                        __m128 x;
                        __m128 y;
                        __m128 z;
                        __m128 x1;
                        __m128 full;
                        __m128 full1;
                       
			x = _mm_setr_ps(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                        y = _mm_setr_ps(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                        z = _mm_setr_ps(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                        x1 = _mm_setr_ps(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                        full = _mm_add_ps(x,_mm_add_ps(y,z));
                        full1 = _mm_add_ps(x1,full);
                        p.red = full1[0]/9;
                        p.green = full1[1]/9;
                        p.blue = full1[2]/9;
                        p.alpha = full1[3]/9;
                        mTrans[linha][coluna] = p;

		     }
		     else
		     {	
                        __m128 x;
                        __m128 y;
                        __m128 z;
                        __m128 x1;
			__m128 y1;
			__m128 z1;
                        __m128 full;
                        __m128 full1;
                        __m128 full2;

                        x = _mm_setr_ps(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                        y = _mm_setr_ps(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                        z = _mm_setr_ps(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);                     
		        x1 = _mm_setr_ps(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                        y1 = _mm_setr_ps(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                        z1 = _mm_setr_ps(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
	                full = _mm_add_ps(x,_mm_add_ps(y,z));
	                full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
	                full2 = _mm_add_ps(full,full1); 
	                p.red = full2[0]/9;
	                p.green = full2[1]/9;
	                p.blue = full2[2]/9;
	                p.alpha = full2[3]/9;
	                mTrans[linha][coluna] = p;

	    	     }

	      }
	       else if(linha == M-1)
	       {
		   if(coluna == 0)
                   {
                        __m128 x;
                        __m128 y;
                        __m128 z;
                        __m128 x1;
                        __m128 full;
                        __m128 full1;
                        
                        x = _mm_setr_ps(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                        y = _mm_setr_ps(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                        z = _mm_setr_ps(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                        x1 = _mm_setr_ps(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                        full = _mm_add_ps(x,_mm_add_ps(y,z));
                        full1 = _mm_add_ps(x1,full);
                        p.red = full1[0]/9;
                        p.green = full1[1]/9;
                        p.blue = full1[2]/9;
                        p.alpha = full1[3]/9;
                        mTrans[linha][coluna] = p;

		   }
		   else if(coluna == N-1)
                   {

                        __m128 x;
                        __m128 y;
                        __m128 z;
                        __m128 x1;
                        __m128 full;
                        __m128 full1;

                        x = _mm_setr_ps(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                        y = _mm_setr_ps(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                        z = _mm_setr_ps(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                        x1 = _mm_setr_ps(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
                        full = _mm_add_ps(x,_mm_add_ps(y,z));
                        full1 = _mm_add_ps(x1,full);
                        p.red = full1[0]/9;
                        p.green = full1[1]/9;
                        p.blue = full1[2]/9;
                        p.alpha = full1[3]/9;
                        mTrans[linha][coluna] = p;

		   }
                   else
                   {

                        __m128 x;
                        __m128 y;
                        __m128 z;
                        __m128 x1;
			__m128 y1;
			__m128 z1;
                        __m128 full;
                        __m128 full1;
                        __m128 full2;

	                x = _mm_setr_ps(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                        y = _mm_setr_ps(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                        z = _mm_setr_ps(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                        x1 = _mm_setr_ps(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                        y1 = _mm_setr_ps(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
                        z1 = _mm_setr_ps(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                        full = _mm_add_ps(x,_mm_add_ps(y,z));
                        full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                        full2 = _mm_add_ps(full,full1);
                        p.red = full2[0]/9;
                        p.green = full2[1]/9;
                        p.blue = full2[2]/9;
                        p.alpha = full2[3]/9;
                        mTrans[linha][coluna] = p;

                   }
                       
	       }
	       else
	       { 
		   if(coluna == 0)
	   	   {  

                        __m128 x;
                        __m128 y;
                        __m128 z;
                        __m128 x1;
                        __m128 y1;
                        __m128 z1;
                        __m128 full;
                        __m128 full1;
                        __m128 full2;

                        x = _mm_setr_ps(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                        y = _mm_setr_ps(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                        z = _mm_setr_ps(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                        x1 = _mm_setr_ps(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
                        y1 = _mm_setr_ps(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                        z1 = _mm_setr_ps(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                        full = _mm_add_ps(x,_mm_add_ps(y,z));
                        full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                        full2 = _mm_add_ps(full,full1);
                        p.red = full2[0]/9;
                        p.green = full2[1]/9;
                        p.blue = full2[2]/9;
                        p.alpha = full2[3]/9;
                        mTrans[linha][coluna] = p;

	 	   }
		   else
		   {
                        __m128 x;
                        __m128 y;
                        __m128 z;
                        __m128 x1;
                        __m128 y1;
                        __m128 z1;
             	        __m128 full;
             	        __m128 full1;
             		__m128 full2;
                        x = _mm_setr_ps(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                        y = _mm_setr_ps(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                        z = _mm_setr_ps(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                        x1 = _mm_setr_ps(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                        y1 = _mm_setr_ps(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                        z1 = _mm_setr_ps(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
                        full = _mm_add_ps(x,_mm_add_ps(y,z));
                        full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                        full2 = _mm_add_ps(full,full1);
                        p.red = full2[0]/9;
                        p.green = full2[1]/9;
                        p.blue = full2[2]/9;
                        p.alpha = full2[3]/9;
                        mTrans[linha][coluna] = p;

	           }
  		
	       }	
	
	}
	else
	{
               __m128 x;
               __m128 y;
               __m128 z;
               __m128 x1;
               __m128 y1;
               __m128 z1;
               __m128 x2;
               __m128 y2;
               __m128 z2;
	       __m128 full;
              __m128 full1;
              __m128 full2;
              __m128 full3;        

/*
	for (i = linha-1;i<=linha+1;i++)
	{
//               	 x = _mm_setr_ps(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
//                	 y = _mm_setr_ps(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
//                	 z = _mm_set_ps(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
  
              x = _mm_setr_ps(m[i][coluna].red,m[i][coluna].green,m[i][coluna].blue,m[i][coluna].alpha);
              y = _mm_setr_ps(m[i][coluna+1].red,m[i][coluna+1].green,m[i][coluna+1].blue,m[i][coluna+1].alpha);
              z = _mm_setr_ps(m[i][coluna-1].red,m[i][coluna-1].green,m[i][coluna-1].blue,m[i][coluna-1].alpha);
              full += _mm_add_ps(x,_mm_add_ps(y,z));
	     // printf("m[%d][%d] = %f\n",linha-1,coluna,m[linha-1][coluna].red);
             // printf("m[%d][%d] = %f\n",linha-1,coluna+1,m[linha-1][coluna+1].red);
             // printf("m[%d][%d] = %f\n",linha-1,coluna-1,m[linha-1][coluna-1].red);
	     // printf("linha = %d coluna = %d Full[0] = %f\n",linha,coluna,full[0]);	

	}

*/

       
	          x = _mm_setr_ps(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                  y = _mm_setr_ps(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                  z = _mm_setr_ps(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);		
		  x1 = _mm_setr_ps(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                  y1 = _mm_setr_ps(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
                  z1 = _mm_setr_ps(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                  x2 = _mm_setr_ps(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                  y2 = _mm_setr_ps(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                  z2 = _mm_setr_ps(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);

		  full = _mm_add_ps(x,_mm_add_ps(y,z));
		  full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
		  full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
		  full3 =  _mm_add_ps(full,_mm_add_ps(full1,full2));
		  full3 = _mm_div_ps(full3,divisao);


	         p.red = full3[0];
	         p.green = full3[1];
	         p.blue = full3[2];
	         p.alpha = full3[3];
	         mTrans[linha][coluna] = p;
		
	}
}
        
	

void transformarMatriz(pix **m,pix **mTrans)
{
	int i;
	int j;
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)

		{
			funcAux(m,mTrans,i,j);
		}
	}
}

pix **alocarMatriz()
{
	int i;
	pix **p;
		p = (pix **) malloc(M * sizeof(pix *)); 
	
	//p = (pix **) _mm_malloc(M*sizeof(pix *),16);

	for(i=0;i<M;i++)
	{
		p[i] = (pix *) malloc(N * sizeof(pix));
	//        p[i] = (pix *) _mm_malloc(N * sizeof(pix),16);
	}
	
	return p;
}


void MontarMatriz(pix **m)
{
	int i;
	int j;
	pix p;
	
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			p.red = rand() % 256;
			p.green = rand() % 256;
			p.blue = rand() % 256;
			p.alpha = rand() % 256;
			m[i][j] = p;
		}
	}
	
}

void imprimirMatriz(pix **m)
{
	int i;
	int j;
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			printf("Matriz RED[%d][%d] = %f\n",i,j,m[i][j].red);
			//printf("Matriz GREEN[%d][%d] = %f\n",i,j,m[i][j].green);
			//printf("Matriz BLUE[%d][%d] = %f\n",i,j,m[i][j].blue);
			//printf("Matriz ALPHA[%d][%d] = %f\n",i,j,m[i][j].alpha);
		}			
	}	
}
