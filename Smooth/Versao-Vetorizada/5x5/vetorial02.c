nclude <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "emmintrin.h"
#include "xmmintrin.h"
#define M 5
#define N 5


typedef struct  pixel
{
     unsigned char red;
     unsigned char green;
     unsigned char blue;
     unsigned char alpha;
    
}pix;

pix **alocarMatriz();
void MontarMatriz(pix **m);
void imprimirMatriz(pix **m);

int main()
{

/*
	int i;
	char v[32] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,28,19,20,21,22,23,24,25,26,27,28,29,30,31};
	char v2[32] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,28,19,20,21,22,23,24,25,26,27,28,29,30,31};
	char v3[32];
	char v4[16] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
	__m128i *av, *bv, *cv;
	__m128i x,y,z;
	

	
	av = (__m128i*)v3; 
    bv = (__m128i*)v2; 
	cv = (__m128i*)v; 
	
	
	
	
	for (i = 0; i < N/16; i++)
	{
		__m128i br = _mm_loadu_si128(&cv[i]);
  		__m128i cr = _mm_loadu_si128(&bv[i]);
  		__m128i ar = _mm_add_epi8(br, cr);
  		//_mm_storeu_si128(&av[i], ar);
		char c = br[2];
		char c2 = ar[0];
		printf("c = %d\n",c);
		printf("c2 = %d\n",c2);
	}
*/	

	  //printf("v3[15] = %d\n",v3[2]);
/*
	__m128i *load;
	pix **p = alocarMatriz();
	MontarMatriz(p);
	imprimirMatriz(p);
//	pix *teste = &p[0][0];

	load = (__m128i*) &p[0][0];
	printf("endereco de p[0][0] = %d\n",&p[0][0].red);
	printf("endereco que aponta load = %d\n",load); 
	printf("endereco de load = %d\n",&load); 
		
	
	__m128i teste = _mm_loadu_si128(&load[0]);
	unsigned char string[16];
	__m128i *teste2 = (__m128i *)string;
	_mm_storeu_si128(&teste2[0],teste);

	
	int i;

	for(i=0;i<16;i++)
	{
		string[i] = teste[i];
	}

	for(i=0;i<16;i++)
	{
		printf("string[%d] = %d\n",i,string[i]);
	}	

*/


	__m128i *load;
	__m128i*load2;
	pix **p = alocarMatriz();
	MontarMatriz(p);
	imprimirMatriz(p);
	//	pix *teste = &p[0][0];

	load = (__m128i*) &p[0][0];
	load2 = (__m128i*) &p[1][0];
	//printf("endereco de p[0][0] = %d\n",&p[0][0].red);
	//printf("endereco que aponta load = %d\n",load); 
	//printf("endereco de load = %d\n",&load); 
		
	
	__m128i teste = _mm_loadu_si128(&load[0]);
	__m128i teste2 = _mm_loadu_si128(&load2[0]);
	__m128i teste3 = _mm_add_epi8(teste,teste2);
	unsigned char string[16];
	unsigned char string2[16];
	__m128i *vetor = (__m128i *)string;	
	__m128i *vetor2 = (__m128i *)string2;		
	_mm_storeu_si128(&vetor[0],teste3);
	_mm_storeu_si128(&vetor2[0],teste2);
	
	int i;
/*
	for(i=0;i<16;i++)
	{
		string[i] = teste[i];
	}
*/
	for(i=0;i<16;i++)
	{
		//printf("string[%d] = %d\n",i,string2[i]);
		printf("string[%d] = %d\n",i,string[i]);
	}






	
	//unsigned char *c = &teste[0];
	//printf("c = %d\n",c);
/*	
	printf("teste[0] = %d\n",&teste[0]);
	printf("teste[1] = %d\n",&teste[1]);
	printf("teste[2] = %d\n",&teste[2]);
	printf("teste[3] = %d\n",&teste[3]);
	printf("teste[4] = %d\n",&teste[4]);
	printf("teste[5] = %d\n",&teste[5]);
	printf("teste[6] = %d\n",&teste[6]);
	printf("teste[7] = %d\n",&teste[7]);
	printf("teste[8] = %d\n",&teste[8]);
	printf("teste[9] = %d\n",&teste[9]);
	printf("teste[10] = %d\n",&teste[10]);
	printf("teste[11] = %d\n",&teste[11]);
	printf("teste[12] = %d\n",&teste[12]);
	printf("teste[13] = %d\n",&teste[13]);
	printf("teste[14] = %d\n",&teste[14]);
	printf("teste[15] = %d\n",&teste[15]);
	//_mm_storeu_si128(av,)
*/
	
//	printf("Endereco de p[0][0] = %d\n",p[0][0].red);
//	teste->red = 20;
//	printf("Endereco de p[0][0] = %d\n",p[0][0].red);
	//av = (__m128i*) &p[0][0];
	//printf("Endereco de av = %d\n",av);

	
	return 0;
}

pix **alocarMatriz()
{
    int i;
    pix **p;
    p = (pix **) malloc(M * sizeof(pix *));
    
    //p = (pix **) _mm_malloc(M*sizeof(pix *),16);
    
    for(i=0;i<M;i++)
    {
        p[i] = (pix *) malloc(N * sizeof(pix));
        //        p[i] = (pix *) _mm_malloc(N * sizeof(pix),16);
    }
    
    return p;
}

void MontarMatriz(pix **m)
{
    int i;
    int j;
    pix p;
    
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
        {
            p.red = rand() % 256;
            p.green = rand() % 256;
            p.blue = rand() % 256;
            p.alpha = rand() % 256;
            m[i][j] = p;
        }
    }
    
}

void imprimirMatriz(pix **m)
{
    int i;
    int j;
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
        {
            printf("Matriz RED[%d][%d] = %d\n",i,j,m[i][j].red);
        	printf("Matriz GREEN[%d][%d] = %d\n",i,j,m[i][j].green);
           	printf("Matriz BLUE[%d][%d] = %d\n",i,j,m[i][j].blue);
           	printf("Matriz ALPHA[%d][%d] = %d\n",i,j,m[i][j].alpha);
        }			
    }	
}
