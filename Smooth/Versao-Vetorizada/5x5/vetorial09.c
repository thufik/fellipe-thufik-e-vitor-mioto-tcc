#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "emmintrin.h"
#define M 5
#define N 5


typedef struct  pixel
{
    int red;
    int green;
    int blue;
    int alpha;
    
}pix;

//__m128i divisao;


pix **alocarMatriz(); //aloca uma matriz que contem em cada posiç uma structt
void MontarMatriz(pix **m); //poe em cada posiç da matriz uma struct,gerando numeros aleatorios de 0 a 255 para o RGBA
void imprimirMatriz(pix **m);

void transformarMatriz(pix **m,pix **mTrans);
void funcAux(pix **m,pix **mTrans,int linha,int coluna);

int contador = 0;


int main()
{
    time_t start,stop;
    double avg_time = 0;
    double cur_time;
    pix **p = alocarMatriz(); //matriz de structs
    pix **vTrans = alocarMatriz();
    MontarMatriz(p);
    imprimirMatriz(p);
    start = clock();
    transformarMatriz(p,vTrans);
    stop = clock();
    cur_time = ((double) stop-start) / CLOCKS_PER_SEC;
    printf("Averagely used %f seconds.\n", cur_time);
    //	printf("Contador = %d\n",contador);
    imprimirMatriz(vTrans);
    return 0;
    
}

void funcAux(pix **m,pix **mTrans,int linha,int coluna)
{
    int i;
    pix p;
    int soma[4];
    
    
    if(linha <= 1 || coluna <= 1 || linha >= (M-2) || coluna >= (N-2))
    {
        
        
	       if(linha == 0)
           {
               
               if(coluna == 0)
               {
                   
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
                   x1 = _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   y1 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
                   z1 = _mm_setr_epi32(m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);
                   x2 = _mm_setr_epi32(m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha);
                   y2 = _mm_setr_epi32(m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);
                   z2 = _mm_setr_epi32(m[linha+2][coluna+2].red,m[linha+2][coluna+2].green,m[linha+2][coluna+2].blue,m[linha+2][coluna+2].alpha);
                                              
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   _mm_storeu_si128((__m128i*)soma,full3);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }

               
               else if(coluna == 1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   

                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
                   x1 = _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   y1 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
                   z1 = _mm_setr_epi32(m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);
                   x2 = _mm_setr_epi32(m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha);
                   y2 = _mm_setr_epi32(m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);
                   z2 = _mm_setr_epi32(m[linha+2][coluna+2].red,m[linha+2][coluna+2].green,m[linha+2][coluna+2].blue,m[linha+2][coluna+2].alpha);
                   x3 = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   y3 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                   z3 = _mm_setr_epi32(m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha);
                                             
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full5 = _mm_add_epi32(full3,full4);
                   _mm_storeu_si128((__m128i*)soma,full5);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
               }
               else if(coluna == N-1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);
                   x1 = _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   y1 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                   z1 = _mm_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);
                   x2 = _mm_setr_epi32(m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha);
                   y2 = _mm_setr_epi32(m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha);
                   z2 = _mm_setr_epi32(m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha);
                   
                                              
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   _mm_storeu_si128((__m128i*)soma,full3);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == N-2)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   
                   x =  _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y =  _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   z =  _mm_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);
                   x1 =  _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   y1 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                   z1 = _mm_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);
                   x2 = _mm_setr_epi32(m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha);
                   y2 = _mm_setr_epi32(m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha);
                   z2 = _mm_setr_epi32(m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha);
                   x3 = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   y3 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
                   z3 = _mm_setr_epi32(m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);
                   
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full5 = _mm_add_epi32(full3,full4);
                   _mm_storeu_si128((__m128i*)soma,full5);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;
                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);
                   x1 = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   y1 = _mm_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
                   z1 =  _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   x2 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                   y2 = _mm_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);
                   z2 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
                   x3 = _mm_setr_epi32(m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);
                   y3 = _mm_setr_epi32(m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha);
                   z3 = _mm_setr_epi32(m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha);
                   x4 = _mm_setr_epi32(m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha);
                   y4 = _mm_setr_epi32(m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);
                   z4 = _mm_setr_epi32(m[linha+2][coluna+2].red,m[linha+2][coluna+2].green,m[linha+2][coluna+2].blue,m[linha+2][coluna+2].alpha);
                   
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(x4,_mm_add_epi32(y4,z4));
                   full5 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full6 = _mm_add_epi32(full3,full4);
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               
           }
           else if(linha == 1)
           {
               
               if(coluna == 0)
               {
                   
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
                   x1 =  _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   y1 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
                   z1 = _mm_setr_epi32(m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);
                   x2 = _mm_setr_epi32(m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha);
                   y2 = _mm_setr_epi32(m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);
                   z2 = _mm_setr_epi32(m[linha+2][coluna+2].red,m[linha+2][coluna+2].green,m[linha+2][coluna+2].blue,m[linha+2][coluna+2].alpha);
                   x3 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   y3 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                   z3 = _mm_setr_epi32(m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full5 = _mm_add_epi32(full3,full4);
                   _mm_storeu_si128((__m128i*)soma,full5);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == 1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   
                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
                   x1 = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   y1 =  _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   z1 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
                   x2 = _mm_setr_epi32(m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);
                   y2 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                   z2 = _mm_setr_epi32(m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha);
                   x3 = _mm_setr_epi32(m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);
                   y3 = _mm_setr_epi32(m[linha+2][coluna+2].red,m[linha+2][coluna+2].green,m[linha+2][coluna+2].blue,m[linha+2][coluna+2].alpha);
                   z3 = _mm_setr_epi32(m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha);
                   x4 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   y4 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                   z4 = _mm_setr_epi32(m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);
                   x5 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);

                   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(full,full1);
                   full5 = _mm_add_epi32(full2,full3);
                   full6 = _mm_add_epi32(full4,full5);
                   _mm_storeu_si128((__m128i*)soma,full6);
                                    
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
               }
               else if(coluna == N-1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;

                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);
                   x1 =  _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   y1 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                   z1 = _mm_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);
                   x2 = _mm_setr_epi32(m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha);
                   y2 = _mm_setr_epi32(m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha);
                   z2 = _mm_setr_epi32(m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha);
                   x3 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   y3 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
                   z3 = _mm_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,full1);
                   full5 = _mm_add_epi32(full2,full3);
                   full6 = _mm_add_epi32(full4,full5);
                   _mm_storeu_si128((__m128i*)soma,full6);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == N-2)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);
                   x1 = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   y1 =  _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   z1 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                   x2 = _mm_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);
                   y2 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
                   z2 = _mm_setr_epi32(m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha);
                   x3 = _mm_setr_epi32(m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha);
                   y3 = _mm_setr_epi32(m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha);
                   z3 = _mm_setr_epi32(m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);
                   x4 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   y4 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
                   z4 = _mm_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);
                   x5 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);

                   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(full,full1);
                   full5 = _mm_add_epi32(full2,full3);
                   full6 = _mm_add_epi32(full4,full5);
                   _mm_storeu_si128((__m128i*)soma,full6);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
                   
               }
               else
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   __m128i y5;
                   __m128i z5;
                   __m128i x6;
                   __m128i y6; 
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;
                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);
                   x1 = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   y1 = _mm_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
                   z1 =  _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   x2 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                   y2 = _mm_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);
                   z2 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
                   x3 = _mm_setr_epi32(m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);
                   y3 = _mm_setr_epi32(m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha);
                   z3 = _mm_setr_epi32(m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha);
                   x4 = _mm_setr_epi32(m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha);
                   y4 = _mm_setr_epi32(m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);
                   z4 = _mm_setr_epi32(m[linha+2][coluna+2].red,m[linha+2][coluna+2].green,m[linha+2][coluna+2].blue,m[linha+2][coluna+2].alpha);
                   x5 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   y5 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
                   z5 = _mm_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);
                   x6 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                   y6 = _mm_setr_epi32(m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);
                   
                   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(_mm_add_epi32(y5,y6),_mm_add_epi32(z5,x6));
                   full5 = _mm_add_epi32(full,full1);
                   full6 = _mm_add_epi32(full2,_mm_add_epi32(full3,full4));
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               
           }
           else if(linha == M-2)
	   {
		if(coluna == 0)
               {
                   
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   x = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
                   x1 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   y1 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                   z1 = _mm_setr_epi32(m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);
                   x2 = _mm_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha);
                   y2 = _mm_setr_epi32(m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);
                   z2 = _mm_setr_epi32(m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha);
                   x3 = _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   y3 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
                   z3 = _mm_setr_epi32(m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full5 = _mm_add_epi32(full3,full4);
                   _mm_storeu_si128((__m128i*)soma,full5);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == 1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   
                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
                   x1 = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   y1 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   z1 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                   x2 = _mm_setr_epi32(m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);
                   y2 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
                   z2 = _mm_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha);
                   x3 = _mm_setr_epi32(m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);
                   y3 = _mm_setr_epi32(m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha);
                   z3 = _mm_setr_epi32(m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha);
                   x4 = _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   y4 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
                   z4 = _mm_setr_epi32(m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);
                   x5 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);

                   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(full,full1);
                   full5 = _mm_add_epi32(full2,full3);
                   full6 = _mm_add_epi32(full4,full5);
                   _mm_storeu_si128((__m128i*)soma,full6);
                                    
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
               }
               else if(coluna == N-1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;

                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);
                   x1 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   y1 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
                   z1 = _mm_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);
                   x2 = _mm_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha);
                   y2 = _mm_setr_epi32(m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha);
                   z2 = _mm_setr_epi32(m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha);
                   x3 = _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   y3 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                   z3 = _mm_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,full1);
                   full5 = _mm_add_epi32(full2,full3);
                   full6 = _mm_add_epi32(full4,full5);
                   _mm_storeu_si128((__m128i*)soma,full6);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == N-2)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);
                   x1 = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   y1 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   z1 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
                   x2 = _mm_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);
                   y2 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                   z2 = _mm_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha);
                   x3 = _mm_setr_epi32(m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha);
                   y3 = _mm_setr_epi32(m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha);
                   z3 = _mm_setr_epi32(m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);
                   x4 =  _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   y4 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                   z4 = _mm_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);
                   x5 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);

                   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(full,full1);
                   full5 = _mm_add_epi32(full2,full3);
                   full6 = _mm_add_epi32(full4,full5);
                   _mm_storeu_si128((__m128i*)soma,full6);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
                   
               }
               else
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   __m128i y5;
                   __m128i z5;
                   __m128i x6;
                   __m128i y6; 
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;
                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);
                   x1 = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   y1 = _mm_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
                   z1 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   x2 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
                   y2 = _mm_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);
                   z2 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                   x3 = _mm_setr_epi32(m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);
                   y3 = _mm_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha);
                   z3 = _mm_setr_epi32(m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha);
                   x4 = _mm_setr_epi32(m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha);
                   y4 = _mm_setr_epi32(m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);
                   z4 = _mm_setr_epi32(m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha);
                   x5 =  _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   y5 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                   z5 = _mm_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);
                   x6 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
                   y6 = _mm_setr_epi32(m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);
                   
                   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(_mm_add_epi32(y5,y6),_mm_add_epi32(z5,x6));
                   full5 = _mm_add_epi32(full,full1);
                   full6 = _mm_add_epi32(full2,_mm_add_epi32(full3,full4));
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }

	   }
           else if(linha == M-1)
           {
               if(coluna == 0)
               {
                   
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;

                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
                   x1 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   y1 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                   z1 = _mm_setr_epi32(m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);
                   x2 = _mm_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha);
                   y2 = _mm_setr_epi32(m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);
                   z2 = _mm_setr_epi32(m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   _mm_storeu_si128((__m128i*)soma,full3);

                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;

               }
               else if(coluna == 1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;

                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;

                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
                   x1 = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   y1 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   z1 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                   x2 = _mm_setr_epi32(m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);
                   y2 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
                   z2 = _mm_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha);
                   x3 = _mm_setr_epi32(m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);
                   y3 = _mm_setr_epi32(m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha);
                   z3 = _mm_setr_epi32(m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full5 = _mm_add_epi32(full3,full4);
                   _mm_storeu_si128((__m128i*)soma,full5);

                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;

               }
               else if(coluna == N-1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);
                   x1 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   y1 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
                   z1 = _mm_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);
                   x2 = _mm_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha);
                   y2 = _mm_setr_epi32(m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha);
                   z2 = _mm_setr_epi32(m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   _mm_storeu_si128((__m128i*)soma,full3);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;                   
                 
                   
               }
               else if(coluna == N-2)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);
                   x1 = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   y1 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   z1 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
                   x2 = _mm_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);
                   y2 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                   z2 = _mm_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha);
                   x3 = _mm_setr_epi32(m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha);
                   y3 = _mm_setr_epi32(m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha);
                   z3 = _mm_setr_epi32(m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full5 = _mm_add_epi32(full3,full4);
                   _mm_storeu_si128((__m128i*)soma,full5);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
           
               }
               else
               {
                                                                                                        
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;
                   
                   x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   z = _mm_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);
                   x1 = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   y1 = _mm_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
                   z1 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   x2 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
                   y2 = _mm_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);
                   z2 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                   x3 = _mm_setr_epi32(m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);
                   y3 = _mm_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha);
                   z3 = _mm_setr_epi32(m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha);
                   x4 = _mm_setr_epi32(m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha);
                   y4 = _mm_setr_epi32(m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);
                   z4 = _mm_setr_epi32(m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(x4,_mm_add_epi32(y4,z4));
                   full5 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full6 = _mm_add_epi32(full3,full4);
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;                  
                   
               }
               
           }
           else
           {
               if(coluna == 0)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;

                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;
                   
                   x = _mm_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha);
                   y = _mm_setr_epi32(m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);
                   z = _mm_setr_epi32(m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha);
                   x1 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   y1 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
                   z1 = _mm_setr_epi32(m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);
                   x2 = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y2 = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   z2 = _mm_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
                   x3 =  _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   y3 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
                   z3 = _mm_setr_epi32(m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);
                   x4 = _mm_setr_epi32(m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha);
                   y4 = _mm_setr_epi32(m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);
                   z4 = _mm_setr_epi32(m[linha+2][coluna+2].red,m[linha+2][coluna+2].green,m[linha+2][coluna+2].blue,m[linha+2][coluna+2].alpha);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(x4,_mm_add_epi32(y4,z4));
                   full5 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full6 = _mm_add_epi32(full3,full4);
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);


                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
	       else if(coluna == 1)
	       {
		
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   __m128i y5;
                   __m128i z5;
                   __m128i x6;
                   __m128i y6;

                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;

               x = _mm_setr_epi32(m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha);
               y = _mm_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha);
               z = _mm_setr_epi32(m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);
               x1 = _mm_setr_epi32(m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha);
               y1 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
               z1 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
               x2 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
               y2 = _mm_setr_epi32(m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);
               z2 = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
               x3 = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
               y3 = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
               z3 = _mm_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
               x4 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
               y4 =  _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
               z4 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
               x5 = _mm_setr_epi32(m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);
               y5 = _mm_setr_epi32(m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha);
               z5 = _mm_setr_epi32(m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha);
               x6 = _mm_setr_epi32(m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);
               y6 = _mm_setr_epi32(m[linha+2][coluna+2].red,m[linha+2][coluna+2].green,m[linha+2][coluna+2].blue,m[linha+2][coluna+2].alpha);

                   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(_mm_add_epi32(y5,y6),_mm_add_epi32(z5,x6));
                   full5 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full6 = _mm_add_epi32(full3,full4);
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);

                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;


	       }
               else if(coluna == M-1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;

                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;

                   z = _mm_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha);
                   y = _mm_setr_epi32(m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha);
                   z = _mm_setr_epi32(m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha);
                   x1 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   y1 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
                   z1 = _mm_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);
                   x2 = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
                   y2 = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
                   z2 = _mm_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);
                   x3 =  _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   y3 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
                   z3 = _mm_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);
                   x4 = _mm_setr_epi32(m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha);
                   x4 = _mm_setr_epi32(m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha);
                   z4 = _mm_setr_epi32(m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(x4,_mm_add_epi32(y4,z4));
                   full5 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full6 = _mm_add_epi32(full3,full4);
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);


                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
         
                   
               }
	       else if(coluna == M-2)
	       {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   __m128i y5;
                   __m128i z5;
                   __m128i x6;
                   __m128i y6;

                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;

               x = _mm_setr_epi32(m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);
               y = _mm_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha);
               z = _mm_setr_epi32(m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha);
               x1 = _mm_setr_epi32(m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha);
               y1 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
               z1 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
               x2 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
               y2 = _mm_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);
               z2 = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
               x3 = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
               y3 = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
               z3 = _mm_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);
               x4 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
               y4 =  _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
               z4 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
               x5 = _mm_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);
               y5 = _mm_setr_epi32(m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);
               z5 = _mm_setr_epi32(m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha);
               x6 = _mm_setr_epi32(m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha);
               y6 = _mm_setr_epi32(m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha);

                   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(_mm_add_epi32(y5,y6),_mm_add_epi32(z5,x6));
                   full5 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full6 = _mm_add_epi32(full3,full4);
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);

                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;

               }
               
           }
        
    }
    else
    {
        __m128i x;
        __m128i y;
        __m128i z;
        __m128i x1;
        __m128i y1;
        __m128i z1;
        __m128i x2;
        __m128i y2;
        __m128i z2;
        __m128i x3;
        __m128i y3;
        __m128i z3;
        __m128i x4;
        __m128i y4;
        __m128i z4;
        __m128i x5;
        __m128i y5;
        __m128i z5;
        __m128i x6;
        __m128i y6;
        __m128i z6;
        __m128i x7;
        __m128i y7;
        __m128i z7;
        __m128i x8;
        __m128i full;
        __m128i full1;
        __m128i full2;
        __m128i full3;
        __m128i full4;
        __m128i full5;
        __m128i full6;
        __m128i full7;

        x = _mm_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);
        y = _mm_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
        z = _mm_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
        x1 = _mm_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);
        y1 = _mm_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);
        z1 =  _mm_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
        x2 = _mm_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);
        y2 = _mm_setr_epi32(m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);
        z2 = _mm_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);
        x3 = _mm_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);
        y3 = _mm_setr_epi32(m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha);
        z3 = _mm_setr_epi32(m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);
        x4 = _mm_setr_epi32(m[linha+2][coluna+2].red,m[linha+2][coluna+2].green,m[linha+2][coluna+2].blue,m[linha+2][coluna+2].alpha);
        y4 = _mm_setr_epi32(m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha);
        z4 = _mm_setr_epi32(m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha);
        x5 = _mm_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
        y5 = _mm_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);
        z5 = _mm_setr_epi32(m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);
        x6 = _mm_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);
        y6 = _mm_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);
        z6 = _mm_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha);
        x7 = _mm_setr_epi32(m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);
        y7 = _mm_setr_epi32(m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha);
        z7 = _mm_setr_epi32(m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha);
        x8 = _mm_setr_epi32(m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha);

            full = _mm_add_epi32(y1,_mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z)));
            full1 = _mm_add_epi32(z1,_mm_add_epi32(_mm_add_epi32(x2,y2),_mm_add_epi32(z2,x3)));
            full2 = _mm_add_epi32(y3,_mm_add_epi32(_mm_add_epi32(z3,x4),_mm_add_epi32(y4,z4)));            
            full3 = _mm_add_epi32(x5,_mm_add_epi32(_mm_add_epi32(y5,z5),_mm_add_epi32(x6,y6)));            
            full4 = _mm_add_epi32(z6,_mm_add_epi32(_mm_add_epi32(x7,y7),_mm_add_epi32(z7,x8)));
            full5 =  _mm_add_epi32(full,_mm_add_epi32(full1,full2));
            full6 =  _mm_add_epi32(full3,full4);
            full7 =  _mm_add_epi32(full5,full6);
            _mm_storeu_si128((__m128i*)soma,full7);
        
        
            p.red = soma[0]/25;
            p.green = soma[1]/25;
            p.blue = soma[2]/25;
            p.alpha = soma[3]/25;
            mTrans[linha][coluna] = p;
 
        
    }
}



void transformarMatriz(pix **m,pix **mTrans)
{
    int i;
    int j;
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
            
        {
            funcAux(m,mTrans,i,j);
        }
    }
}

pix **alocarMatriz()
{
    int i;
    pix **p;
    p = (pix **) malloc(M * sizeof(pix *));
    
    //p = (pix **) _mm_malloc(M*sizeof(pix *),16);
    
    for(i=0;i<M;i++)
    {
        p[i] = (pix *) malloc(N * sizeof(pix));
        //        p[i] = (pix *) _mm_malloc(N * sizeof(pix),16);
    }
    
    return p;
}


void MontarMatriz(pix **m)
{
    int i;
    int j;
    pix p;
    
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
        {
            p.red = rand() % 256;
            p.green = rand() % 256;
            p.blue = rand() % 256;
            p.alpha = rand() % 256;
            m[i][j] = p;
        }
    }
    
}

void imprimirMatriz(pix **m)
{
    int i;
    int j;
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
        {
            printf("Matriz RED[%d][%d] = %d\n",i,j,m[i][j].red);
            //	printf("Matriz GREEN[%d][%d] = %f\n",i,j,m[i][j].green);
            //	printf("Matriz BLUE[%d][%d] = %f\n",i,j,m[i][j].blue);
            //	printf("Matriz ALPHA[%d][%d] = %f\n",i,j,m[i][j].alpha);
        }			
    }	
}
