#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "emmintrin.h"
#define M 5
#define N 5


typedef struct  pixel
{
    int red;
    int green;
    int blue;
    int alpha;
    
}pix;

//__m128i divisao;


pix **alocarMatriz(); //aloca uma matriz que contem em cada posiç uma structt
void MontarMatriz(pix **m); //poe em cada posiç da matriz uma struct,gerando numeros aleatorios de 0 a 255 para o RGBA
void imprimirMatriz(pix **m);

void transformarMatriz(pix **m,pix **mTrans);
void funcAux(pix **m,pix **mTrans,int linha,int coluna);

int contador = 0;


int main()
{
    time_t start,stop;
    double avg_time = 0;
    double cur_time;
    pix **p = alocarMatriz(); //matriz de structs
    pix **vTrans = alocarMatriz();
    MontarMatriz(p);
    imprimirMatriz(p);
    start = clock();
    transformarMatriz(p,vTrans);
    stop = clock();
    cur_time = ((double) stop-start) / CLOCKS_PER_SEC;
    printf("Averagely used %f seconds.\n", cur_time);
    //	printf("Contador = %d\n",contador);
    imprimirMatriz(vTrans);
    return 0;
    
}

void funcAux(pix **m,pix **mTrans,int linha,int coluna)
{
    int i;
    pix p;
    int soma[4];
    
    
    if(linha <= 1 || coluna <= 1 || linha >= (M-2) || coluna >= (N-2))
    {
        
        
	       if(linha == 0)
           {
               
               if(coluna == 0)
               {
                   
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+2].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+2].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   _mm_storeu_si128((__m128i*)soma,full3);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }

               
               else if(coluna == 1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+2].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+1].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+2].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-1].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full5 = _mm_add_epi32(full3,full4);
                   _mm_storeu_si128((__m128i*)soma,full5);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
               }
               else if(coluna == N-1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-2].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-2].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   _mm_storeu_si128((__m128i*)soma,full3);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == N-2)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-2].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-1].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-2].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+1].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full5 = _mm_add_epi32(full3,full4);
                   _mm_storeu_si128((__m128i*)soma,full5);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
                   
               }
               else
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+2].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-2].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+2].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-1].red);
                   x4 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-2].red);
                   y4 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+1].red);
                   z4 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+2].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(x4,_mm_add_epi32(y4,z4));
                   full5 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full6 = _mm_add_epi32(full3,full4);
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               
           }
           else if(linha == 1)
           {
               
               if(coluna == 0)
               {
                   
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+2].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+2].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+2].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full5 = _mm_add_epi32(full3,full4);
                   _mm_storeu_si128((__m128i*)soma,full5);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == 1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+2].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+1].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+2].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-1].red);
                   x4 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   y4 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);
                   z4 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+2].red);
                   x5 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);

		   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(full,full1);
                   full5 = _mm_add_epi32(full2,full3);
                   full6 = _mm_add_epi32(full4,full5);
                   _mm_storeu_si128((__m128i*)soma,full6);
                                    
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
               }
               else if(coluna == N-1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;

                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-2].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-2].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-2].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,full1);
                   full5 = _mm_add_epi32(full2,full3);
                   full6 = _mm_add_epi32(full4,full5);
                   _mm_storeu_si128((__m128i*)soma,full6);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == N-2)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-2].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-1].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-2].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+1].red);
                   x4 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   y4 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);
                   z4 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-2].red);
                   x5 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);

                   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(full,full1);
                   full5 = _mm_add_epi32(full2,full3);
                   full6 = _mm_add_epi32(full4,full5);
                   _mm_storeu_si128((__m128i*)soma,full6);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
                   
               }
               else
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   __m128i y5;
                   __m128i z5;
                   __m128i x6;
                   __m128i y6; 
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+2].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-2].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+2].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-1].red);
                   x4 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-2].red);
                   y4 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+1].red);
                   z4 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+2].red);
                   x5 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   y5 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);
                   z5 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-2].red);
                   x6 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);
                   y6 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+2].red);
                   
                   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(_mm_add_epi32(y5,y6),_mm_add_epi32(z5,x6));
                   full5 = _mm_add_epi32(full,full1);
                   full6 = _mm_add_epi32(full2,_mm_add_epi32(full3,full4));
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               
           }
           else if(linha == M-2)
	   {
		if(coluna == 0)
               {
                   
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+2].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+2].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+2].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full5 = _mm_add_epi32(full3,full4);
                   _mm_storeu_si128((__m128i*)soma,full5);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == 1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+2].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+1].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+2].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-1].red);
                   x4 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   y4 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);
                   z4 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+2].red);
                   x5 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);

                   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(full,full1);
                   full5 = _mm_add_epi32(full2,full3);
                   full6 = _mm_add_epi32(full4,full5);
                   _mm_storeu_si128((__m128i*)soma,full6);
                                    
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
               }
               else if(coluna == N-1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;

                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-2].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-2].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-2].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,full1);
                   full5 = _mm_add_epi32(full2,full3);
                   full6 = _mm_add_epi32(full4,full5);
                   _mm_storeu_si128((__m128i*)soma,full6);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == N-2)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
		   __m128i full6;
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-2].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-1].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-2].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+1].red);
                   x4 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   y4 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);
                   z4 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-2].red);
                   x5 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);

                   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(full,full1);
                   full5 = _mm_add_epi32(full2,full3);
                   full6 = _mm_add_epi32(full4,full5);
                   _mm_storeu_si128((__m128i*)soma,full6);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
                   
               }
               else
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   __m128i y5;
                   __m128i z5;
                   __m128i x6;
                   __m128i y6; 
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+2].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-2].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+2].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-1].red);
                   x4 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-2].red);
                   y4 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+1].red);
                   z4 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+2].red);
                   x5 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   y5 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);
                   z5 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-2].red);
                   x6 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);
                   y6 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+2].red);
                   
                   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(_mm_add_epi32(y5,y6),_mm_add_epi32(z5,x6));
                   full5 = _mm_add_epi32(full,full1);
                   full6 = _mm_add_epi32(full2,_mm_add_epi32(full3,full4));
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }

	   }
           else if(linha == M-1)
           {
               if(coluna == 0)
               {
                   
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;

                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+2].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+2].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   _mm_storeu_si128((__m128i*)soma,full3);

                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;

               }
               else if(coluna == 1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;

                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;

                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+2].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+1].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+2].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-1].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full5 = _mm_add_epi32(full3,full4);
                   _mm_storeu_si128((__m128i*)soma,full5);

                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;

               }
               else if(coluna == N-1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-2].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-2].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   _mm_storeu_si128((__m128i*)soma,full3);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;                   
                 
                   
               }
               else if(coluna == N-2)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-2].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-1].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-2].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+1].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full5 = _mm_add_epi32(full3,full4);
                   _mm_storeu_si128((__m128i*)soma,full5);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
           
               }
               else
               {
                                                                                                        
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   
                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+2].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-2].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+2].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-1].red);
                   x4 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-2].red);
                   y4 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+1].red);
                   z4 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+2].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(x4,_mm_add_epi32(y4,z4));
                   full5 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full6 = _mm_add_epi32(full3,full4);
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);
                   
                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;                  
                   
               }
               
           }
           else
           {
               if(coluna == 0)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;

                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;
                   
                   x = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+2].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+2].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+2].red);
                   x4 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna].red);
                   y4 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+1].red);
                   z4 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+2].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(x4,_mm_add_epi32(y4,z4));
                   full5 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full6 = _mm_add_epi32(full3,full4);
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);


                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
	       else if(coluna == 1)
	       {
		
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   __m128i y5;
                   __m128i z5;
                   __m128i x6;
                   __m128i y6;

                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;

                   x = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-1].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+1].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+2].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+2].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+2].red);
                   x4 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);
                   y4 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   z4 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);
                   x5 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+2].red);
                   y5 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-1].red);
                   z5 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna].red);
                   x6 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+1].red);
                   y6 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+2].red);

                   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(_mm_add_epi32(y5,y6),_mm_add_epi32(z5,x6));
                   full5 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full6 = _mm_add_epi32(full3,full4);
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);

                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;


	       }
               else if(coluna == M-1)
               {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;

                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;

                   x = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-1].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-2].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-2].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-2].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-2].red);
                   x4 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna].red);
                   y4 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-1].red);
                   z4 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-2].red);
                   full = _mm_add_epi32(x,_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(x1,_mm_add_epi32(y1,z1));
                   full2 = _mm_add_epi32(x2,_mm_add_epi32(y2,z2));
                   full3 = _mm_add_epi32(x3,_mm_add_epi32(y3,z3));
                   full4 = _mm_add_epi32(x4,_mm_add_epi32(y4,z4));
                   full5 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full6 = _mm_add_epi32(full3,full4);
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);


                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;
         
                   
               }
	       else if(coluna == M-2)
	       {
                   __m128i x;
                   __m128i y;
                   __m128i z;
                   __m128i x1;
                   __m128i y1;
                   __m128i z1;
                   __m128i x2;
                   __m128i y2;
                   __m128i z2;
                   __m128i x3;
                   __m128i y3;
                   __m128i z3;
                   __m128i x4;
                   __m128i y4;
                   __m128i z4;
                   __m128i x5;
                   __m128i y5;
                   __m128i z5;
                   __m128i x6;
                   __m128i y6;

                   __m128i full;
                   __m128i full1;
                   __m128i full2;
                   __m128i full3;
                   __m128i full4;
                   __m128i full5;
                   __m128i full6;
                   __m128i full7;

                   x = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+1].red);
                   y = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna].red);
                   z = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-1].red);
                   x1 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-2].red);
                   y1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);
                   z1 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
                   x2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);
                   y2 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-2].red);
                   z2 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
                   x3 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
                   y3 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
                   z3 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-2].red);
                   x4 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);
                   y4 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
                   z4 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);
                   x5 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-2].red);
                   y5 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+1].red);
                   z5 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna].red);
                   x6 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-1].red);
                   y6 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-2].red);

                   full = _mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z));
                   full1 = _mm_add_epi32(_mm_add_epi32(y1,y2),_mm_add_epi32(z1,x2));
                   full2 = _mm_add_epi32(_mm_add_epi32(z2,z3),_mm_add_epi32(x3,y3));
                   full3 = _mm_add_epi32(_mm_add_epi32(x4,x5),_mm_add_epi32(y4,z4));
                   full4 = _mm_add_epi32(_mm_add_epi32(y5,y6),_mm_add_epi32(z5,x6));
                   full5 = _mm_add_epi32(full,_mm_add_epi32(full1,full2));
                   full6 = _mm_add_epi32(full3,full4);
                   full7 = _mm_add_epi32(full5,full6);
                   _mm_storeu_si128((__m128i*)soma,full7);

                   p.red = soma[0]/25;
                   p.green = soma[1]/25;
                   p.blue = soma[2]/25;
                   p.alpha = soma[3]/25;
                   mTrans[linha][coluna] = p;

               }
               
           }
        
    }
    else
    {
        __m128i x;
        __m128i y;
        __m128i z;
        __m128i x1;
        __m128i y1;
        __m128i z1;
        __m128i x2;
        __m128i y2;
        __m128i z2;
        __m128i x3;
        __m128i y3;
        __m128i z3;
        __m128i x4;
        __m128i y4;
        __m128i z4;
        __m128i x5;
        __m128i y5;
        __m128i z5;
        __m128i x6;
        __m128i y6;
        __m128i z6;
        __m128i x7;
        __m128i y7;
        __m128i z7;
        __m128i x8;
        __m128i full;
        __m128i full1;
        __m128i full2;
        __m128i full3;
        __m128i full4;
        __m128i full5;
        __m128i full6;
        __m128i full7;

            x = _mm_loadu_si128( (__m128i const*)&m[linha][coluna].red);
            y = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+1].red);
            z = _mm_loadu_si128( (__m128i const*)&m[linha][coluna+2].red);
            x1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-1].red);
            y1 = _mm_loadu_si128( (__m128i const*)&m[linha][coluna-2].red);
            z1 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna].red);
            x2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+1].red);
            y2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna+2].red);
            z2 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-1].red);
            x3 = _mm_loadu_si128( (__m128i const*)&m[linha+1][coluna-2].red);
            y3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna].red);
            z3 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+1].red);
            x4 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna+2].red);
            y4 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-1].red);
            z4 = _mm_loadu_si128( (__m128i const*)&m[linha+2][coluna-2].red);
            x5 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna].red);
            y5 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+1].red);
            z5 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna+2].red);
            x6 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-1].red);
            y6 = _mm_loadu_si128( (__m128i const*)&m[linha-1][coluna-2].red);
            z6 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna].red);
            x7 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+1].red);
            y7 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna+2].red);
            z7 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-1].red);
            x8 = _mm_loadu_si128( (__m128i const*)&m[linha-2][coluna-2].red);

            full = _mm_add_epi32(y1,_mm_add_epi32(_mm_add_epi32(x,x1),_mm_add_epi32(y,z)));
            full1 = _mm_add_epi32(z1,_mm_add_epi32(_mm_add_epi32(x2,y2),_mm_add_epi32(z2,x3)));
            full2 = _mm_add_epi32(y3,_mm_add_epi32(_mm_add_epi32(z3,x4),_mm_add_epi32(y4,z4)));            
            full3 = _mm_add_epi32(x5,_mm_add_epi32(_mm_add_epi32(y5,z5),_mm_add_epi32(x6,y6)));            
            full4 = _mm_add_epi32(z6,_mm_add_epi32(_mm_add_epi32(x7,y7),_mm_add_epi32(z7,x8)));
            full5 =  _mm_add_epi32(full,_mm_add_epi32(full1,full2));
            full6 =  _mm_add_epi32(full3,full4);
            full7 =  _mm_add_epi32(full5,full6);
            _mm_storeu_si128((__m128i*)soma,full7);
        
        
            p.red = soma[0]/25;
            p.green = soma[1]/25;
            p.blue = soma[2]/25;
            p.alpha = soma[3]/25;
            mTrans[linha][coluna] = p;
     
        
    }
}



void transformarMatriz(pix **m,pix **mTrans)
{
    int i;
    int j;
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
            
        {
            funcAux(m,mTrans,i,j);
        }
    }
}

pix **alocarMatriz()
{
    int i;
    pix **p;
    p = (pix **) malloc(M * sizeof(pix *));
    
    //p = (pix **) _mm_malloc(M*sizeof(pix *),16);
    
    for(i=0;i<M;i++)
    {
        p[i] = (pix *) malloc(N * sizeof(pix));
        //        p[i] = (pix *) _mm_malloc(N * sizeof(pix),16);
    }
    
    return p;
}


void MontarMatriz(pix **m)
{
    int i;
    int j;
    pix p;
    
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
        {
            p.red = rand() % 256;
            p.green = rand() % 256;
            p.blue = rand() % 256;
            p.alpha = rand() % 256;
            m[i][j] = p;
        }
    }
    
}

void imprimirMatriz(pix **m)
{
    int i;
    int j;
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
        {
            printf("Matriz RED[%d][%d] = %d\n",i,j,m[i][j].red);
            //	printf("Matriz GREEN[%d][%d] = %f\n",i,j,m[i][j].green);
            //	printf("Matriz BLUE[%d][%d] = %f\n",i,j,m[i][j].blue);
            //	printf("Matriz ALPHA[%d][%d] = %f\n",i,j,m[i][j].alpha);
        }			
    }	
}
