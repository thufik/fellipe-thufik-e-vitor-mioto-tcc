#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "immintrin.h"
#define M 5000
#define N 5000


typedef struct  pixel
{
    int red;
    int green;
    int blue;
    int alpha;
    
}pix;




pix **alocarMatriz(); //aloca uma matriz que contem em cada posiç uma structt
void MontarMatriz(pix m[][N]); //poe em cada posiç da matriz uma struct,gerando numeros aleatorios de 0 a 255 para o RGBA
void MontarMatrizTrans(pix m[][N]);

void transformarMatriz(pix m[][N],pix mTrans[][N]);

void imprimirMatriz(pix[][N]);


void funcAux(pix m[][N],pix mTrans[][N],int linha,int coluna);

 pix m[M][N] __attribute__((aligned(64)));
 pix vTrans[M][N];// __attribute__((aligned(64)));


int main()
{
    struct timeval start, end;
    double  delta = 0;
    //pix **m = alocarMatriz(); //matriz de structs
    // _attribute__((target(mic))) pix m[M][N];    
    //pix vTrans[M][N];
    MontarMatriz(m);
    //MontarMatrizTrans(vTrans);
    //imprimirMatriz(m);
    gettimeofday(&start, NULL);
    transformarMatriz(m,vTrans);
    gettimeofday(&end, NULL);
    delta = ((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6;
    printf("Averagely used %f seconds.\n", delta);
  //  printf("Contador = %ld\n",contador);
    //imprimirMatriz(vTrans);
    return 0;
    
}

void funcAux(pix m[][N],pix mTrans[][N],int linha,int coluna)
{
    int i;
    pix p;
    
    
    
    if(linha <= 1 || coluna <= 1 || linha >= (M-2) || coluna >= (N-2))
    {
    
    
	       if(linha == 0)
               {
               
               if(coluna == 0)
               {
                   
                   int vetor[16] __attribute__((aligned(64))); 
                   
		   __m512i load = _mm512_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha,m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
		   __m512i load2 = _mm512_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha,m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha,m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha,m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);
		
                  

                   __m512i soma = _mm512_add_epi32(load,load2);
                   _mm512_store_epi32(vetor,soma);

                   
                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12] + m[linha+2][coluna+2].red)/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13] + m[linha+2][coluna+2].green)/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14] + m[linha+2][coluna+2].blue)/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15] + m[linha+2][coluna+2].alpha)/25;
		   mTrans[linha][coluna] = p;
                   
               }         
               
               else if(coluna == 1)
               {
                
                   int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha,m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);
               
                   __m512i load2 = _mm512_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha,m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha,m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha,m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha,m[linha+2][coluna+2].red,m[linha+2][coluna+2].green,m[linha+2][coluna+2].blue,m[linha+2][coluna+2].alpha);

	           __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,load3));
                   _mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;

		    mTrans[linha][coluna] = p;                   
                   
               }

              
	        else if(coluna == N-1)
               {
                   
		    int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha);
                   __m512i load2 = _mm512_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha,m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha,m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha,m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha);



                   __m512i soma = _mm512_add_epi32(load,load2);
                   _mm512_store_epi32(vetor,soma);


                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12] + m[linha+2][coluna-2].red)/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13] + m[linha+2][coluna-2].green)/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14] + m[linha+2][coluna-2].blue)/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15] + m[linha+2][coluna-2].alpha)/25;
                   mTrans[linha][coluna] = p;
              }
               else if(coluna == N-2)
               {
	
                   int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna-1].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);

                   __m512i load2 = _mm512_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha,m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha,m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha,m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha,m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha);


		   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,load3));
                   _mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;
			
			mTrans[linha][coluna] = p;
	 
		}
               else
               {
           
		int vetor[16] __attribute__((aligned(64)));	

                   __m512i load = _mm512_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha,m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   __m512i load2 = _mm512_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha,m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha,m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha,m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha,m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);

        
                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,load3));
                   _mm512_store_epi32(vetor,soma);


                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12] + m[linha][coluna+2].red + m[linha+1][coluna+2].red + m[linha+2][coluna+2].red)/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13] + m[linha][coluna+2].green + m[linha+1][coluna+2].green + m[linha+2][coluna+2].green)/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14] + m[linha][coluna+2].blue + m[linha+1][coluna+2].blue + m[linha+2][coluna+2].blue)/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15] + m[linha][coluna+2].alpha + m[linha+1][coluna+2].alpha + m[linha+2][coluna+2].alpha)/25;
		mTrans[linha][coluna] = p;

               }

               
           }
           else if(linha == 1)
           {
               
               if(coluna == 0)
               {
              
		int vetor[16] __attribute__((aligned(64))); 
   
               __m512i load = _mm512_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha,m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);

              __m512i  load2 = _mm512_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha,m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][
coluna+1].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha,m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha,m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha,m[linha+2][coluna+2].red,m[linha+2][coluna+2].green,m[linha+2][coluna+2].blue,m[linha+2][coluna+2].alpha);
		
                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,load3));
                   _mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;

                        mTrans[linha][coluna] = p;


		}

		else if(coluna == 1)
               {

		int vetor[16] __attribute__((aligned(64)));                  
 
                   __m512i load = _mm512_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha,m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);

                   __m512i load2 = _mm512_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha,m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha,m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);

                   __m512i load4 = _mm512_setr_epi32(m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha,m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha,m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha,m[linha+2][coluna+2].red,m[linha+2][coluna+2].green,m[linha+2][coluna+2].blue,m[linha+2][coluna+2].alpha);	


                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,_mm512_add_epi32(load3,load4)));
                   _mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;

                        mTrans[linha][coluna] = p;
	
              }


               else if(coluna == N-1)
               {
               
 		int vetor[16] __attribute__((aligned(64)));

               __m512i load = _mm512_setr_epi32(m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha,m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);

              __m512i  load2 = _mm512_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha,m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha,m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha,m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha);

                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,load3));
                   _mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;

                        mTrans[linha][coluna] = p;   
               }

               else if(coluna == N-2)
               {
               

                int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha,m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);

                   __m512i load2 = _mm512_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha,m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);

                   __m512i load4 = _mm512_setr_epi32(m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha,m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha,m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha,m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha);


                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,_mm512_add_epi32(load3,load4)));
                   _mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;

                        mTrans[linha][coluna] = p;
    
               }
               else
               {
   

                int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha,m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);


                   __m512i load2 = _mm512_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha,m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha,m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);

                   __m512i load4 = _mm512_setr_epi32(m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha,m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha,m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha,m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);

                   __m512i load5 = _mm512_setr_epi32(m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha,m[linha][coluna+2].red,m[linha][coluna +2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha,m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha,m[linha+2][coluna+2].red,m[linha+2][coluna+2].green,m[linha+2][coluna+2].blue,m[linha+2][coluna+2].alpha);

                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,_mm512_add_epi32(load3,_mm512_add_epi32(load4,load5) )));
		_mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;
		 mTrans[linha][coluna] = p;
 
               }

           }
           else if(linha == M-2)
	   {
		if(coluna == 0)
               {
 
		int vetor[16] __attribute__((aligned(64)));
    	
           __m512i load = _mm512_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha,m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);

              __m512i  load2 = _mm512_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha,m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha,m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha,m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha,m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha);

                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,load3));
                   _mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;

                        mTrans[linha][coluna] = p;
		}
               else if(coluna == 1)
               {

                                int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha,m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);

                   __m512i load2 = _mm512_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha,m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha,m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);

                   __m512i load4 = _mm512_setr_epi32(m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha,m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha,m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha,m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha);


                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,_mm512_add_epi32(load3,load4)));
                   _mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;

                        mTrans[linha][coluna] = p;   
               }
               else if(coluna == N-1)
               {
               
                int vetor[16] __attribute__((aligned(64)));

               __m512i load = _mm512_setr_epi32(m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha,m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha);

              __m512i  load2 = _mm512_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha,m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha,m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha,m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha);

                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,load3));
                   _mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;

                        mTrans[linha][coluna] = p;


		}

               else if(coluna == N-2)
               {
                   
                                   int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha,m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);

                   __m512i load2 = _mm512_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha,m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);

                   __m512i load4 = _mm512_setr_epi32(m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha,m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha,m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha,m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha);


                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,_mm512_add_epi32(load3,load4)));
                   _mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;

                        mTrans[linha][coluna] = p;
               }
               else
               {
                
                int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha,m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);


                   __m512i load2 = _mm512_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha,m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha,m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);

                   __m512i load4 = _mm512_setr_epi32(m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha,m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha,m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha,m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);

                   __m512i load5 = _mm512_setr_epi32(m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha,m[linha][coluna+2].red,m[linha][coluna +2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha,m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha,m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha);

                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,_mm512_add_epi32(load3,_mm512_add_epi32(load4,load5) )));
                _mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;
                 mTrans[linha][coluna] = p;
               }

	   }
           else if(linha == M-1)
           {
               if(coluna == 0)
               {

                   int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha,m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   __m512i load2 = _mm512_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha,m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha,m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha,m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);



                   __m512i soma = _mm512_add_epi32(load,load2);
                   _mm512_store_epi32(vetor,soma);


                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12] + m[linha-2][coluna+2].red)/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13] + m[linha-2][coluna+2].green)/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14] + m[linha-2][coluna+2].blue)/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15] + m[linha-2][coluna+2].alpha)/25;
                   mTrans[linha][coluna] = p;

               }
               else if(coluna == 1)
               {
                   int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha,m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);

                   __m512i load2 = _mm512_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha,m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha,m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha,m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha,m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha);

                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,load3));
                   _mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;

                    mTrans[linha][coluna] = p;


               }
               else if(coluna == N-1)
               {

                    int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                   __m512i load2 = _mm512_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha,m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha,m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha,m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha);



                   __m512i soma = _mm512_add_epi32(load,load2);
                   _mm512_store_epi32(vetor,soma);


                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12] + m[linha-2][coluna-2].red)/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13] + m[linha-2][coluna-2].green)/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14] + m[linha-2][coluna-2].blue)/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15] + m[linha-2][coluna-2].alpha)/25;
                   mTrans[linha][coluna] = p;
              
	       }
               else if(coluna == N-2)
               {


                   int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna-1].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);

                   __m512i load2 = _mm512_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha,m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha,m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha,m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha,m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha);


                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,load3));
                   _mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;

                        mTrans[linha][coluna] = p;

           
               }
               else
               {

                int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha,m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);
                   __m512i load2 = _mm512_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha,m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha,m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha,m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha,m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);


                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,load3));
                   _mm512_store_epi32(vetor,soma);


                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12] + m[linha][coluna+2].red + m[linha-1][coluna+2].red + m[linha-2][coluna+2].red)/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13] + m[linha][coluna+2].green + m[linha-1][coluna+2].green + m[linha-2][coluna+2].green)/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14] + m[linha][coluna+2].blue + m[linha-1][coluna+2].blue + m[linha-2][coluna+2].blue)/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15] + m[linha][coluna+2].alpha + m[linha-1][coluna+2].alpha + m[linha-2][coluna+2].alpha)/25;
                mTrans[linha][coluna] = p;
                   
               }
               
           }

           else
           {
               if(coluna == 0)
               {
			int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha,m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha,m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);
                  
		  __m512i load2 = _mm512_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha,m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha,m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);

                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,load3));
                   _mm512_store_epi32(vetor,soma);


                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12] + m[linha+2][coluna].red + m[linha+2][coluna+1].red + m[linha+2][coluna+2].red)/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13] + m[linha+2][coluna].green + m[linha+2][coluna+1].green + m[linha+2][coluna+2].green)/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14] + m[linha+2][coluna].blue + m[linha+2][coluna+1].blue + m[linha+2][coluna+2].blue)/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15] + m[linha+2][coluna].alpha + m[linha+2][coluna+1].alpha + m[linha+2][coluna+2].alpha)/25;
                mTrans[linha][coluna] = p;
	                   
               }
	       else if(coluna == 1)
	       {
			int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha,m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha,m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha,m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha);


                   __m512i load2 = _mm512_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha,m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha,m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha);

                   __m512i load4 = _mm512_setr_epi32(m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha,m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);

                   __m512i load5 = _mm512_setr_epi32(m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha,m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha,m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha,m[linha+2][coluna+2].red,m[linha+2][coluna+2].green,m[linha+2][coluna+2].blue,m[linha+2][coluna+2].alpha);

                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,_mm512_add_epi32(load3,_mm512_add_epi32(load4,load5) )));
                _mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;
                 mTrans[linha][coluna] = p;

	       }
               else if(coluna == M-1)
               {
                        int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha,m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha,m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha);

                  __m512i load2 = _mm512_setr_epi32(m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha,m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha,m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);

                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,load3));
                   _mm512_store_epi32(vetor,soma);


                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12] + m[linha+2][coluna].red + m[linha+2][coluna-1].red + m[linha+2][coluna-2].red)/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13] + m[linha+2][coluna].green + m[linha+2][coluna-1].green + m[linha+2][coluna-2].green)/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14] + m[linha+2][coluna].blue + m[linha+2][coluna-1].blue + m[linha+2][coluna-2].blue)/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15] + m[linha+2][coluna].alpha + m[linha+2][coluna-1].alpha + m[linha+2][coluna-2].alpha)/25;
                mTrans[linha][coluna] = p;         
                   
               }
	       else if(coluna == M-2)
	       {
                        int vetor[16] __attribute__((aligned(64)));

                   __m512i load = _mm512_setr_epi32(m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha,m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha,m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha,m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha);


                   __m512i load2 = _mm512_setr_epi32(m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha,m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha);

                   __m512i load3 = _mm512_setr_epi32(m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha);

                   __m512i load4 = _mm512_setr_epi32(m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha,m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha);

                   __m512i load5 = _mm512_setr_epi32(m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha,m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha,m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha,m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha);

                   __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,_mm512_add_epi32(load3,_mm512_add_epi32(load4,load5) )));
                _mm512_store_epi32(vetor,soma);

                   p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12])/25;
                   p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13])/25;
                   p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14])/25;
                   p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15])/25;
                 mTrans[linha][coluna] = p;
               }
               
           }

}

    else
    {

	int vetor[16] __attribute__((aligned(64)));
       
	__m512i load; 
	__m512i load2;
        __m512i load3;
        __m512i load4;
        __m512i load5;
        __m512i load6;
    

	load = _mm512_setr_epi32(m[linha-2][coluna-2].red,m[linha-2][coluna-2].green,m[linha-2][coluna-2].blue,m[linha-2][coluna-2].alpha,m[linha-2][coluna-1].red,m[linha-2][coluna-1].green,m[linha-2][coluna-1].blue,m[linha-2][coluna-1].alpha,m[linha-2][coluna].red,m[linha-2][coluna].green,m[linha-2][coluna].blue,m[linha-2][coluna].alpha,m[linha-2][coluna+1].red,m[linha-2][coluna+1].green,m[linha-2][coluna+1].blue,m[linha-2][coluna+1].alpha);
	
	        load2 = _mm512_setr_epi32(m[linha-1][coluna-2].red,m[linha-1][coluna-2].green,m[linha-1][coluna-2].blue,m[linha-1][coluna-2].alpha,m[linha-1][coluna-1].red,m[linha-1][coluna-1].green,m[linha-1][coluna-1].blue,m[linha-1][coluna-1].alpha,m[linha-1][coluna].red,m[linha-1][coluna].green,m[linha-1][coluna].blue,m[linha-1][coluna].alpha,m[linha-1][coluna+1].red,m[linha-1][coluna+1].green,m[linha-1][coluna+1].blue,m[linha-1][coluna+1].alpha);

        load3 = _mm512_setr_epi32(m[linha][coluna-2].red,m[linha][coluna-2].green,m[linha][coluna-2].blue,m[linha][coluna-2].alpha,m[linha][coluna-1].red,m[linha][coluna-1].green,m[linha][coluna-1].blue,m[linha][coluna-1].alpha,m[linha][coluna].red,m[linha][coluna].green,m[linha][coluna].blue,m[linha][coluna].alpha,m[linha][coluna+1].red,m[linha][coluna+1].green,m[linha][coluna+1].blue,m[linha][coluna+1].alpha);

        load4 = _mm512_setr_epi32(m[linha+1][coluna-2].red,m[linha+1][coluna-2].green,m[linha+1][coluna-2].blue,m[linha+1][coluna-2].alpha,m[linha+1][coluna-1].red,m[linha+1][coluna-1].green,m[linha+1][coluna-1].blue,m[linha+1][coluna-1].alpha,m[linha+1][coluna].red,m[linha+1][coluna].green,m[linha+1][coluna].blue,m[linha+1][coluna].alpha,m[linha+1][coluna+1].red,m[linha+1][coluna+1].green,m[linha+1][coluna+1].blue,m[linha+1][coluna+1].alpha);

        load5 = _mm512_setr_epi32(m[linha+2][coluna-2].red,m[linha+2][coluna-2].green,m[linha+2][coluna-2].blue,m[linha+2][coluna-2].alpha,m[linha+2][coluna-1].red,m[linha+2][coluna-1].green,m[linha+2][coluna-1].blue,m[linha+2][coluna-1].alpha,m[linha+2][coluna].red,m[linha+2][coluna].green,m[linha+2][coluna].blue,m[linha+2][coluna].alpha,m[linha+2][coluna+1].red,m[linha+2][coluna+1].green,m[linha+2][coluna+1].blue,m[linha+2][coluna+1].alpha);

        load6 = _mm512_setr_epi32(m[linha-2][coluna+2].red,m[linha-2][coluna+2].green,m[linha-2][coluna+2].blue,m[linha-2][coluna+2].alpha,m[linha-1][coluna+2].red,m[linha-1][coluna+2].green,m[linha-1][coluna+2].blue,m[linha-1][coluna+2].alpha,m[linha][coluna+2].red,m[linha][coluna+2].green,m[linha][coluna+2].blue,m[linha][coluna+2].alpha,m[linha+1][coluna+2].red,m[linha+1][coluna+2].green,m[linha+1][coluna+2].blue,m[linha+1][coluna+2].alpha);

        __m512i soma = _mm512_add_epi32(load,_mm512_add_epi32(load2,_mm512_add_epi32(load3,_mm512_add_epi32(load4,_mm512_add_epi32(load5,load6)))));	
	_mm512_store_epi32(vetor,soma);

        p.red = (vetor[0] + vetor[4] + vetor[8] + vetor[12] + m[linha+2][coluna+2].red)/25;
        p.green =  (vetor[1] + vetor[5] + vetor[9] +vetor[13] + m[linha+2][coluna+2].green)/25;
        p.blue = (vetor[2] + vetor[6] + vetor[10] +vetor[14] + m[linha+2][coluna+2].blue)/25;
        p.alpha =  (vetor[3] + vetor[7] + vetor[11] +vetor[15] + m[linha+2][coluna+2].alpha)/25;

        mTrans[linha][coluna] = p;

	}
}



void transformarMatriz(pix m[][N],pix mTrans[][N])
{
    int i;
    int j;
    #pragma omp parallel for collapse(2) num_threads(240)
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
            
        {
            funcAux(m,mTrans,i,j);
        }
    }
}

pix **alocarMatriz()
{
    int i;
    pix **p;
    p = (pix **) malloc(M * sizeof(pix *));
    
    //p = (pix **) _mm_malloc(M*sizeof(pix *),16);
    
    for(i=0;i<M;i++)
    {
        p[i] = (pix *) malloc(N * sizeof(pix));
        //        p[i] = (pix *) _mm_malloc(N * sizeof(pix),16);
    }
    
    return p;
}


void MontarMatriz(pix m[][N])
{
    int i;
    int j;
    pix p;
    
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
        {
            p.red = rand() % 256;
            p.green = rand() % 256;
            p.blue = rand() % 256;
            p.alpha = rand() % 256;
            m[i][j] = p;
        }
    }
    
}

void MontarMatrizTrans(pix m[][N])
{	
    int i;
    int j;
    pix p;


    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
        {
            p.red = 0;
            p.green = 0;
            p.blue = 0;
            p.alpha = 0;
            m[i][j] = p;
        }
    }
}


void imprimirMatriz(pix m[][N])
{
    int i;
    int j;
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
        {
            printf("Matriz RED[%d][%d] = %d\n",i,j,m[i][j].red);
            printf("Matriz GREEN[%d][%d] = %d\n",i,j,m[i][j].green);
            printf("Matriz BLUE[%d][%d] = %d\n",i,j,m[i][j].blue);
            printf("Matriz ALPHA[%d][%d] = %d\n",i,j,m[i][j].alpha);
        }			
    }	
}

