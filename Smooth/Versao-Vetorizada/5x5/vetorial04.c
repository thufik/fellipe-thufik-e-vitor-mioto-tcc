#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "emmintrin.h"
#include "mmintrin.h"
#include "immintrin.h"
#define M 9000
#define N 9000


typedef struct  pixel
{
    short red;
    short green;
    short blue;
    short alpha;
    
}pix;




pix **alocarMatriz(); //aloca uma matriz que contem em cada posiç uma structt
void MontarMatriz(pix **m); //poe em cada posiç da matriz uma struct,gerando numeros aleatorios de 0 a 255 para o RGBA
void imprimirMatriz(pix **m);

void transformarMatriz(pix **m,pix **mTrans);
void funcAux(pix **m,pix **mTrans,int linha,int coluna);

int contador = 0;


int main()
{
    time_t start,stop;
    double  avg_time = 0;
    double cur_time;
    pix **p = alocarMatriz(); //matriz de structs
    pix **vTrans = alocarMatriz();
    MontarMatriz(p);
    //    imprimirMatriz(p);
    start = clock();
    transformarMatriz(p,vTrans);
    stop = clock();
    cur_time = ((double) stop-start) / CLOCKS_PER_SEC;
    printf("Averagely used %0.2f seconds.\n", cur_time);
    //  printf("Contador = %ld\n",contador);
    //imprimirMatriz(vTrans);
    return 0;
    
}

void funcAux(pix **m,pix **mTrans,int linha,int coluna)
{
    int i;
    pix p;
    
    
    
    if(linha <= 1 || coluna <= 1 || linha >= (M-2) || coluna >= (N-2))
    {
        
	       if(linha == 0)
           {
               
               if(coluna == 0)
               {
                   
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m64 *load4;
                   __m64 *load5;
                   __m64 *load6;
                   __m64 *load7;
                   __m128i *load8;
                   
                   
                   load =  (__m128i *) &m[linha][coluna].red;
                   load2 = (__m128i *) &m[linha+1][coluna].red;
                   load3 = (__m128i *) &m[linha+2][coluna].red;
                   
                   load4 = (__m64 *) &m[linha][coluna + 2].red;
                   load5 = (__m64 *) &m[linha +1][coluna + 2].red;
                   load6 = (__m64 *) &m[linha +2][coluna + 2].red;
                   load7 = (__m64 *) string2;
                   load8 = (__m128i *) string;
                   
                   __m128i teste = _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   
                   
                   __m128i teste4 = _mm_add_epi16(teste,_mm_add_epi16(teste2,teste3));
                   
                   __m64 teste5 = _mm_add_pi16(load4[0],_mm_add_pi16(load5[0],load6[0]));
                   load7[0] = teste5;
                   _mm_storeu_si128(&load8[0],teste4);
                   
                   p.red = (string[0] + string[4] + string2[0])/25;
                   p.green =  (string[1] + string[5] + string2[1])/25;
                   p.blue = (string[2] + string[6] + string2[2])/25;
                   p.alpha =  (string[3] + string[7] + string2[3])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               
               
               
               else if(coluna == 1)
               {
                   
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m128i *load6;
                   __m128i *load7;
                   
                   
                   load =  (__m128i *) &m[linha][coluna-1].red;
                   load2 = (__m128i *) &m[linha][coluna+1].red;
                   load3 = (__m128i *) &m[linha+1][coluna-1].red;
                   load4 = (__m128i *) &m[linha+1][coluna+1].red;
                   load5 = (__m128i *) &m[linha+2][coluna-1].red;
                   load6 = (__m128i *) &m[linha+2][coluna+1].red;
                   load7 = (__m128i *) string;
                   
                   __m128i teste =  _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 =  _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   __m128i teste6 = _mm_loadu_si128(&load6[0]);
                   __m128i teste7 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,_mm_add_epi16(teste5,teste6)))));
                   _mm_storeu_si128(&load7[0],teste7);
                   
                   p.red = (string[0] + string[4])/25;
                   p.green =  (string[1] + string[5])/25;
                   p.blue = (string[2] + string[6])/25;
                   p.alpha =  (string[3] + string[7])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               
               
               else if(coluna == N-1)
               {
                   
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m64 *load4;
                   __m64 *load5;
                   __m64 *load6;
                   __m64 *load7;
                   __m128i *load8;
                   
                   
                   load =  (__m128i *) &m[linha][coluna-2].red;
                   load2 = (__m128i *) &m[linha+1][coluna-2].red;
                   load3 = (__m128i *) &m[linha+2][coluna-2].red;
                   
                   load4 = (__m64 *) &m[linha][coluna].red;
                   load5 = (__m64 *) &m[linha +1][coluna].red;
                   load6 = (__m64 *) &m[linha +2][coluna].red;
                   load7 = (__m64 *) string2;
                   load8 = (__m128i *) string;
                   
                   __m128i teste = _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   
                   
                   __m128i teste4 = _mm_add_epi16(teste,_mm_add_epi16(teste2,teste3));
                   
                   __m64 teste5 = _mm_add_pi16(load4[0],_mm_add_pi16(load5[0],load6[0]));
                   load7[0] = teste5;
                   _mm_storeu_si128(&load8[0],teste4);
                   
                   p.red = (string[0] + string[4] + string2[0])/25;
                   p.green =  (string[1] + string[5] + string2[1])/25;
                   p.blue = (string[2] + string[6] + string2[2])/25;
                   p.alpha =  (string[3] + string[7] + string2[3])/25;
                   mTrans[linha][coluna] = p;
               }
               else if(coluna == N-2)
               {
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m128i *load6;
                   __m128i *load7;
                   
                   
                   load =  (__m128i *) &m[linha][coluna-2].red;
                   load2 = (__m128i *) &m[linha][coluna].red;
                   load3 = (__m128i *) &m[linha+1][coluna-2].red;
                   load4 = (__m128i *) &m[linha+1][coluna].red;
                   load5 = (__m128i *) &m[linha+2][coluna-2].red;
                   load6 = (__m128i *) &m[linha+2][coluna].red;
                   load7 = (__m128i *) string;
                   
                   __m128i teste =  _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 =  _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   __m128i teste6 = _mm_loadu_si128(&load6[0]);
                   __m128i teste7 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,_mm_add_epi16(teste5,teste6)))));
                   _mm_storeu_si128(&load7[0],teste7);
                   
                   p.red = (string[0] + string[4])/25;
                   p.green =  (string[1] + string[5])/25;
                   p.blue = (string[2] + string[6])/25;
                   p.alpha =  (string[3] + string[7])/25;
                   mTrans[linha][coluna] = p;
               }
               else
               {
                   
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m128i *load6;
                   __m64 *load7;
                   __m64 *load8;
                   __m64 *load9;
                   __m64 *load10;
                   __m128i *load11;
                   
                   load =  (__m128i *) &m[linha][coluna-2].red;
                   load2 = (__m128i *) &m[linha][coluna].red;
                   load3 = (__m128i *) &m[linha+1][coluna-2].red;
                   load4 = (__m128i *) &m[linha+1][coluna].red;
                   load5 = (__m128i *) &m[linha+2][coluna-2].red;
                   load6 = (__m128i *) &m[linha+2][coluna].red;
                   load7 = (__m64 *) &m[linha][coluna+2].red;
                   load8 = (__m64 *) &m[linha +1][coluna+2].red;
                   load9 = (__m64 *) &m[linha +2][coluna+2].red;
                   load10 = (__m64 *) string2;
                   load11 = (__m128i *) string;
                   
                   
                   __m128i teste =  _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 =  _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   __m128i teste6 = _mm_loadu_si128(&load6[0]);
                   __m128i teste7 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,_mm_add_epi16(teste5,teste6)))));
                   _mm_storeu_si128(&load11[0],teste7);
                   
                   __m64 teste8 = _mm_add_pi16(load7[0],_mm_add_pi16(load8[0],load9[0]));
                   load10[0] = teste8;
                   
                   p.red = (string[0] + string[4] + string2[0])/25;
                   p.green =  (string[1] + string[5] + string2[1])/25;
                   p.blue = (string[2] + string[6] + string2[2])/25;
                   p.alpha =  (string[3] + string[7] + string2[3])/25;
                   mTrans[linha][coluna] = p;
               }
               
           }
           else if(linha == 1)
           {
               
               if(coluna == 0)
               {
                   
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m64 *load5;
                   __m64 *load6;
                   __m64 *load7;
                   __m64 *load8;
                   __m64 *load9;
                   __m128i *load10;
                   
                   
                   load =  (__m128i *) &m[linha][coluna].red;
                   load2 = (__m128i *) &m[linha+1][coluna].red;
                   load3 = (__m128i *) &m[linha+2][coluna].red;
                   load4 = (__m128i *) &m[linha-1][coluna].red;
                   load5 = (__m64 *) &m[linha][coluna + 2].red;
                   load6 = (__m64 *) &m[linha +1][coluna + 2].red;
                   load7 = (__m64 *) &m[linha +2][coluna + 2].red;
                   load8 = (__m64 *) &m[linha -1][coluna + 2].red;
                   load9 = (__m64 *) string2;
                   load10 = (__m128i *) string;
                   
                   __m128i teste = _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 = _mm_loadu_si128(&load4[0]);
                   
                   __m128i teste5 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,teste4)));
                   
                   __m64 teste6 = _mm_add_pi16(load5[0],_mm_add_pi16(load6[0],_mm_add_pi16(load7[0],load8[0])));
                   load9[0] = teste6;
                   _mm_storeu_si128(&load10[0],teste5);
                   
                   p.red = (string[0] + string[4] + string2[0])/25;
                   p.green =  (string[1] + string[5] + string2[1])/25;
                   p.blue = (string[2] + string[6] + string2[2])/25;
                   p.alpha =  (string[3] + string[7] + string2[3])/25;
                   mTrans[linha][coluna] = p;
               }
               else if(coluna == 1)
               {
                   
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m128i *load6;
                   __m128i *load7;
                   __m128i *load8;
                   __m128i *load9;
                   
                   
                   
                   load =  (__m128i *) &m[linha][coluna-1].red;
                   load2 = (__m128i *) &m[linha][coluna+1].red;
                   load3 = (__m128i *) &m[linha+1][coluna-1].red;
                   load4 = (__m128i *) &m[linha+1][coluna+1].red;
                   load5 = (__m128i *) &m[linha+2][coluna-1].red;
                   load6 = (__m128i *) &m[linha+2][coluna+1].red;
                   load7 = (__m128i *) &m[linha-1][coluna-1].red;
                   load8 = (__m128i *) &m[linha-1][coluna+1].red;
                   load9 = (__m128i *) string;
                   
                   __m128i teste =  _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 =  _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   __m128i teste6 = _mm_loadu_si128(&load6[0]);
                   __m128i teste7 = _mm_loadu_si128(&load7[0]);
                   __m128i teste8 = _mm_loadu_si128(&load8[0]);
                   
                   __m128i teste9 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,_mm_add_epi16(teste5,_mm_add_epi16(teste6,_mm_add_epi16(teste7,teste8)))))));
                   _mm_storeu_si128(&load9[0],teste9);
                   
                   p.red = (string[0] + string[4])/25;
                   p.green =  (string[1] + string[5])/25;
                   p.blue = (string[2] + string[6])/25;
                   p.alpha =  (string[3] + string[7])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               
               else if(coluna == N-1)
               {
                   
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m64 *load5;
                   __m64 *load6;
                   __m64 *load7;
                   __m64 *load8;
                   __m64 *load9;
                   __m128i *load10;
                   
                   
                   load =  (__m128i *) &m[linha][coluna-2].red;
                   load2 = (__m128i *) &m[linha+1][coluna-2].red;
                   load3 = (__m128i *) &m[linha+2][coluna-2].red;
                   load4 = (__m128i *) &m[linha-1][coluna-2].red;
                   load5 = (__m64 *) &m[linha][coluna].red;
                   load6 = (__m64 *) &m[linha +1][coluna].red;
                   load7 = (__m64 *) &m[linha +2][coluna].red;
                   load8 = (__m64 *) &m[linha -1][coluna].red;
                   load9 = (__m64 *) string2;
                   load10 = (__m128i *) string;
                   
                   __m128i teste = _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 = _mm_loadu_si128(&load4[0]);
                   
                   __m128i teste5 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,teste4)));
                   
                   __m64 teste6 = _mm_add_pi16(load5[0],_mm_add_pi16(load6[0],_mm_add_pi16(load7[0],load8[0])));
                   load9[0] = teste6;
                   _mm_storeu_si128(&load10[0],teste5);
                   
                   p.red = (string[0] + string[4] + string2[0])/25;
                   p.green =  (string[1] + string[5] + string2[1])/25;
                   p.blue = (string[2] + string[6] + string2[2])/25;
                   p.alpha =  (string[3] + string[7] + string2[3])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               
               else if(coluna == N-2)
               {
                   
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m128i *load6;
                   __m128i *load7;
                   __m128i *load8;
                   __m128i *load9;
                   
                   
                   
                   load =  (__m128i *) &m[linha][coluna-2].red;
                   load2 = (__m128i *) &m[linha][coluna].red;
                   load3 = (__m128i *) &m[linha+1][coluna-2].red;
                   load4 = (__m128i *) &m[linha+1][coluna].red;
                   load5 = (__m128i *) &m[linha+2][coluna-2].red;
                   load6 = (__m128i *) &m[linha+2][coluna].red;
                   load7 = (__m128i *) &m[linha-1][coluna-2].red;
                   load8 = (__m128i *) &m[linha-1][coluna].red;
                   load9 = (__m128i *) string;
                   
                   __m128i teste =  _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 =  _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   __m128i teste6 = _mm_loadu_si128(&load6[0]);
                   __m128i teste7 = _mm_loadu_si128(&load7[0]);
                   __m128i teste8 = _mm_loadu_si128(&load8[0]);
                   
                   __m128i teste9 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,_mm_add_epi16(teste5,_mm_add_epi16(teste6,_mm_add_epi16(teste7,teste8)))))));
                   _mm_storeu_si128(&load9[0],teste9);
                   
                   p.red = (string[0] + string[4])/25;
                   p.green =  (string[1] + string[5])/25;
                   p.blue = (string[2] + string[6])/25;
                   p.alpha =  (string[3] + string[7])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else
               {
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m128i *load6;
                   __m128i *load7;
                   __m128i *load8;
                   __m64 *load9;
                   __m64 *load10;
                   __m64 *load11;
                   __m64 *load12;
                   __m64 *load13;
                   __m128i *load14;
                   
                   load =  (__m128i *) &m[linha][coluna-2].red;
                   load2 = (__m128i *) &m[linha][coluna].red;
                   load3 = (__m128i *) &m[linha+1][coluna-2].red;
                   load4 = (__m128i *) &m[linha+1][coluna].red;
                   load5 = (__m128i *) &m[linha+2][coluna-2].red;
                   load6 = (__m128i *) &m[linha+2][coluna].red;
                   load7 = (__m128i *) &m[linha-1][coluna-2].red;
                   load8 = (__m128i *) &m[linha-1][coluna].red;
                   load9 = (__m64 *) &m[linha][coluna+2].red;
                   load10 = (__m64 *) &m[linha +1][coluna+2].red;
                   load11 = (__m64 *) &m[linha +2][coluna+2].red;
                   load12 = (__m64 *) &m[linha -1][coluna+2].red;
                   load13 = (__m64 *) string2;
                   load14 = (__m128i *) string;
                   
                   
                   __m128i teste =  _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 =  _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   __m128i teste6 = _mm_loadu_si128(&load6[0]);
                   __m128i teste7 = _mm_loadu_si128(&load7[0]);
                   __m128i teste8 = _mm_loadu_si128(&load8[0]);
                   __m128i teste9 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,_mm_add_epi16(teste5,_mm_add_epi16(teste6,_mm_add_epi16(teste7,teste8)))))));
                   _mm_storeu_si128(&load14[0],teste9);
                   
                   __m64 teste10 = _mm_add_pi16(load9[0],_mm_add_pi16(load10[0],_mm_add_pi16(load11[0],load12[0])));
                   load13[0] = teste10;
                   
                   p.red = (string[0] + string[4] + string2[0])/25;
                   p.green =  (string[1] + string[5] + string2[1])/25;
                   p.blue = (string[2] + string[6] + string2[2])/25;
                   p.alpha =  (string[3] + string[7] + string2[3])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               
           }
           else if(linha == M-2)
           {
               if(coluna == 0)
               {
                   
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m64 *load5;
                   __m64 *load6;
                   __m64 *load7;
                   __m64 *load8;
                   __m64 *load9;
                   __m128i *load10;
                   
                   
                   load =  (__m128i *) &m[linha][coluna].red;
                   load2 = (__m128i *) &m[linha-1][coluna].red;
                   load3 = (__m128i *) &m[linha-2][coluna].red;
                   load4 = (__m128i *) &m[linha+1][coluna].red;
                   load5 = (__m64 *) &m[linha][coluna + 2].red;
                   load6 = (__m64 *) &m[linha -1][coluna + 2].red;
                   load7 = (__m64 *) &m[linha -2][coluna + 2].red;
                   load8 = (__m64 *) &m[linha +1][coluna + 2].red;
                   load9 = (__m64 *) string2;
                   load10 = (__m128i *) string;
                   
                   __m128i teste = _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 = _mm_loadu_si128(&load4[0]);
                   
                   __m128i teste5 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,teste4)));
                   
                   __m64 teste6 = _mm_add_pi16(load5[0],_mm_add_pi16(load6[0],_mm_add_pi16(load7[0],load8[0])));
                   load9[0] = teste6;
                   _mm_storeu_si128(&load10[0],teste5);
                   
                   p.red = (string[0] + string[4] + string2[0])/25;
                   p.green =  (string[1] + string[5] + string2[1])/25;
                   p.blue = (string[2] + string[6] + string2[2])/25;
                   p.alpha =  (string[3] + string[7] + string2[3])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == 1)
               {
                   
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m128i *load6;
                   __m128i *load7;
                   __m128i *load8;
                   __m128i *load9;
                   
                   
                   
                   load =  (__m128i *) &m[linha][coluna-1].red;
                   load2 = (__m128i *) &m[linha][coluna+1].red;
                   load3 = (__m128i *) &m[linha-1][coluna-1].red;
                   load4 = (__m128i *) &m[linha-1][coluna+1].red;
                   load5 = (__m128i *) &m[linha-2][coluna-1].red;
                   load6 = (__m128i *) &m[linha-2][coluna+1].red;
                   load7 = (__m128i *) &m[linha+1][coluna-1].red;
                   load8 = (__m128i *) &m[linha+1][coluna+1].red;
                   load9 = (__m128i *) string;
                   
                   __m128i teste =  _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 =  _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   __m128i teste6 = _mm_loadu_si128(&load6[0]);
                   __m128i teste7 = _mm_loadu_si128(&load7[0]);
                   __m128i teste8 = _mm_loadu_si128(&load8[0]);
                   
                   __m128i teste9 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,_mm_add_epi16(teste5,_mm_add_epi16(teste6,_mm_add_epi16(teste7,teste8)))))));
                   _mm_storeu_si128(&load9[0],teste9);
                   
                   p.red = (string[0] + string[4])/25;
                   p.green =  (string[1] + string[5])/25;
                   p.blue = (string[2] + string[6])/25;
                   p.alpha =  (string[3] + string[7])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == N-1)
               {
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m64 *load5;
                   __m64 *load6;
                   __m64 *load7;
                   __m64 *load8;
                   __m64 *load9;
                   __m128i *load10;
                   
                   
                   load =  (__m128i *) &m[linha][coluna-2].red;
                   load2 = (__m128i *) &m[linha-1][coluna-2].red;
                   load3 = (__m128i *) &m[linha-2][coluna-2].red;
                   load4 = (__m128i *) &m[linha+1][coluna-2].red;
                   load5 = (__m64 *) &m[linha][coluna].red;
                   load6 = (__m64 *) &m[linha -1][coluna].red;
                   load7 = (__m64 *) &m[linha -2][coluna].red;
                   load8 = (__m64 *) &m[linha +1][coluna].red;
                   load9 = (__m64 *) string2;
                   load10 = (__m128i *) string;
                   
                   __m128i teste = _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 = _mm_loadu_si128(&load4[0]);
                   
                   __m128i teste5 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,teste4)));
                   
                   __m64 teste6 = _mm_add_pi16(load5[0],_mm_add_pi16(load6[0],_mm_add_pi16(load7[0],load8[0])));
                   load9[0] = teste6;
                   _mm_storeu_si128(&load10[0],teste5);
                   
                   p.red = (string[0] + string[4] + string2[0])/25;
                   p.green =  (string[1] + string[5] + string2[1])/25;
                   p.blue = (string[2] + string[6] + string2[2])/25;
                   p.alpha =  (string[3] + string[7] + string2[3])/25;
                   mTrans[linha][coluna] = p;
               }
               
               else if(coluna == N-2)
               {
                   
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m128i *load6;
                   __m128i *load7;
                   __m128i *load8;
                   __m128i *load9;
                   
                   
                   
                   load =  (__m128i *) &m[linha][coluna-2].red;
                   load2 = (__m128i *) &m[linha][coluna].red;
                   load3 = (__m128i *) &m[linha-1][coluna-2].red;
                   load4 = (__m128i *) &m[linha-1][coluna].red;
                   load5 = (__m128i *) &m[linha-2][coluna-2].red;
                   load6 = (__m128i *) &m[linha-2][coluna].red;
                   load7 = (__m128i *) &m[linha+1][coluna-2].red;
                   load8 = (__m128i *) &m[linha+1][coluna].red;
                   load9 = (__m128i *) string;
                   
                   __m128i teste =  _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 =  _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   __m128i teste6 = _mm_loadu_si128(&load6[0]);
                   __m128i teste7 = _mm_loadu_si128(&load7[0]);
                   __m128i teste8 = _mm_loadu_si128(&load8[0]);
                   
                   __m128i teste9 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,_mm_add_epi16(teste5,_mm_add_epi16(teste6,_mm_add_epi16(teste7,teste8)))))));
                   _mm_storeu_si128(&load9[0],teste9);
                   
                   p.red = (string[0] + string[4])/25;
                   p.green =  (string[1] + string[5])/25;
                   p.blue = (string[2] + string[6])/25;
                   p.alpha =  (string[3] + string[7])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else
               {
                   
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m128i *load6;
                   __m128i *load7;
                   __m128i *load8;
                   __m64 *load9;
                   __m64 *load10;
                   __m64 *load11;
                   __m64 *load12;
                   __m64 *load13;
                   __m128i *load14;
                   
                   load =  (__m128i *) &m[linha][coluna-2].red;
                   load2 = (__m128i *) &m[linha][coluna].red;
                   load3 = (__m128i *) &m[linha-1][coluna-2].red;
                   load4 = (__m128i *) &m[linha-1][coluna].red;
                   load5 = (__m128i *) &m[linha-2][coluna-2].red;
                   load6 = (__m128i *) &m[linha-2][coluna].red;
                   load7 = (__m128i *) &m[linha+1][coluna-2].red;
                   load8 = (__m128i *) &m[linha+1][coluna].red;
                   load9 = (__m64 *) &m[linha][coluna+2].red;
                   load10 = (__m64 *) &m[linha -1][coluna+2].red;
                   load11 = (__m64 *) &m[linha -2][coluna+2].red;
                   load12 = (__m64 *) &m[linha +1][coluna+2].red;
                   load13 = (__m64 *) string2;
                   load14 = (__m128i *) string;
                   
                   
                   __m128i teste =  _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 =  _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   __m128i teste6 = _mm_loadu_si128(&load6[0]);
                   __m128i teste7 = _mm_loadu_si128(&load7[0]);
                   __m128i teste8 = _mm_loadu_si128(&load8[0]);
                   __m128i teste9 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,_mm_add_epi16(teste5,_mm_add_epi16(teste6,_mm_add_epi16(teste7,teste8)))))));
                   _mm_storeu_si128(&load14[0],teste9);
                   
                   __m64 teste10 = _mm_add_pi16(load9[0],_mm_add_pi16(load10[0],_mm_add_pi16(load11[0],load12[0])));
                   load13[0] = teste10;
                   
                   p.red = (string[0] + string[4] + string2[0])/25;
                   p.green =  (string[1] + string[5] + string2[1])/25;
                   p.blue = (string[2] + string[6] + string2[2])/25;
                   p.alpha =  (string[3] + string[7] + string2[3])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               
           }
           else if(linha == M-1)
           {
               if(coluna == 0)
               {
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m64 *load4;
                   __m64 *load5;
                   __m64 *load6;
                   __m64 *load7;
                   __m128i *load8;
                   
                   
                   load =  (__m128i *) &m[linha][coluna].red;
                   load2 = (__m128i *) &m[linha-1][coluna].red;
                   load3 = (__m128i *) &m[linha-2][coluna].red;
                   
                   load4 = (__m64 *) &m[linha][coluna + 2].red;
                   load5 = (__m64 *) &m[linha -1][coluna + 2].red;
                   load6 = (__m64 *) &m[linha -2][coluna + 2].red;
                   load7 = (__m64 *) string2;
                   load8 = (__m128i *) string;
                   
                   __m128i teste = _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   
                   
                   __m128i teste4 = _mm_add_epi16(teste,_mm_add_epi16(teste2,teste3));
                   
                   __m64 teste5 = _mm_add_pi16(load4[0],_mm_add_pi16(load5[0],load6[0]));
                   load7[0] = teste5;
                   _mm_storeu_si128(&load8[0],teste4);
                   
                   p.red = (string[0] + string[4] + string2[0])/25;
                   p.green =  (string[1] + string[5] + string2[1])/25;
                   p.blue = (string[2] + string[6] + string2[2])/25;
                   p.alpha =  (string[3] + string[7] + string2[3])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == 1)
               {
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m128i *load6;
                   __m128i *load7;
                   
                   
                   load =  (__m128i *) &m[linha][coluna-1].red;
                   load2 = (__m128i *) &m[linha][coluna+1].red;
                   load3 = (__m128i *) &m[linha-1][coluna-1].red;
                   load4 = (__m128i *) &m[linha-1][coluna+1].red;
                   load5 = (__m128i *) &m[linha-2][coluna-1].red;
                   load6 = (__m128i *) &m[linha-2][coluna+1].red;
                   load7 = (__m128i *) string;
                   
                   __m128i teste =  _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 =  _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   __m128i teste6 = _mm_loadu_si128(&load6[0]);
                   __m128i teste7 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,_mm_add_epi16(teste5,teste6)))));
                   _mm_storeu_si128(&load7[0],teste7);
                   
                   p.red = (string[0] + string[4])/25;
                   p.green =  (string[1] + string[5])/25;
                   p.blue = (string[2] + string[6])/25;
                   p.alpha =  (string[3] + string[7])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == N-1)
               {
                   
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m64 *load4;
                   __m64 *load5;
                   __m64 *load6;
                   __m64 *load7;
                   __m128i *load8;
                   
                   
                   load =  (__m128i *) &m[linha][coluna-2].red;
                   load2 = (__m128i *) &m[linha-1][coluna-2].red;
                   load3 = (__m128i *) &m[linha-2][coluna-2].red;
                   
                   load4 = (__m64 *) &m[linha][coluna].red;
                   load5 = (__m64 *) &m[linha -1][coluna].red;
                   load6 = (__m64 *) &m[linha -2][coluna].red;
                   load7 = (__m64 *) string2;
                   load8 = (__m128i *) string;
                   
                   __m128i teste = _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   
                   
                   __m128i teste4 = _mm_add_epi16(teste,_mm_add_epi16(teste2,teste3));
                   
                   __m64 teste5 = _mm_add_pi16(load4[0],_mm_add_pi16(load5[0],load6[0]));
                   load7[0] = teste5;
                   _mm_storeu_si128(&load8[0],teste4);
                   
                   p.red = (string[0] + string[4] + string2[0])/25;
                   p.green =  (string[1] + string[5] + string2[1])/25;
                   p.blue = (string[2] + string[6] + string2[2])/25;
                   p.alpha =  (string[3] + string[7] + string2[3])/25;
                   mTrans[linha][coluna] = p;
               }
               else if(coluna == N-2)
               {
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m128i *load6;
                   __m128i *load7;
                   
                   
                   load =  (__m128i *) &m[linha][coluna-2].red;
                   load2 = (__m128i *) &m[linha][coluna].red;
                   load3 = (__m128i *) &m[linha-1][coluna-2].red;
                   load4 = (__m128i *) &m[linha-1][coluna].red;
                   load5 = (__m128i *) &m[linha-2][coluna-2].red;
                   load6 = (__m128i *) &m[linha-2][coluna].red;
                   load7 = (__m128i *) string;
                   
                   __m128i teste =  _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 =  _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   __m128i teste6 = _mm_loadu_si128(&load6[0]);
                   __m128i teste7 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,_mm_add_epi16(teste5,teste6)))));
                   _mm_storeu_si128(&load7[0],teste7);
                   
                   p.red = (string[0] + string[4])/25;
                   p.green =  (string[1] + string[5])/25;
                   p.blue = (string[2] + string[6])/25;
                   p.alpha =  (string[3] + string[7])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else
               {
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m128i *load6;
                   __m64 *load7;
                   __m64 *load8;
                   __m64 *load9;
                   __m64 *load10;
                   __m128i *load11;
                   
                   load =  (__m128i *) &m[linha][coluna-2].red;
                   load2 = (__m128i *) &m[linha][coluna].red;
                   load3 = (__m128i *) &m[linha-1][coluna-2].red;
                   load4 = (__m128i *) &m[linha-1][coluna].red;
                   load5 = (__m128i *) &m[linha-2][coluna-2].red;
                   load6 = (__m128i *) &m[linha-2][coluna].red;
                   load7 = (__m64 *) &m[linha][coluna+2].red;
                   load8 = (__m64 *) &m[linha -1][coluna+2].red;
                   load9 = (__m64 *) &m[linha -2][coluna+2].red;
                   load10 = (__m64 *) string2;
                   load11 = (__m128i *) string;
                   
                   
                   __m128i teste =  _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 =  _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   __m128i teste6 = _mm_loadu_si128(&load6[0]);
                   __m128i teste7 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,_mm_add_epi16(teste5,teste6)))));
                   _mm_storeu_si128(&load11[0],teste7);
                   
                   __m64 teste8 = _mm_add_pi16(load7[0],_mm_add_pi16(load8[0],load9[0]));
                   load10[0] = teste8;
                   
                   p.red = (string[0] + string[4] + string2[0])/25;
                   p.green =  (string[1] + string[5] + string2[1])/25;
                   p.blue = (string[2] + string[6] + string2[2])/25;
                   p.alpha =  (string[3] + string[7] + string2[3])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               
           }
           else
           {
               if(coluna == 0)
               {
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m64 *load6;
                   __m64 *load7;
                   __m64 *load8;
                   __m64 *load9;
                   __m64 *load10;
                   __m64 *load11;
                   __m128i *load12;
                   
                   load =  (__m128i *) &m[linha][coluna].red;
                   load2 = (__m128i *) &m[linha-1][coluna].red;
                   load3 = (__m128i *) &m[linha-2][coluna].red;
                   load4 = (__m128i *) &m[linha+1][coluna].red;
                   load5 = (__m128i *) &m[linha+2][coluna].red;
                   
                   load6 = (__m64 *) &m[linha][coluna + 2].red;
                   load7 = (__m64 *) &m[linha -2][coluna + 2].red;
                   load8 = (__m64 *) &m[linha -1][coluna + 2].red;
                   load9 = (__m64 *) &m[linha +1][coluna + 2].red;
                   load10 = (__m64 *) &m[linha +2][coluna + 2].red;
                   load11 = (__m64 *) string2;
                   load12 = (__m128i *) string;
                   
                   __m128i teste = _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 = _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   
                   __m128i teste6 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,teste5))));
                   
                   __m64 teste7 = _mm_add_pi16(load6[0],_mm_add_pi16(load7[0],_mm_add_pi16(load8[0],_mm_add_pi16(load9[0],load10[0]))));
                   load11[0] = teste7;
                   _mm_storeu_si128(&load12[0],teste6);
                   
                   p.red = (string[0] + string[4] + string2[0])/25;
                   p.green =  (string[1] + string[5] + string2[1])/25;
                   p.blue = (string[2] + string[6] + string2[2])/25;
                   p.alpha =  (string[3] + string[7] + string2[3])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == 1)
               {
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m128i *load6;
                   __m128i *load7;
                   __m128i *load8;
                   __m128i *load9;
                   __m128i *load10;
                   __m128i *load11;
                   
                   load =  (__m128i *) &m[linha][coluna-1].red;
                   load2 = (__m128i *) &m[linha][coluna+1].red;
                   load3 =  (__m128i *) &m[linha-1][coluna-1].red;
                   load4 = (__m128i *) &m[linha-1][coluna+1].red;
                   load5 = (__m128i *) &m[linha+1][coluna-1].red;
                   load6 = (__m128i *) &m[linha+1][coluna+1].red;
                   load7 = (__m128i *) &m[linha+2][coluna-1].red;
                   load8 = (__m128i *) &m[linha+2][coluna+1].red;
                   load9 = (__m128i *) &m[linha-2][coluna-1].red;
                   load10 = (__m128i *) &m[linha-2][coluna+1].red;
                   load11 = (__m128i *) string;
                   
                   __m128i teste =  _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 =  _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   __m128i teste6 = _mm_loadu_si128(&load6[0]);
                   __m128i teste7 = _mm_loadu_si128(&load7[0]);
                   __m128i teste8 =  _mm_loadu_si128(&load8[0]);
                   __m128i teste9 = _mm_loadu_si128(&load9[0]);
                   __m128i teste10 = _mm_loadu_si128(&load10[0]);
                   __m128i teste11 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,_mm_add_epi16(teste5,_mm_add_epi16(teste6,_mm_add_epi16(teste7,_mm_add_epi16(teste8,_mm_add_epi16(teste9,teste10)))))))));
                   _mm_storeu_si128(&load11[0],teste11);
                   
                   p.red = (string[0] + string[4])/25;
                   p.green =  (string[1] + string[5])/25;
                   p.blue = (string[2] + string[6])/25;
                   p.alpha =  (string[3] + string[7])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               
               else if(coluna == M-1)
               {
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m64 *load6;
                   __m64 *load7;
                   __m64 *load8;
                   __m64 *load9;
                   __m64 *load10;
                   __m64 *load11;
                   __m128i *load12;
                   
                   load =  (__m128i *) &m[linha][coluna-2].red;
                   load2 = (__m128i *) &m[linha-1][coluna-2].red;
                   load3 = (__m128i *) &m[linha-2][coluna-2].red;
                   load4 = (__m128i *) &m[linha+1][coluna-2].red;
                   load5 = (__m128i *) &m[linha+2][coluna-2].red;
                   
                   load6 = (__m64 *) &m[linha][coluna].red;
                   load7 = (__m64 *) &m[linha -2][coluna].red;
                   load8 = (__m64 *) &m[linha -1][coluna].red;
                   load9 = (__m64 *) &m[linha +1][coluna].red;
                   load10 = (__m64 *) &m[linha +2][coluna].red;
                   load11 = (__m64 *) string2;
                   load12 = (__m128i *) string;
                   
                   __m128i teste = _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 = _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   
                   __m128i teste6 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,teste5))));
                   
                   __m64 teste7 = _mm_add_pi16(load6[0],_mm_add_pi16(load7[0],_mm_add_pi16(load8[0],_mm_add_pi16(load9[0],load10[0]))));
                   load11[0] = teste7;
                   _mm_storeu_si128(&load12[0],teste6);
                   
                   p.red = (string[0] + string[4] + string2[0])/25;
                   p.green =  (string[1] + string[5] + string2[1])/25;
                   p.blue = (string[2] + string[6] + string2[2])/25;
                   p.alpha =  (string[3] + string[7] + string2[3])/25;
                   mTrans[linha][coluna] = p;
                   
                   
               }
               else if(coluna == M-2)
               {
                   short string[8];
                   short string2[4];
                   
                   __m128i *load;
                   __m128i *load2;
                   __m128i *load3;
                   __m128i *load4;
                   __m128i *load5;
                   __m128i *load6;
                   __m128i *load7;
                   __m128i *load8;
                   __m128i *load9;
                   __m128i *load10;
                   __m128i *load11;
                   
                   load =  (__m128i *) &m[linha][coluna-2].red;
                   load2 = (__m128i *) &m[linha][coluna].red;
                   load3 =  (__m128i *) &m[linha-1][coluna-2].red;
                   load4 = (__m128i *) &m[linha-1][coluna].red;
                   load5 = (__m128i *) &m[linha+1][coluna-2].red;
                   load6 = (__m128i *) &m[linha+1][coluna].red;
                   load7 = (__m128i *) &m[linha+2][coluna-2].red;
                   load8 = (__m128i *) &m[linha+2][coluna].red;
                   load9 = (__m128i *) &m[linha-2][coluna-2].red;
                   load10 = (__m128i *) &m[linha-2][coluna].red;
                   load11 = (__m128i *) string;
                   
                   __m128i teste =  _mm_loadu_si128(&load[0]);
                   __m128i teste2 = _mm_loadu_si128(&load2[0]);
                   __m128i teste3 = _mm_loadu_si128(&load3[0]);
                   __m128i teste4 =  _mm_loadu_si128(&load4[0]);
                   __m128i teste5 = _mm_loadu_si128(&load5[0]);
                   __m128i teste6 = _mm_loadu_si128(&load6[0]);
                   __m128i teste7 = _mm_loadu_si128(&load7[0]);
                   __m128i teste8 =  _mm_loadu_si128(&load8[0]);
                   __m128i teste9 = _mm_loadu_si128(&load9[0]);
                   __m128i teste10 = _mm_loadu_si128(&load10[0]);
                   __m128i teste11 = _mm_add_epi16(teste,_mm_add_epi16(teste2,_mm_add_epi16(teste3,_mm_add_epi16(teste4,_mm_add_epi16(teste5,_mm_add_epi16(teste6,_mm_add_epi16(teste7,_mm_add_epi16(teste8,_mm_add_epi16(teste9,teste10)))))))));
                   _mm_storeu_si128(&load11[0],teste11);
                   
                   p.red = (string[0] + string[4])/25;
                   p.green =  (string[1] + string[5])/25;
                   p.blue = (string[2] + string[6])/25;
                   p.alpha =  (string[3] + string[7])/25;
                   mTrans[linha][coluna] = p;
                   
               }
               
           }
        
    }
    else
    {
        short string[8];
        short string2[4];
        
        __m256i *load;
        __m256i *load2;
        __m256i *load3;
        __m256i *load4;
        __m256i *load5;
        __m64   *load6;
        __m64   *load7;
        __m64   *load8;
        __m64   *load9;
        __m64   *load10;
        __m64   *load11;
        __m256i *load12;
        
        
        load = (__m256i*) &m[linha-2][coluna-2].red;
        load2 = (__m256i*) &m[linha-1][coluna-2].red;
        load3 = (__m256i*) &m[linha][coluna-2].red;
        load4 = (__m256i*) &m[linha+1][coluna-2].red;
        load5 = (__m256i*) &m[linha+2][coluna-2].red;
        load6 = (__m64 *) &m[linha-2][coluna+2].red;
        load7 = (__m64 *) &m[linha-1][coluna+2].red;
        load8 = (__m64 *) &m[linha][coluna+2].red;
        load9 = (__m64 *) &m[linha+1][coluna+2].red;
        load10 = (__m64 *) &m[linha+2][coluna+2].red;
        
        load11 = (__m64 *)string2;
        load12 = (__m256i*)string;
        
        __m256i teste =  _mm_loadu_si256(&load[0]);
        __m256i teste2 = _mm_loadu_si256(&load2[0]);
        __m256i teste3 = _mm_loadu_si256(&load3[0]);
        __m256i teste4 = _mm_loadu_si256(&load4[0]);
        __m256i teste5 = _mm_loadu_si256(&load5[0]);
        
        __m256i teste6 = _mm256_add_epi16(teste,_mm256_add_epi16(teste2,_mm256_add_epi16(teste3,_mm256_add_epi16(teste4,teste5))));
        __m64 teste7 = _mm_add_pi16(load11[0],_mm_add_pi16(load12[0],_mm_add_pi16(load13[0],_mm_add_pi16(load14[0],load15[0]))));
        load11[0] = teste7;
        _mm_storeu_si128(&load12[0],teste6);
       
        
        p.red = (string[0] + string[4] + string2[0])/25;
        p.green =  (string[1] + string[5] + string2[1])/25;
        p.blue = (string[2] + string[6] + string2[2])/25;
        p.alpha =  (string[3] + string[7] + string2[3])/25;
        mTrans[linha][coluna] = p;
        
    }
}



void transformarMatriz(pix **m,pix **mTrans)
{
    int i;
    int j;
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
            
        {
            funcAux(m,mTrans,i,j);
        }
    }
}

pix **alocarMatriz()
{
    int i;
    pix **p;
    p = (pix **) malloc(M * sizeof(pix *));
    
    //p = (pix **) _mm_malloc(M*sizeof(pix *),16);
    
    for(i=0;i<M;i++)
    {
        p[i] = (pix *) malloc(N * sizeof(pix));
        //        p[i] = (pix *) _mm_malloc(N * sizeof(pix),16);
    }
    
    return p;
}


void MontarMatriz(pix **m)
{
    int i;
    int j;
    pix p;
    
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
        {
            p.red = rand() % 256;
            p.green = rand() % 256;
            p.blue = rand() % 256;
            p.alpha = rand() % 256;
            m[i][j] = p;
        }
    }
    
}

void imprimirMatriz(pix **m)
{
    int i;
    int j;
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
        {
            printf("Matriz RED[%d][%d] = %d\n",i,j,m[i][j].red);
            //	printf("Matriz GREEN[%d][%d] = %d\n",i,j,m[i][j].green);
            //	printf("Matriz BLUE[%d][%d] = %d\n",i,j,m[i][j].blue);
            //	printf("Matriz ALPHA[%d][%d] = %d\n",i,j,m[i][j].alpha);
        }			
    }	
}

