#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "xmmintrin.h"
#define M 9000
#define N 9000


typedef struct  pixel
{
    float red;
    float green;
    float blue;
    float alpha;
    
}pix;

__m128 divisao;


pix **alocarMatriz(); //aloca uma matriz que contem em cada posiç uma structt
void MontarMatriz(pix **m); //poe em cada posiç da matriz uma struct,gerando numeros aleatorios de 0 a 255 para o RGBA
void imprimirMatriz(pix **m);

void transformarMatriz(pix **m,pix **mTrans);
void funcAux(pix **m,pix **mTrans,int linha,int coluna);

int contador = 0;


int main()
{
    time_t start,stop;
    double avg_time = 0;
    double cur_time;
    pix **p = alocarMatriz(); //matriz de structs
    pix **vTrans = alocarMatriz();
    MontarMatriz(p);
    //imprimirMatriz(p);
    start = clock();
    divisao = _mm_set_ps1(25.0);
    transformarMatriz(p,vTrans);
    stop = clock();
    cur_time = ((double) stop-start) / CLOCKS_PER_SEC;
    printf("Averagely used %f seconds.\n", cur_time);
    //	printf("Contador = %d\n",contador);
    //imprimirMatriz(vTrans);
    return 0;
    
}

void funcAux(pix **m,pix **mTrans,int linha,int coluna)
{
    int i;
    pix p;
    
    
    
    if(linha <= 1 || coluna <= 1 || linha >= (M-2) || coluna >= (N-2))
    {
        
	       if(linha == 0)
           {
               
               if(coluna == 0)
               {
                   
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna+1].red);
                   z = _mm_loadu_ps(&m[linha][coluna+2].red);
                   x1 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   y1 = _mm_loadu_ps(&m[linha+1][coluna+1].red);
                   z1 = _mm_loadu_ps(&m[linha+1][coluna+2].red);
                   x2 = _mm_loadu_ps(&m[linha+2][coluna].red);
                   y2 = _mm_loadu_ps(&m[linha+2][coluna+1].red);
                   z2 = _mm_loadu_ps(&m[linha+2][coluna+2].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   //full3 = _mm_div_ps(full3,divisao);
                   
                   p.red = full3[0]/25;
                   p.green = full3[1]/25;
                   p.blue = full3[2]/25;
                   p.alpha = full3[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == 1)
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna+1].red);
                   z = _mm_loadu_ps(&m[linha][coluna+2].red);
                   x1 = _mm_loadu_ps(&m[linha][coluna-1].red);
                   y1 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   z1 = _mm_loadu_ps(&m[linha+1][coluna+1].red);
                   x2 = _mm_loadu_ps(&m[linha+1][coluna+2].red);
                   y2 = _mm_loadu_ps(&m[linha+1][coluna-1].red);
                   z2 = _mm_loadu_ps(&m[linha+2][coluna].red);
                   x3 = _mm_loadu_ps(&m[linha+2][coluna+1].red);
                   y3 = _mm_loadu_ps(&m[linha+2][coluna+2].red);
                   z3 = _mm_loadu_ps(&m[linha+2][coluna-1].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(x3,_mm_add_ps(y3,z3));
                   full4 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   full5 = _mm_add_ps(full3,full4);
                   //full3 = _mm_div_ps(full3,divisao);
                   
                   p.red = full5[0]/25;
                   p.green = full5[1]/25;
                   p.blue = full5[2]/25;
                   p.alpha = full5[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
               }
               else if(coluna == N-1)
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna-1].red);
                   z = _mm_loadu_ps(&m[linha][coluna-2].red);
                   x1 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   y1 = _mm_loadu_ps(&m[linha+1][coluna-1].red);
                   z1 = _mm_loadu_ps(&m[linha+1][coluna-2].red);
                   x2 = _mm_loadu_ps(&m[linha+2][coluna].red);
                   y2 = _mm_loadu_ps(&m[linha+2][coluna-1].red);
                   z2 = _mm_loadu_ps(&m[linha+2][coluna-2].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   //full3 = _mm_div_ps(full3,divisao);
                   
                   p.red = full3[0]/25;
                   p.green = full3[1]/25;
                   p.blue = full3[2]/25;
                   p.alpha = full3[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == N-2)
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna-1].red);
                   z = _mm_loadu_ps(&m[linha][coluna-2].red);
                   x1 = _mm_loadu_ps(&m[linha][coluna+1].red);
                   y1 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   z1 = _mm_loadu_ps(&m[linha+1][coluna-1].red);
                   x2 = _mm_loadu_ps(&m[linha+1][coluna-2].red);
                   y2 = _mm_loadu_ps(&m[linha+1][coluna+1].red);
                   z2 = _mm_loadu_ps(&m[linha+2][coluna].red);
                   x3 = _mm_loadu_ps(&m[linha+2][coluna-1].red);
                   y3 = _mm_loadu_ps(&m[linha+2][coluna-2].red);
                   z3 = _mm_loadu_ps(&m[linha+2][coluna+1].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(x3,_mm_add_ps(y3,z3));
                   full4 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   full5 = _mm_add_ps(full3,full4);
                   //full3 = _mm_div_ps(full3,divisao);
                   
                   p.red = full5[0]/25;
                   p.green = full5[1]/25;
                   p.blue = full5[2]/25;
                   p.alpha = full5[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
                   
               }
               else
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 x4;
                   __m128 y4;
                   __m128 z4;
                   
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   __m128 full6;
                   __m128 full7;
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna-1].red);
                   z = _mm_loadu_ps(&m[linha][coluna-2].red);
                   x1 = _mm_loadu_ps(&m[linha][coluna+1].red);
                   y1 = _mm_loadu_ps(&m[linha][coluna+2].red);
                   z1 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   x2 = _mm_loadu_ps(&m[linha+1][coluna-1].red);
                   y2 = _mm_loadu_ps(&m[linha+1][coluna-2].red);
                   z2 = _mm_loadu_ps(&m[linha+1][coluna+1].red);
                   x3 = _mm_loadu_ps(&m[linha+1][coluna+2].red);
                   y3 = _mm_loadu_ps(&m[linha+2][coluna].red);
                   z3 = _mm_loadu_ps(&m[linha+2][coluna-1].red);
                   x4 = _mm_loadu_ps(&m[linha+2][coluna-2].red);
                   y4 = _mm_loadu_ps(&m[linha+2][coluna+1].red);
                   z4 = _mm_loadu_ps(&m[linha+2][coluna+2].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(x3,_mm_add_ps(y3,z3));
                   full4 = _mm_add_ps(x4,_mm_add_ps(y4,z4));
                   full5 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   full6 = _mm_add_ps(full3,full4);
                   full7 = _mm_add_ps(full5,full6);
                   //full3 = _mm_div_ps(full3,divisao);
                   
                   p.red = full7[0]/25;
                   p.green = full7[1]/25;
                   p.blue = full7[2]/25;
                   p.alpha = full7[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               
           }
           else if(linha == 1)
           {
               
               if(coluna == 0)
               {
                   
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna+1].red);
                   z = _mm_loadu_ps(&m[linha][coluna+2].red);
                   x1 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   y1 = _mm_loadu_ps(&m[linha+1][coluna+1].red);
                   z1 = _mm_loadu_ps(&m[linha+1][coluna+2].red);
                   x2 = _mm_loadu_ps(&m[linha+2][coluna].red);
                   y2 = _mm_loadu_ps(&m[linha+2][coluna+1].red);
                   z2 = _mm_loadu_ps(&m[linha+2][coluna+2].red);
                   x3 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   y3 = _mm_loadu_ps(&m[linha-1][coluna+1].red);
                   z3 = _mm_loadu_ps(&m[linha-1][coluna+2].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(x3,_mm_add_ps(y3,z3));
                   full4 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   full5 = _mm_add_ps(full3,full4);
                   
                   
                   p.red = full5[0]/25;
                   p.green = full5[1]/25;
                   p.blue = full5[2]/25;
                   p.alpha = full5[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == 1)
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 x4;
                   __m128 y4;
                   __m128 z4;
		   __m128 x5;
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   __m128 full6;
                   
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna+1].red);
                   z = _mm_loadu_ps(&m[linha][coluna+2].red);
                   x1 = _mm_loadu_ps(&m[linha][coluna-1].red);
                   y1 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   z1 = _mm_loadu_ps(&m[linha+1][coluna+1].red);
                   x2 = _mm_loadu_ps(&m[linha+1][coluna+2].red);
                   y2 = _mm_loadu_ps(&m[linha+1][coluna-1].red);
                   z2 = _mm_loadu_ps(&m[linha+2][coluna].red);
                   x3 = _mm_loadu_ps(&m[linha+2][coluna+1].red);
                   y3 = _mm_loadu_ps(&m[linha+2][coluna+2].red);
                   z3 = _mm_loadu_ps(&m[linha+2][coluna-1].red);
                   x4 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   y4 = _mm_loadu_ps(&m[linha-1][coluna+1].red);
                   z4 = _mm_loadu_ps(&m[linha-1][coluna+2].red);
                   x5 = _mm_loadu_ps(&m[linha-1][coluna-1].red);

		   full = _mm_add_ps(_mm_add_ps(x,x1),_mm_add_ps(y,z));
                   full1 = _mm_add_ps(_mm_add_ps(y1,y2),_mm_add_ps(z1,x2));
                   full2 = _mm_add_ps(_mm_add_ps(z2,z3),_mm_add_ps(x3,y3));
                   full3 = _mm_add_ps(_mm_add_ps(x4,x5),_mm_add_ps(y4,z4));
                   full4 = _mm_add_ps(full,full1);
                   full5 = _mm_add_ps(full2,full3);
                   full6 = _mm_add_ps(full4,full5);
                                    
                   p.red = full6[0]/25;
                   p.green = full6[1]/25;
                   p.blue = full6[2]/25;
                   p.alpha = full6[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
               }
               else if(coluna == N-1)
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   __m128 full6;

                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna-1].red);
                   z = _mm_loadu_ps(&m[linha][coluna-2].red);
                   x1 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   y1 = _mm_loadu_ps(&m[linha+1][coluna-1].red);
                   z1 = _mm_loadu_ps(&m[linha+1][coluna-2].red);
                   x2 = _mm_loadu_ps(&m[linha+2][coluna].red);
                   y2 = _mm_loadu_ps(&m[linha+2][coluna-1].red);
                   z2 = _mm_loadu_ps(&m[linha+2][coluna-2].red);
                   x3 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   y3 = _mm_loadu_ps(&m[linha-1][coluna-1].red);
                   z3 = _mm_loadu_ps(&m[linha-1][coluna-2].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(x3,_mm_add_ps(y3,z3));
                   full4 = _mm_add_ps(full,full1);
                   full5 = _mm_add_ps(full2,full3);
                   full6 = _mm_add_ps(full4,full5);
                   //full3 = _mm_div_ps(full3,divisao);
                   
                   p.red = full6[0]/25;
                   p.green = full6[1]/25;
                   p.blue = full6[2]/25;
                   p.alpha = full6[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == N-2)
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 x4;
                   __m128 y4;
                   __m128 z4;
                   __m128 x5;
                   
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
		   __m128 full6;
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna-1].red);
                   z = _mm_loadu_ps(&m[linha][coluna-2].red);
                   x1 = _mm_loadu_ps(&m[linha][coluna+1].red);
                   y1 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   z1 = _mm_loadu_ps(&m[linha+1][coluna-1].red);
                   x2 = _mm_loadu_ps(&m[linha+1][coluna-2].red);
                   y2 = _mm_loadu_ps(&m[linha+1][coluna+1].red);
                   z2 = _mm_loadu_ps(&m[linha+2][coluna].red);
                   x3 = _mm_loadu_ps(&m[linha+2][coluna-1].red);
                   y3 = _mm_loadu_ps(&m[linha+2][coluna-2].red);
                   z3 = _mm_loadu_ps(&m[linha+2][coluna+1].red);
                   x4 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   y4 = _mm_loadu_ps(&m[linha-1][coluna-1].red);
                   z4 = _mm_loadu_ps(&m[linha-1][coluna-2].red);
                   x5 = _mm_loadu_ps(&m[linha-1][coluna+1].red);

                   full = _mm_add_ps(_mm_add_ps(x,x1),_mm_add_ps(y,z));
                   full1 = _mm_add_ps(_mm_add_ps(y1,y2),_mm_add_ps(z1,x2));
                   full2 = _mm_add_ps(_mm_add_ps(z2,z3),_mm_add_ps(x3,y3));
                   full3 = _mm_add_ps(_mm_add_ps(x4,x5),_mm_add_ps(y4,z4));
                   full4 = _mm_add_ps(full,full1);
                   full5 = _mm_add_ps(full2,full3);
                   full6 = _mm_add_ps(full4,full5);
                   //full3 = _mm_div_ps(full3,divisao);
                   
                   p.red = full6[0]/25;
                   p.green = full6[1]/25;
                   p.blue = full6[2]/25;
                   p.alpha = full6[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
                   
               }
               else
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 x4;
                   __m128 y4;
                   __m128 z4;
                   __m128 x5;
                   __m128 y5;
                   __m128 z5;
                   __m128 x6;
                   __m128 y6; 
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   __m128 full6;
                   __m128 full7;
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna-1].red);
                   z = _mm_loadu_ps(&m[linha][coluna-2].red);
                   x1 = _mm_loadu_ps(&m[linha][coluna+1].red);
                   y1 = _mm_loadu_ps(&m[linha][coluna+2].red);
                   z1 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   x2 = _mm_loadu_ps(&m[linha+1][coluna-1].red);
                   y2 = _mm_loadu_ps(&m[linha+1][coluna-2].red);
                   z2 = _mm_loadu_ps(&m[linha+1][coluna+1].red);
                   x3 = _mm_loadu_ps(&m[linha+1][coluna+2].red);
                   y3 = _mm_loadu_ps(&m[linha+2][coluna].red);
                   z3 = _mm_loadu_ps(&m[linha+2][coluna-1].red);
                   x4 = _mm_loadu_ps(&m[linha+2][coluna-2].red);
                   y4 = _mm_loadu_ps(&m[linha+2][coluna+1].red);
                   z4 = _mm_loadu_ps(&m[linha+2][coluna+2].red);
                   x5 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   y5 = _mm_loadu_ps(&m[linha-1][coluna-1].red);
                   z5 = _mm_loadu_ps(&m[linha-1][coluna-2].red);
                   x6 = _mm_loadu_ps(&m[linha-1][coluna+1].red);
                   y6 = _mm_loadu_ps(&m[linha-1][coluna+2].red);
                   
                   full = _mm_add_ps(_mm_add_ps(x,x1),_mm_add_ps(y,z));
                   full1 = _mm_add_ps(_mm_add_ps(y1,y2),_mm_add_ps(z1,x2));
                   full2 = _mm_add_ps(_mm_add_ps(z2,z3),_mm_add_ps(x3,y3));
                   full3 = _mm_add_ps(_mm_add_ps(x4,x5),_mm_add_ps(y4,z4));
                   full4 = _mm_add_ps(_mm_add_ps(y5,y6),_mm_add_ps(z5,x6));
                   full5 = _mm_add_ps(full,full1);
		   full6 = _mm_add_ps(full2,_mm_add_ps(full3,full4));
                   full7 = _mm_add_ps(full5,full6);
                   //full3 = _mm_div_ps(full3,divisao);
                   
                   p.red = full7[0]/25;
                   p.green = full7[1]/25;
                   p.blue = full7[2]/25;
                   p.alpha = full7[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               
           }
           else if(linha == M-2)
	   {
		if(coluna == 0)
               {
                   
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna+1].red);
                   z = _mm_loadu_ps(&m[linha][coluna+2].red);
                   x1 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   y1 = _mm_loadu_ps(&m[linha-1][coluna+1].red);
                   z1 = _mm_loadu_ps(&m[linha-1][coluna+2].red);
                   x2 = _mm_loadu_ps(&m[linha-2][coluna].red);
                   y2 = _mm_loadu_ps(&m[linha-2][coluna+1].red);
                   z2 = _mm_loadu_ps(&m[linha-2][coluna+2].red);
                   x3 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   y3 = _mm_loadu_ps(&m[linha+1][coluna+1].red);
                   z3 = _mm_loadu_ps(&m[linha+1][coluna+2].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(x3,_mm_add_ps(y3,z3));
                   full4 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   full5 = _mm_add_ps(full3,full4);
                   
                   
                   p.red = full5[0]/25;
                   p.green = full5[1]/25;
                   p.blue = full5[2]/25;
                   p.alpha = full5[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == 1)
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 x4;
                   __m128 y4;
                   __m128 z4;
		   __m128 x5;
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   __m128 full6;
                   
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna+1].red);
                   z = _mm_loadu_ps(&m[linha][coluna+2].red);
                   x1 = _mm_loadu_ps(&m[linha][coluna-1].red);
                   y1 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   z1 = _mm_loadu_ps(&m[linha-1][coluna+1].red);
                   x2 = _mm_loadu_ps(&m[linha-1][coluna+2].red);
                   y2 = _mm_loadu_ps(&m[linha-1][coluna-1].red);
                   z2 = _mm_loadu_ps(&m[linha-2][coluna].red);
                   x3 = _mm_loadu_ps(&m[linha-2][coluna+1].red);
                   y3 = _mm_loadu_ps(&m[linha-2][coluna+2].red);
                   z3 = _mm_loadu_ps(&m[linha-2][coluna-1].red);
                   x4 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   y4 = _mm_loadu_ps(&m[linha+1][coluna+1].red);
                   z4 = _mm_loadu_ps(&m[linha+1][coluna+2].red);
                   x5 = _mm_loadu_ps(&m[linha+1][coluna-1].red);

		   full = _mm_add_ps(_mm_add_ps(x,x1),_mm_add_ps(y,z));
                   full1 = _mm_add_ps(_mm_add_ps(y1,y2),_mm_add_ps(z1,x2));
                   full2 = _mm_add_ps(_mm_add_ps(z2,z3),_mm_add_ps(x3,y3));
                   full3 = _mm_add_ps(_mm_add_ps(x4,x5),_mm_add_ps(y4,z4));
                   full4 = _mm_add_ps(full,full1);
                   full5 = _mm_add_ps(full2,full3);
                   full6 = _mm_add_ps(full4,full5);
                                    
                   p.red = full6[0]/25;
                   p.green = full6[1]/25;
                   p.blue = full6[2]/25;
                   p.alpha = full6[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
               }
               else if(coluna == N-1)
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   __m128 full6;

                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna-1].red);
                   z = _mm_loadu_ps(&m[linha][coluna-2].red);
                   x1 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   y1 = _mm_loadu_ps(&m[linha-1][coluna-1].red);
                   z1 = _mm_loadu_ps(&m[linha-1][coluna-2].red);
                   x2 = _mm_loadu_ps(&m[linha-2][coluna].red);
                   y2 = _mm_loadu_ps(&m[linha-2][coluna-1].red);
                   z2 = _mm_loadu_ps(&m[linha-2][coluna-2].red);
                   x3 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   y3 = _mm_loadu_ps(&m[linha+1][coluna-1].red);
                   z3 = _mm_loadu_ps(&m[linha+1][coluna-2].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(x3,_mm_add_ps(y3,z3));
                   full4 = _mm_add_ps(full,full1);
                   full5 = _mm_add_ps(full2,full3);
                   full6 = _mm_add_ps(full4,full5);
                   //full3 = _mm_div_ps(full3,divisao);
                   
                   p.red = full6[0]/25;
                   p.green = full6[1]/25;
                   p.blue = full6[2]/25;
                   p.alpha = full6[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
               else if(coluna == N-2)
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 x4;
                   __m128 y4;
                   __m128 z4;
                   __m128 x5;
                   
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
		   __m128 full6;
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna-1].red);
                   z = _mm_loadu_ps(&m[linha][coluna-2].red);
                   x1 = _mm_loadu_ps(&m[linha][coluna+1].red);
                   y1 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   z1 = _mm_loadu_ps(&m[linha-1][coluna-1].red);
                   x2 = _mm_loadu_ps(&m[linha-1][coluna-2].red);
                   y2 = _mm_loadu_ps(&m[linha-1][coluna+1].red);
                   z2 = _mm_loadu_ps(&m[linha-2][coluna].red);
                   x3 = _mm_loadu_ps(&m[linha-2][coluna-1].red);
                   y3 = _mm_loadu_ps(&m[linha-2][coluna-2].red);
                   z3 = _mm_loadu_ps(&m[linha-2][coluna+1].red);
                   x4 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   y4 = _mm_loadu_ps(&m[linha+1][coluna-1].red);
                   z4 = _mm_loadu_ps(&m[linha+1][coluna-2].red);
                   x5 = _mm_loadu_ps(&m[linha+1][coluna+1].red);

                   full = _mm_add_ps(_mm_add_ps(x,x1),_mm_add_ps(y,z));
                   full1 = _mm_add_ps(_mm_add_ps(y1,y2),_mm_add_ps(z1,x2));
                   full2 = _mm_add_ps(_mm_add_ps(z2,z3),_mm_add_ps(x3,y3));
                   full3 = _mm_add_ps(_mm_add_ps(x4,x5),_mm_add_ps(y4,z4));
                   full4 = _mm_add_ps(full,full1);
                   full5 = _mm_add_ps(full2,full3);
                   full6 = _mm_add_ps(full4,full5);
                   //full3 = _mm_div_ps(full3,divisao);
                   
                   p.red = full6[0]/25;
                   p.green = full6[1]/25;
                   p.blue = full6[2]/25;
                   p.alpha = full6[3]/25;
                   mTrans[linha][coluna] = p;
                   
                   
                   
               }
               else
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 x4;
                   __m128 y4;
                   __m128 z4;
                   __m128 x5;
                   __m128 y5;
                   __m128 z5;
                   __m128 x6;
                   __m128 y6; 
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   __m128 full6;
                   __m128 full7;
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna-1].red);
                   z = _mm_loadu_ps(&m[linha][coluna-2].red);
                   x1 = _mm_loadu_ps(&m[linha][coluna+1].red);
                   y1 = _mm_loadu_ps(&m[linha][coluna+2].red);
                   z1 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   x2 = _mm_loadu_ps(&m[linha-1][coluna-1].red);
                   y2 = _mm_loadu_ps(&m[linha-1][coluna-2].red);
                   z2 = _mm_loadu_ps(&m[linha-1][coluna+1].red);
                   x3 = _mm_loadu_ps(&m[linha-1][coluna+2].red);
                   y3 = _mm_loadu_ps(&m[linha-2][coluna].red);
                   z3 = _mm_loadu_ps(&m[linha-2][coluna-1].red);
                   x4 = _mm_loadu_ps(&m[linha-2][coluna-2].red);
                   y4 = _mm_loadu_ps(&m[linha-2][coluna+1].red);
                   z4 = _mm_loadu_ps(&m[linha-2][coluna+2].red);
                   x5 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   y5 = _mm_loadu_ps(&m[linha+1][coluna-1].red);
                   z5 = _mm_loadu_ps(&m[linha+1][coluna-2].red);
                   x6 = _mm_loadu_ps(&m[linha+1][coluna+1].red);
                   y6 = _mm_loadu_ps(&m[linha+1][coluna+2].red);
                   
                   full = _mm_add_ps(_mm_add_ps(x,x1),_mm_add_ps(y,z));
                   full1 = _mm_add_ps(_mm_add_ps(y1,y2),_mm_add_ps(z1,x2));
                   full2 = _mm_add_ps(_mm_add_ps(z2,z3),_mm_add_ps(x3,y3));
                   full3 = _mm_add_ps(_mm_add_ps(x4,x5),_mm_add_ps(y4,z4));
                   full4 = _mm_add_ps(_mm_add_ps(y5,y6),_mm_add_ps(z5,x6));
                   full5 = _mm_add_ps(full,full1);
		   full6 = _mm_add_ps(full2,_mm_add_ps(full3,full4));
                   full7 = _mm_add_ps(full5,full6);
                   //full3 = _mm_div_ps(full3,divisao);
                   
                   p.red = full7[0]/25;
                   p.green = full7[1]/25;
                   p.blue = full7[2]/25;
                   p.alpha = full7[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }

	   }
           else if(linha == M-1)
           {
               if(coluna == 0)
               {
                   
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;

                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna+1].red);
                   z = _mm_loadu_ps(&m[linha][coluna+2].red);
                   x1 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   y1 = _mm_loadu_ps(&m[linha-1][coluna+1].red);
                   z1 = _mm_loadu_ps(&m[linha-1][coluna+2].red);
                   x2 = _mm_loadu_ps(&m[linha-2][coluna].red);
                   y2 = _mm_loadu_ps(&m[linha-2][coluna+1].red);
                   z2 = _mm_loadu_ps(&m[linha-2][coluna+2].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   //full3 = _mm_div_ps(full3,divisao);

                   p.red = full3[0]/25;
                   p.green = full3[1]/25;
                   p.blue = full3[2]/25;
                   p.alpha = full3[3]/25;
                   mTrans[linha][coluna] = p;

               }
               else if(coluna == 1)
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;

                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;

                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna+1].red);
                   z = _mm_loadu_ps(&m[linha][coluna+2].red);
                   x1 = _mm_loadu_ps(&m[linha][coluna-1].red);
                   y1 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   z1 = _mm_loadu_ps(&m[linha-1][coluna+1].red);
                   x2 = _mm_loadu_ps(&m[linha-1][coluna+2].red);
                   y2 = _mm_loadu_ps(&m[linha-1][coluna-1].red);
                   z2 = _mm_loadu_ps(&m[linha-2][coluna].red);
                   x3 = _mm_loadu_ps(&m[linha-2][coluna+1].red);
                   y3 = _mm_loadu_ps(&m[linha-2][coluna+2].red);
                   z3 = _mm_loadu_ps(&m[linha-2][coluna-1].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(x3,_mm_add_ps(y3,z3));
                   full4 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   full5 = _mm_add_ps(full3,full4);
                   //full3 = _mm_div_ps(full3,divisao);

                   p.red = full5[0]/25;
                   p.green = full5[1]/25;
                   p.blue = full5[2]/25;
                   p.alpha = full5[3]/25;
                   mTrans[linha][coluna] = p;

               }
               else if(coluna == N-1)
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna-1].red);
                   z = _mm_loadu_ps(&m[linha][coluna-2].red);
                   x1 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   y1 = _mm_loadu_ps(&m[linha-1][coluna-1].red);
                   z1 = _mm_loadu_ps(&m[linha-1][coluna-2].red);
                   x2 = _mm_loadu_ps(&m[linha-2][coluna].red);
                   y2 = _mm_loadu_ps(&m[linha-2][coluna-1].red);
                   z2 = _mm_loadu_ps(&m[linha-2][coluna-2].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   //full3 = _mm_div_ps(full3,divisao);
                   
                   p.red = full3[0]/25;
                   p.green = full3[1]/25;
                   p.blue = full3[2]/25;
                   p.alpha = full3[3]/25;
                   mTrans[linha][coluna] = p;                   
                 
                   
               }
               else if(coluna == N-2)
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna-1].red);
                   z = _mm_loadu_ps(&m[linha][coluna-2].red);
                   x1 = _mm_loadu_ps(&m[linha][coluna+1].red);
                   y1 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   z1 = _mm_loadu_ps(&m[linha-1][coluna-1].red);
                   x2 = _mm_loadu_ps(&m[linha-1][coluna-2].red);
                   y2 = _mm_loadu_ps(&m[linha-1][coluna+1].red);
                   z2 = _mm_loadu_ps(&m[linha-2][coluna].red);
                   x3 = _mm_loadu_ps(&m[linha-2][coluna-1].red);
                   y3 = _mm_loadu_ps(&m[linha-2][coluna-2].red);
                   z3 = _mm_loadu_ps(&m[linha-2][coluna+1].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(x3,_mm_add_ps(y3,z3));
                   full4 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   full5 = _mm_add_ps(full3,full4);
                   //full3 = _mm_div_ps(full3,divisao);
                   
                   p.red = full5[0]/25;
                   p.green = full5[1]/25;
                   p.blue = full5[2]/25;
                   p.alpha = full5[3]/25;
                   mTrans[linha][coluna] = p;
                   
           
               }
               else
               {
                                                                                                        
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 x4;
                   __m128 y4;
                   __m128 z4;
                   
                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   __m128 full6;
                   __m128 full7;
                   
                   x = _mm_loadu_ps(&m[linha][coluna].red);
                   y = _mm_loadu_ps(&m[linha][coluna-1].red);
                   z = _mm_loadu_ps(&m[linha][coluna-2].red);
                   x1 = _mm_loadu_ps(&m[linha][coluna+1].red);
                   y1 = _mm_loadu_ps(&m[linha][coluna+2].red);
                   z1 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   x2 = _mm_loadu_ps(&m[linha-1][coluna-1].red);
                   y2 = _mm_loadu_ps(&m[linha-1][coluna-2].red);
                   z2 = _mm_loadu_ps(&m[linha-1][coluna+1].red);
                   x3 = _mm_loadu_ps(&m[linha-1][coluna+2].red);
                   y3 = _mm_loadu_ps(&m[linha-2][coluna].red);
                   z3 = _mm_loadu_ps(&m[linha-2][coluna-1].red);
                   x4 = _mm_loadu_ps(&m[linha-2][coluna-2].red);
                   y4 = _mm_loadu_ps(&m[linha-2][coluna+1].red);
                   z4 = _mm_loadu_ps(&m[linha-2][coluna+2].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(x3,_mm_add_ps(y3,z3));
                   full4 = _mm_add_ps(x4,_mm_add_ps(y4,z4));
                   full5 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   full6 = _mm_add_ps(full3,full4);
                   full7 = _mm_add_ps(full5,full6);
                   //full3 = _mm_div_ps(full3,divisao);
                   
                   p.red = full7[0]/25;
                   p.green = full7[1]/25;
                   p.blue = full7[2]/25;
                   p.alpha = full7[3]/25;
                   mTrans[linha][coluna] = p;                  
                   
               }
               
           }
           else
           {
               if(coluna == 0)
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 x4;
                   __m128 y4;
                   __m128 z4;

                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   __m128 full6;
                   __m128 full7;
                   
                   x = _mm_loadu_ps(&m[linha-2][coluna].red);
                   y = _mm_loadu_ps(&m[linha-2][coluna+1].red);
                   z = _mm_loadu_ps(&m[linha-2][coluna+2].red);
                   x1 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   y1 = _mm_loadu_ps(&m[linha-1][coluna+1].red);
                   z1 = _mm_loadu_ps(&m[linha-1][coluna+2].red);
                   x2 = _mm_loadu_ps(&m[linha][coluna].red);
                   y2 = _mm_loadu_ps(&m[linha][coluna+1].red);
                   z2 = _mm_loadu_ps(&m[linha][coluna+2].red);
                   x3 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   y3 = _mm_loadu_ps(&m[linha+1][coluna+1].red);
                   z3 = _mm_loadu_ps(&m[linha+1][coluna+2].red);
                   x4 = _mm_loadu_ps(&m[linha+2][coluna].red);
                   y4 = _mm_loadu_ps(&m[linha+2][coluna+1].red);
                   z4 = _mm_loadu_ps(&m[linha+2][coluna+2].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(x3,_mm_add_ps(y3,z3));
                   full4 = _mm_add_ps(x4,_mm_add_ps(y4,z4));
                   full5 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   full6 = _mm_add_ps(full3,full4);
                   full7 = _mm_add_ps(full5,full6);


                   p.red = full7[0]/25;
                   p.green = full7[1]/25;
                   p.blue = full7[2]/25;
                   p.alpha = full7[3]/25;
                   mTrans[linha][coluna] = p;
                   
               }
	       else if(coluna == 1)
	       {
		
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 x4;
                   __m128 y4;
                   __m128 z4;
                   __m128 x5;
                   __m128 y5;
                   __m128 z5;
                   __m128 x6;
                   __m128 y6;

                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   __m128 full6;
                   __m128 full7;

                   x = _mm_loadu_ps(&m[linha-2][coluna-1].red);
                   y = _mm_loadu_ps(&m[linha-2][coluna].red);
                   z = _mm_loadu_ps(&m[linha-2][coluna+1].red);
                   x1 = _mm_loadu_ps(&m[linha-2][coluna+2].red);
                   y1 = _mm_loadu_ps(&m[linha-1][coluna-1].red);
                   z1 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   x2 = _mm_loadu_ps(&m[linha-1][coluna+1].red);
                   y2 = _mm_loadu_ps(&m[linha-1][coluna+2].red);
                   z2 = _mm_loadu_ps(&m[linha][coluna-1].red);
                   x3 = _mm_loadu_ps(&m[linha][coluna].red);
                   y3 = _mm_loadu_ps(&m[linha][coluna+1].red);
                   z3 = _mm_loadu_ps(&m[linha][coluna+2].red);
                   x4 = _mm_loadu_ps(&m[linha+1][coluna-1].red);
                   y4 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   z4 = _mm_loadu_ps(&m[linha+1][coluna+1].red);
                   x5 = _mm_loadu_ps(&m[linha+1][coluna+2].red);
                   y5 = _mm_loadu_ps(&m[linha+2][coluna-1].red);
                   z5 = _mm_loadu_ps(&m[linha+2][coluna].red);
                   x6 = _mm_loadu_ps(&m[linha+2][coluna+1].red);
                   y6 = _mm_loadu_ps(&m[linha+2][coluna+2].red);

                   full = _mm_add_ps(_mm_add_ps(x,x1),_mm_add_ps(y,z));
                   full1 = _mm_add_ps(_mm_add_ps(y1,y2),_mm_add_ps(z1,x2));
                   full2 = _mm_add_ps(_mm_add_ps(z2,z3),_mm_add_ps(x3,y3));
                   full3 = _mm_add_ps(_mm_add_ps(x4,x5),_mm_add_ps(y4,z4));
                   full4 = _mm_add_ps(_mm_add_ps(y5,y6),_mm_add_ps(z5,x6));
		   full5 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   full6 = _mm_add_ps(full3,full4);
                   full7 = _mm_add_ps(full5,full6);	

                   p.red = full7[0]/25;
                   p.green = full7[1]/25;
                   p.blue = full7[2]/25;
                   p.alpha = full7[3]/25;
                   mTrans[linha][coluna] = p;


	       }
               else if(coluna == M-1)
               {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 x4;
                   __m128 y4;
                   __m128 z4;

                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   __m128 full6;
                   __m128 full7;

                   x = _mm_loadu_ps(&m[linha-2][coluna].red);
                   y = _mm_loadu_ps(&m[linha-2][coluna-1].red);
                   z = _mm_loadu_ps(&m[linha-2][coluna-2].red);
                   x1 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   y1 = _mm_loadu_ps(&m[linha-1][coluna-1].red);
                   z1 = _mm_loadu_ps(&m[linha-1][coluna-2].red);
                   x2 = _mm_loadu_ps(&m[linha][coluna].red);
                   y2 = _mm_loadu_ps(&m[linha][coluna-1].red);
                   z2 = _mm_loadu_ps(&m[linha][coluna-2].red);
                   x3 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   y3 = _mm_loadu_ps(&m[linha+1][coluna-1].red);
                   z3 = _mm_loadu_ps(&m[linha+1][coluna-2].red);
                   x4 = _mm_loadu_ps(&m[linha+2][coluna].red);
                   y4 = _mm_loadu_ps(&m[linha+2][coluna-1].red);
                   z4 = _mm_loadu_ps(&m[linha+2][coluna-2].red);
                   full = _mm_add_ps(x,_mm_add_ps(y,z));
                   full1 = _mm_add_ps(x1,_mm_add_ps(y1,z1));
                   full2 = _mm_add_ps(x2,_mm_add_ps(y2,z2));
                   full3 = _mm_add_ps(x3,_mm_add_ps(y3,z3));
                   full4 = _mm_add_ps(x4,_mm_add_ps(y4,z4));
                   full5 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   full6 = _mm_add_ps(full3,full4);
                   full7 = _mm_add_ps(full5,full6);


                   p.red = full7[0]/25;
                   p.green = full7[1]/25;
                   p.blue = full7[2]/25;
                   p.alpha = full7[3]/25;
                   mTrans[linha][coluna] = p;
         
                   
               }
	       else if(coluna == M-2)
	       {
                   __m128 x;
                   __m128 y;
                   __m128 z;
                   __m128 x1;
                   __m128 y1;
                   __m128 z1;
                   __m128 x2;
                   __m128 y2;
                   __m128 z2;
                   __m128 x3;
                   __m128 y3;
                   __m128 z3;
                   __m128 x4;
                   __m128 y4;
                   __m128 z4;
                   __m128 x5;
                   __m128 y5;
                   __m128 z5;
                   __m128 x6;
                   __m128 y6;

                   __m128 full;
                   __m128 full1;
                   __m128 full2;
                   __m128 full3;
                   __m128 full4;
                   __m128 full5;
                   __m128 full6;
                   __m128 full7;

                   x = _mm_loadu_ps(&m[linha-2][coluna+1].red);
                   y = _mm_loadu_ps(&m[linha-2][coluna].red);
                   z = _mm_loadu_ps(&m[linha-2][coluna-1].red);
                   x1 = _mm_loadu_ps(&m[linha-2][coluna-2].red);
                   y1 = _mm_loadu_ps(&m[linha-1][coluna+1].red);
                   z1 = _mm_loadu_ps(&m[linha-1][coluna].red);
                   x2 = _mm_loadu_ps(&m[linha-1][coluna-1].red);
                   y2 = _mm_loadu_ps(&m[linha-1][coluna-2].red);
                   z2 = _mm_loadu_ps(&m[linha][coluna+1].red);
                   x3 = _mm_loadu_ps(&m[linha][coluna].red);
                   y3 = _mm_loadu_ps(&m[linha][coluna-1].red);
                   z3 = _mm_loadu_ps(&m[linha][coluna-2].red);
                   x4 = _mm_loadu_ps(&m[linha+1][coluna+1].red);
                   y4 = _mm_loadu_ps(&m[linha+1][coluna].red);
                   z4 = _mm_loadu_ps(&m[linha+1][coluna-1].red);
                   x5 = _mm_loadu_ps(&m[linha+1][coluna-2].red);
                   y5 = _mm_loadu_ps(&m[linha+2][coluna+1].red);
                   z5 = _mm_loadu_ps(&m[linha+2][coluna].red);
                   x6 = _mm_loadu_ps(&m[linha+2][coluna-1].red);
                   y6 = _mm_loadu_ps(&m[linha+2][coluna-2].red);

                   full = _mm_add_ps(_mm_add_ps(x,x1),_mm_add_ps(y,z));
                   full1 = _mm_add_ps(_mm_add_ps(y1,y2),_mm_add_ps(z1,x2));
                   full2 = _mm_add_ps(_mm_add_ps(z2,z3),_mm_add_ps(x3,y3));
                   full3 = _mm_add_ps(_mm_add_ps(x4,x5),_mm_add_ps(y4,z4));
                   full4 = _mm_add_ps(_mm_add_ps(y5,y6),_mm_add_ps(z5,x6));
                   full5 = _mm_add_ps(full,_mm_add_ps(full1,full2));
                   full6 = _mm_add_ps(full3,full4);
                   full7 = _mm_add_ps(full5,full6);

                   p.red = full7[0]/25;
                   p.green = full7[1]/25;
                   p.blue = full7[2]/25;
                   p.alpha = full7[3]/25;
                   mTrans[linha][coluna] = p;

               }
               
           }
        
    }
    else
    {
        __m128 x;
        __m128 y;
        __m128 z;
        __m128 x1;
        __m128 y1;
        __m128 z1;
        __m128 x2;
        __m128 y2;
        __m128 z2;
        __m128 x3;
        __m128 y3;
        __m128 z3;
        __m128 x4;
        __m128 y4;
        __m128 z4;
        __m128 x5;
        __m128 y5;
        __m128 z5;
        __m128 x6;
        __m128 y6;
        __m128 z6;
        __m128 x7;
        __m128 y7;
        __m128 z7;
	__m128 x8;
        __m128 full;
        __m128 full1;
        __m128 full2;
        __m128 full3;
        __m128 full4;
        __m128 full5;
        __m128 full6;
        __m128 full7;

            x = _mm_loadu_ps(&m[linha][coluna].red);
            y = _mm_loadu_ps(&m[linha][coluna+1].red);
            z = _mm_loadu_ps(&m[linha][coluna+2].red);
            x1 = _mm_loadu_ps(&m[linha][coluna-1].red);
            y1 = _mm_loadu_ps(&m[linha][coluna-2].red);
            z1 = _mm_loadu_ps(&m[linha+1][coluna].red);
            x2 = _mm_loadu_ps(&m[linha+1][coluna+1].red);
            y2 = _mm_loadu_ps(&m[linha+1][coluna+2].red);
            z2 = _mm_loadu_ps(&m[linha+1][coluna-1].red);
            x3 = _mm_loadu_ps(&m[linha+1][coluna-2].red);
            y3 = _mm_loadu_ps(&m[linha+2][coluna].red);
            z3 = _mm_loadu_ps(&m[linha+2][coluna+1].red);
            x4 = _mm_loadu_ps(&m[linha+2][coluna+2].red);
            y4 = _mm_loadu_ps(&m[linha+2][coluna-1].red);
            z4 = _mm_loadu_ps(&m[linha+2][coluna-2].red);
            x5 = _mm_loadu_ps(&m[linha-1][coluna].red);
            y5 = _mm_loadu_ps(&m[linha-1][coluna+1].red);
            z5 = _mm_loadu_ps(&m[linha-1][coluna+2].red);
            x6 = _mm_loadu_ps(&m[linha-1][coluna-1].red);
            y6 = _mm_loadu_ps(&m[linha-1][coluna-2].red);
            z6 = _mm_loadu_ps(&m[linha-2][coluna].red);
            x7 = _mm_loadu_ps(&m[linha-2][coluna+1].red);
            y7 = _mm_loadu_ps(&m[linha-2][coluna+2].red);
            z7 = _mm_loadu_ps(&m[linha-2][coluna-1].red);
            x8 = _mm_loadu_ps(&m[linha-2][coluna-2].red);

	    full = _mm_add_ps(y1,_mm_add_ps(_mm_add_ps(x,x1),_mm_add_ps(y,z)));
            full1 = _mm_add_ps(z1,_mm_add_ps(_mm_add_ps(x2,y2),_mm_add_ps(z2,x3)));
            full2 = _mm_add_ps(y3,_mm_add_ps(_mm_add_ps(z3,x4),_mm_add_ps(y4,z4)));            
            full3 = _mm_add_ps(x5,_mm_add_ps(_mm_add_ps(y5,z5),_mm_add_ps(x6,y6)));            
            full4 = _mm_add_ps(z6,_mm_add_ps(_mm_add_ps(x7,y7),_mm_add_ps(z7,x8)));
	    full5 =  _mm_add_ps(full,_mm_add_ps(full1,full2));
            full6 =  _mm_add_ps(full3,full4);
            full7 =  _mm_add_ps(full5,full6);
            full7 = _mm_div_ps(full7,divisao);
            p.red = full7[0];
            p.green = full7[1];
            p.blue = full7[2];
            p.alpha = full7[3];
	    mTrans[linha][coluna] = p;
/*
        
        for(i = linha-2;i<=linha+2;i++)
        {
            x = _mm_loadu_ps(&m[i][coluna].red);
            y = _mm_loadu_ps(&m[i][coluna+1].red);
            z = _mm_loadu_ps(&m[i][coluna+2].red);
            x1 = _mm_loadu_ps(&m[i][coluna-1].red);
            y1 = _mm_loadu_ps(&m[i][coluna-2].red);
            full = _mm_add_ps(x,_mm_add_ps(y,z));
            full1 = _mm_add_ps(x1,y1);
            full2 += _mm_add_ps(full,full1);
      
      
             printf("m[%d][%d] = %f\n",i,coluna,m[i][coluna].red);
             printf("x[0] = %f\n",x[0]);
             printf("y[0] = %f\n",y[0]);
             printf("z[0] = %f\n",z[0]);
             printf("x1[0] = %f\n",x1[0]);
             printf("y1[0] = %f\n",y1[0]);
             printf("full[0] = %f\n",full[0]);
             printf("full1[0] = %f\n",full1[0]);
             getchar();
             
            
        }
        
        full2 = _mm_div_ps(full2,divisao);
        p.red = full2[0];
        p.green = full2[1];
        p.blue = full2[2];
        p.alpha = full2[3];
        mTrans[linha][coluna] = p;
*/       
        
    }
}



void transformarMatriz(pix **m,pix **mTrans)
{
    int i;
    int j;
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
            
        {
            funcAux(m,mTrans,i,j);
        }
    }
}

pix **alocarMatriz()
{
    int i;
    pix **p;
    p = (pix **) malloc(M * sizeof(pix *));
    
    //p = (pix **) _mm_malloc(M*sizeof(pix *),16);
    
    for(i=0;i<M;i++)
    {
        p[i] = (pix *) malloc(N * sizeof(pix));
        //        p[i] = (pix *) _mm_malloc(N * sizeof(pix),16);
    }
    
    return p;
}


void MontarMatriz(pix **m)
{
    int i;
    int j;
    pix p;
    
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
        {
            p.red = rand() % 256;
            p.green = rand() % 256;
            p.blue = rand() % 256;
            p.alpha = rand() % 256;
            m[i][j] = p;
        }
    }
    
}

void imprimirMatriz(pix **m)
{
    int i;
    int j;
    
    for(i=0;i<M;i++)
    {
        for(j=0;j<N;j++)
        {
            printf("Matriz RED[%d][%d] = %f\n",i,j,m[i][j].red);
            //	printf("Matriz GREEN[%d][%d] = %f\n",i,j,m[i][j].green);
            //	printf("Matriz BLUE[%d][%d] = %f\n",i,j,m[i][j].blue);
            //	printf("Matriz ALPHA[%d][%d] = %f\n",i,j,m[i][j].alpha);
        }			
    }	
}
