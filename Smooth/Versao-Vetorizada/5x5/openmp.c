#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "omp.h"
#define M 5000
#define N 5000


typedef struct pixel
{
	int red;
	int green;
	int blue;
	int alpha;

}pix;


int **alocarMatriz();
pix **alocarMatrizPixel();
void MontarMatriz(int **m); 


void imprimirMatrizPixel(pix **m);
void imprimirMatriz(int **m);

void transformarMatriz(int **m,pix **mTrans,short tipo);
void funcAux(int **m,pix **mTrans,int linha,int coluna,short tipo);

int **matrizRed;
int **matrizGreen;
int **matrizBlue;
int **matrizAlpha;


int main()
{
	struct timeval start, end;
	double delta = 0;	
	pix **vTrans = alocarMatrizPixel();
	matrizRed = alocarMatriz(); 
	matrizGreen = alocarMatriz();
	matrizBlue = alocarMatriz();
	matrizAlpha = alocarMatriz();
	
	MontarMatriz(matrizRed);
	MontarMatriz(matrizGreen);
	MontarMatriz(matrizBlue);
	MontarMatriz(matrizAlpha);

	gettimeofday(&start, NULL);
	transformarMatriz(matrizRed,vTrans,0);
        gettimeofday(&end, NULL);
 
        delta = ((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6;
	
	//imprimirMatriz(matrizRed);

	//imprimirMatrizPixel(vTrans);
//	cur_time = ((double) stop-start) / CLOCKS_PER_SEC;
	printf("Averagely used %f seconds.\n", delta);
	return 0;
	
}

void funcAux(int **m,pix **mTrans,int linha,int coluna,short tipo)
{
	int i;
	int j;
	int contador = 0;
	
	for(i = linha -2;i <= linha + 2;i++)
	{
		for(j = coluna - 2;j <= coluna + 2;j++)
		{
			if(i>=0 && i<M && j>=0 && j<N)
			{
				contador += m[i][j];
			}
		}
	}
	
	switch(tipo)
	{
		case 0:
		mTrans[linha][coluna].red = contador/25;
		break;
		case 1:
                mTrans[linha][coluna].green = contador/25;
                break;
		case 2:
                mTrans[linha][coluna].blue = contador/25;
                break;
		case 3:
                mTrans[linha][coluna].alpha = contador/25;
                break;
	}	

}


void transformarMatriz(int **m,pix **mTrans,short tipo)
{
	int i;
	int j;
	#pragma omp parallel for collapse(2) num_threads(32)
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			funcAux(m,mTrans,i,j,tipo);
		}
	}
}

int **alocarMatriz()
{
	int i;
	int **p;
	p = (int **) malloc(M * sizeof(int *)); 
	
	for(i=0;i<M;i++)
	{
		p[i] = (int *) malloc(N * sizeof(int));
	}
	
	return p;
}

pix **alocarMatrizPixel()
{
        int i;
        pix **p;
        p = (pix **) malloc(M * sizeof(pix *));

        for(i=0;i<M;i++)
        {
                p[i] = (pix *) malloc(N * sizeof(pix));
        }

        return p;
}

void MontarMatriz(int **m)
{
	int i;
	int j;
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			m[i][j] = rand() % 256;
		}
	}
	
}



void imprimirMatrizPixel(pix **m)
{
	int i;
	int j;
	
	for(i=0;i<M;i++)
	{
		for(j=0;j<N;j++)
		{
			printf("Matriz RED[%d][%d] = %d\n",i,j,m[i][j].red);
			printf("Matriz GREEN[%d][%d] = %d\n",i,j,m[i][j].green);
			printf("Matriz BLUE[%d][%d] = %d\n",i,j,m[i][j].blue);
			printf("Matriz ALPHA[%d][%d] = %d\n",i,j,m[i][j].alpha);
		}			
	}	
}

void imprimirMatriz(int **m)
{
        int i;
        int j;

        for(i=0;i<M;i++)
        {
                for(j=0;j<N;j++)
                {
                        printf("Matriz[%d][%d] = %d\n",i,j,m[i][j]);
                }
        }
}
