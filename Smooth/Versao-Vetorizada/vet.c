#include "xmmintrin.h"
#include "emmintrin.h"
#include <stdio.h>


//Programa utilizando Instrucoes SSE

#define linhas 5
#define colunas 4


void transformar(float matriz[linhas][colunas],int linha,int coluna);

int main()
{
	float matriz[linhas][colunas];
	int i,j,total;
	__m128 full;
	__m128 parcial;
	__m128 x;
	__m128 y;
	__m128 z;
	
	//Popula a matriz com valores de i
	for(i=0;i<linhas;i++)
	{
		for(j=0;j<colunas;j++)
		{
			matriz[i][j] = i;
		}
	}

	transformar(matriz,4,3);
}


void transformar(float matriz[linhas][colunas],int linha,int coluna)
{
	//int i = linha -1;
	int i;
	int j = coluna -1;
        __m128 x = _mm_setzero_ps();
        __m128 y = _mm_setzero_ps();
        __m128 z = _mm_setzero_ps();
	__m128 parcial = _mm_setzero_ps();
	__m128 full = _mm_setzero_ps();
	float total = 0;

	   for(i = linha -1;i <= linha + 1;i++)
           {
              //  j = 1;
                if(i>=0 && i<=linhas && j>=0 && j != colunas-2)
                {
                         //n consigo carregar 96 bits na memoria,ele iria pegar a quarta posicao
                        if(i == linha - 1)
                        {
                                x = _mm_set_ps(0.0,matriz[i][j+2],matriz[i][j+1],matriz[i][j]);
                        }
                        else if(i == linha)
                        {
                                y = _mm_set_ps(0.0,matriz[i][j+2],matriz[i][j+1],matriz[i][j]);
                        }
                        else if(i == linha + 1)
                        {
                                z = _mm_set_ps(0.0,matriz[i][j+2],matriz[i][j+1],matriz[i][j]);
                        }

                }
                else if(i>=0 && i<=linhas && j<0)
                {
			
			if(i == linha - 1)
			{
				 x = _mm_set_ps(0.0,0.0,matriz[i][j+2],matriz[i][j+1]);
			}	
                        else if(i == linha)
                        {
                                y = _mm_set_ps(0.0,0.0,matriz[i][j+2],matriz[i][j+1]);
                        
			}
                        else if(i == linha + 1)
                        {
                                z = _mm_set_ps(0.0,0.0,matriz[i][j+2],matriz[i][j+1]);
			
                        }
                }
		else if(i>=0 && i<=linhas && j==colunas - 2)
       		{
			 if(i == linha - 1)
                        {
                                 x = _mm_set_ps(0.0,0.0,matriz[i][j],matriz[i][j+1]);
                        }
                        else if(i == linha)
                        {
                                y = _mm_set_ps(0.0,0.0,matriz[i][j],matriz[i][j+1]);
                        
                        }
                        else if(i == linha + 1)
                        {
                                z = _mm_set_ps(0.0,0.0,matriz[i][j],matriz[i][j+1]);
                        
                        }

		}


	 }

	 parcial = _mm_add_ps(y,z);
	 full = _mm_add_ps(parcial,x);
	
	for(i=0;i<4;i++)
        {
              total += full[i];
        }
        printf("total = %f\n",total);
} 
